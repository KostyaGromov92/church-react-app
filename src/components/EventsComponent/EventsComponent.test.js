import React from 'react';
import { shallow } from 'enzyme';
import EventsComponent from './EventsComponent';

describe('<EventsComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<EventsComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
