// @flow

/* eslint-disable max-len */
import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import { Link } from 'react-router-dom';

// STYLES
import './EventsComponent.scss';

// CONSTANTS
import { MAIN_ROUTES } from '../../constants/routes';

const newsPlaceholderImage = require('../../img/archway.jpg');

type Props = {
  postList: Array<Object>,
};

const EventsComponent = ({ postList }: Props) => (
  <>
    <section className="events-list">
      <div className="events-list__title title__with-line">
        <h2>События</h2>
        <hr />
      </div>
      <div className="events-list__block">
        {postList.map((post) => (
          <article className="events-list__item" key={post.id}>
            <Link to={`${MAIN_ROUTES.blog}/${post.id}`}>
              <div className="events-list__image-block">
                <img className="events-list__image" src={post.images[0] || newsPlaceholderImage} alt="Event" />
              </div>
              <h3 className="events-list__title">{post.title}</h3>
              {ReactHtmlParser(post.text)}
            </Link>
          </article>
        ))}
      </div>
      {postList.length > 0
      && <Link to={MAIN_ROUTES.blog} className="events-list__link">Все события</Link>}
    </section>
  </>
);

export default EventsComponent;
