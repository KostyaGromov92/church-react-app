// @flow

import React from 'react';

// COMPONENTS
import TilePhotoGalleryComponent from '../TilePhotoGalleryComponent';
import LazyImageComponent from '../LazyImageComponent';

// STYLES
import './SingleGalleryComponent.scss';

const churchVideoImage = require('../../img/church-video.jpg');

type Props = {
  singleGalleryTitle: string,
  singleGalleryBody: string,
  singleGalleryImageList: Array<Object>,
  singleGalleryMainImage: string,
}

const SingleGalleryComponent = (props: Props) => {
  const {
    singleGalleryTitle,
    singleGalleryBody,
    singleGalleryImageList,
    singleGalleryMainImage,
  } = props;

  return (
    <div>

      <section className="single-gallery">
        <div className="single-gallery__title title__with-line">
          <h2>Фотогалерея</h2>
          <hr />
        </div>
        <div className="single-gallery__block">
          <div className="single-gallery__main-image">
            <LazyImageComponent src={singleGalleryMainImage || churchVideoImage} alt="Main" />
          </div>
          <h3 className="single-gallery__sub-title">{singleGalleryTitle}</h3>
          <p className="single-gallery__text">{singleGalleryBody}</p>
        </div>
        <TilePhotoGalleryComponent photos={singleGalleryImageList} />
      </section>
    </div>
  );
};

export default SingleGalleryComponent;
