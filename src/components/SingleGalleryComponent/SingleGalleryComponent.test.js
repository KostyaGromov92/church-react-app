import React from 'react';
import { shallow } from 'enzyme';
import SingleGalleryComponent from './SingleGalleryComponent';

describe('<SingleGalleryComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SingleGalleryComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
