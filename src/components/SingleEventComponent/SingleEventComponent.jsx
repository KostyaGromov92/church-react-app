// @flow

import React from 'react';
import ReactHtmlParser from 'react-html-parser';

// STYLES
import './SingleEventComponent.scss';


// COMPONENTS
import LazyImage from '../LazyImageComponent';

const newsPlaceholderImage = require('../../img/archway.jpg');

type Props = {
  title: string,
  subTitle: string,
  body: string,
  imageUrl?: ?string,
}

const SingleEventComponent = (props: Props) => {
  const {
    title,
    subTitle,
    body,
    imageUrl,
  } = props;

  return (
    <section className="single-event">
      <div className="single-event__title title__with-line">
        <h2>События</h2>
        <hr />
      </div>
      <div className="single-event__image">
        <LazyImage src={imageUrl || newsPlaceholderImage} alt={title} />
      </div>
      <h2 className="single-event__title-news">{title}</h2>
      <h3 className="single-event__sub-title-news">{subTitle}</h3>

      <div className="single-event__description">
        {ReactHtmlParser(body)}
      </div>
    </section>
  );
};

SingleEventComponent.defaultProps = {
  imageUrl: null,
};

export default SingleEventComponent;
