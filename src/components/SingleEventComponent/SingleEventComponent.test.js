import React from 'react';
import { shallow } from 'enzyme';
import SingleEventComponent from './SingleEventComponent';

describe('<SingleEventComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SingleEventComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
