// @flow

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  button: {
    minHeight: '36px',
  },
}));

export default useStyles;
