// @flow
// BUTTON - COMPONENT

import * as React from 'react';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';

// COMPONENTS
import Spinner from '../Admin/Spinner';

// STYLES
import useStyles from './styles';

type Props = {
  additionalStyle?: ?Object,
  fullWidth?: boolean,
  children: Array<*> | Object,
  startIcon?: ?React.ComponentType<*>,
  variant: string,
  color: string,
  disabled?: boolean,
  onClick: () => void,
  withSpinner?: boolean,
  isFetching?: boolean,
};

const ButtonComponent = (props: Props) => {
  const {
    additionalStyle,
    children,
    fullWidth,
    variant,
    color,
    startIcon,
    disabled,
    onClick,
    withSpinner,
    isFetching,
  } = props;
  const classes = useStyles();

  const className = additionalStyle ? clsx(classes.button, additionalStyle) : classes.button;

  return (
    <Button
      fullWidth={fullWidth}
      variant={variant}
      startIcon={startIcon}
      color={color}
      disabled={disabled}
      className={className}
      onClick={onClick}
    >
      {withSpinner && isFetching
        ? <Spinner size={24} isButtonSpinner />
        : children
      }
    </Button>
  );
};

ButtonComponent.defaultProps = {
  disabled: false,
  startIcon: null,
  additionalStyle: null,
  isFetching: false,
  fullWidth: false,
  withSpinner: false,
};

export default ButtonComponent;
