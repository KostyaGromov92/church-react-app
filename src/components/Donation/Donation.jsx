import React from 'react';

import './Donation.scss';

const donationIcon = require('../../img/icons/donate.svg');

const Donation = () => (
  <div className="donation">
    <h3 className="donation__title">Пожертвование</h3>
    <div className="donation__block">
      <img className="donation__image" src={donationIcon} alt="Donation" />
      <p className="donation__text">
              Давно выяснено, что при оценке дизайна и композиции читаемый
              текст мешает сосредоточиться используют потому
      </p>
    </div>
  </div>
);

export default Donation;
