import React from 'react';
import classNames from 'classnames';

// COMPONENTS
import GalleryListItemComponent from './GalleryListItemComponent';

// STYLES
import './GalleryListComponent.scss';

type Props = {
  photoGalleryList: Array<Object>,
};

const GalleryListComponent = (props: Props) => {
  const { photoGalleryList } = props;

  const chunkArray = (array, chunkSize) => {
    let index = 0;
    const arrayLength = array && array.length;
    const tempArray = [];

    for (index = 0; index < arrayLength; index += chunkSize) {
      const myChunk = array.slice(index, index + chunkSize);

      tempArray.push(myChunk);
    }

    return tempArray;
  };

  const galleryElements = chunkArray(photoGalleryList, 3);

  return (
    <section className="albums">
      <div className="albums__title title__with-line">
        <h2>Фотогалерея</h2>
        <hr />
      </div>
      { galleryElements.map((row, rowIndex) => {
        const isOrderBlock = rowIndex % 2 !== 0;
        const rowId = `${rowIndex}${Date.now()}`;

        return (
          <div key={rowId} className={classNames('albums__block', isOrderBlock && 'albums__block_ordered')}>
            {row.map((elem, index) => {
              const isBigBlock = index % 3 === 0;

              return (
                <GalleryListItemComponent
                  key={elem.id}
                  imgSource={elem.images[0]}
                  countOfImages={elem.images.length}
                  id={elem.id}
                  title={elem.title}
                  styleName={isBigBlock ? 'album__item_big' : ''}
                />
              );
            })}
          </div>
        );
      })}
    </section>
  );
};

export default GalleryListComponent;
