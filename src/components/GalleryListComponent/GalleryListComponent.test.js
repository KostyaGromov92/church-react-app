import React from 'react';
import { shallow } from 'enzyme';
import GalleryListComponent from './GalleryListComponent';

describe('<GalleryListComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<GalleryListComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
