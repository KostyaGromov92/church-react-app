import React from 'react';
import { shallow } from 'enzyme';
import GalleryListItemComponent from './GalleryListItemComponent';

describe('<GalleryListItemComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<GalleryListItemComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
