import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

// CONSTANTS
import { MAIN_ROUTES } from '../../../constants/routes';

type Props = {
  id: number,
  imgSource: string,
  styleName?: string,
  title: string,
  countOfImages: number,
}

const GalleryListItemComponent = (props: Props) => {
  const {
    id,
    imgSource,
    styleName,
    title,
    countOfImages,
  } = props;

  return (
    <Link to={`${MAIN_ROUTES.gallery}/${id}`} className={classNames('album__item', styleName && 'album__item_big')}>
      <div className="album__icon">
        <span className="album__count">{countOfImages}</span>
      </div>
      <img src={imgSource} alt="Church window" />
      <div className="album__content">
        <p className="album__description">{title}</p>
      </div>
    </Link>
  );
};

GalleryListItemComponent.defaultProps = {
  styleName: '',
};

export default GalleryListItemComponent;
