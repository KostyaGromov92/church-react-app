import React from 'react';

import './Pilgrimage.scss';

const pilgrimagePhoto = require('../../img/pilgrimage.jpg');

const Pilgrimage = () => (
  <div className="pilgrimage">
    <h3 className="pilgrimage__title">Паломничество</h3>
    <img className="pilgrimage__image" src={pilgrimagePhoto} alt="Pilgrimage" />
    <p className="pilgrimage__text">
            Давно выяснено, что при оценке дизайна и композиции читаемый текст
            мешает сосредоточиться используют потому, что тот
    </p>
  </div>
);

export default Pilgrimage;
