import React from 'react';
import { shallow } from 'enzyme';
import Pilgrimage from './Pilgrimage';

describe('<Pilgrimage />', () => {
  test('renders', () => {
    const wrapper = shallow(<Pilgrimage />);
    expect(wrapper).toMatchSnapshot();
  });
});
