import React from 'react';
import { shallow } from 'enzyme';
import PhotoGalleryComponent from './PhotoGalleryComponent';

describe('<PhotoGalleryComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<PhotoGalleryComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
