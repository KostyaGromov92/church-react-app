// @flow

import React from 'react';
import { LightgalleryProvider, LightgalleryItem } from 'react-lightgallery';

// COMPONENTS
import LazyImage from '../LazyImageComponent';

type Props = {
  images: Array<*>,
}

const PhotoGalleryComponent = (props: Props) => {
  const { images } = props;

  const PhotoItem = ({ imageSource, alt }: any) => (
    <LightgalleryItem src={imageSource} group="any">
      <LazyImage src={imageSource} alt={alt} />
    </LightgalleryItem>
  );

  return (
    <LightgalleryProvider>
      {images && images.map(image => (
        <PhotoItem
          key={image}
          imageSource={image}
          alt={image}
        />
      ))}
    </LightgalleryProvider>
  );
};

export default PhotoGalleryComponent;
