// @flow

import React from 'react';

// STYLES
import './SingleNewsComponent.scss';
import ReactHtmlParser from 'react-html-parser';

// COMPONENTS
import LazyImage from '../LazyImageComponent';

const newsPlaceholderImage = require('../../img/archway.jpg');

type Props = {
  title: string,
  subTitle: string,
  body: string,
  imageUrl?: ?string,
}

const SingleNewsComponent = (props: Props) => {
  const {
    title,
    subTitle,
    body,
    imageUrl,
  } = props;

  return (
    <section className="single-news">
      <div className="single-news__title title__with-line">
        <h2>Новости</h2>
        <hr />
      </div>
      <div className="single-news__image">
        <LazyImage src={imageUrl || newsPlaceholderImage} alt={title} />
      </div>
      <h2 className="single-news__title-news">{title}</h2>
      <h3 className="single-news__sub-title-news">{subTitle}</h3>

      <div className="single-news__description">
        {ReactHtmlParser(body)}
      </div>
    </section>
  );
};

SingleNewsComponent.defaultProps = {
  imageUrl: null,
};

export default SingleNewsComponent;
