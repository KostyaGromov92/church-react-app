import React from 'react';
import { shallow } from 'enzyme';
import SingleNewsComponent from './SingleNewsComponent';

describe('<NewsItemComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SingleNewsComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
