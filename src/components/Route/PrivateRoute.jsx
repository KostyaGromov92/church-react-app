/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
// @flow
import * as React from 'react';

import { Route } from 'react-router-dom';
import { Redirect } from 'react-router';

// ROUTES
import { ADMIN_ROUTES } from '../../constants/routes';

// HELPERS
import { isUserAuthenticated } from '../../services/helpers';

const PrivateRoute = ({ component: ComponentName, ...rest }: Object) => (
  <Route
    {...rest}
    render={(props) => (
      isUserAuthenticated()
        ? <ComponentName {...props} />
        : <Redirect to={ADMIN_ROUTES.signIn} />
    )}
  />
);

export default PrivateRoute;
