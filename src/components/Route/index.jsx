/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable max-len */
// @flow

import React from 'react';

import { Route as BaseRoute, withRouter } from 'react-router-dom';

type Props = {
  component: any,
  exact: boolean,
  checkAuth?: boolean,
}

function Route({
  component,
  exact = true,
  ...props
}: Props) {
  return (
    <BaseRoute
      exact={exact}
      {...props}
      render={(renderProps) => React.createElement(component, renderProps)}
    />
  );
}

Route.defaultProps = {
  checkAuth: false,
};

export default withRouter(Route);
