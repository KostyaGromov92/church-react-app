// @flow

import React from 'react';
import { Link } from 'react-router-dom';

// LINKS
import { MAIN_ROUTES } from '../../constants/routes';

// STYLES
import './MenuComponent.scss';

const searchIcon = require('../../img/icons/search-icon.svg');
const mobileIconMenu = require('../../img/icons/mobile-menu.svg');

const MenuComponent = () => (
  <>
    <div className="menu-icon">
      <img src={mobileIconMenu} alt="Mobile menu" />
    </div>
    <div className="menu-block">
      <div className="menu-block__menu">
        <input className="menu-block__checkbox" type="checkbox" />
        <span className="menu-block__line" />
        <span className="menu-block__line" />
        <span className="menu-block__line" />

        <nav className="menu">
          <ul>
            <li className="menu__item">
              <Link className="menu__link" to={MAIN_ROUTES.home}>Главная</Link>
            </li>
            <li className="menu__item">
              <Link className="menu__link" to={MAIN_ROUTES.church}>О церкви</Link>
            </li>
            <li className="menu__item">
              <Link className="menu__link" to={MAIN_ROUTES.history}>История</Link>
            </li>
            <li className="menu__item">
              <Link className="menu__link" to={MAIN_ROUTES.gallery}>Фотогалерея</Link>
            </li>
            <li className="menu__item">
              <Link className="menu__link" to={MAIN_ROUTES.blog}>Блог</Link>
            </li>
            <li className="menu__item">
              <Link className="menu__link" to={MAIN_ROUTES.about}>О нас</Link>
            </li>
          </ul>
          <div className="search">
            <button className="search__button" type="button">
              <img className="search__icon" src={searchIcon} alt="Search icon" />
            </button>
          </div>
        </nav>
      </div>

    </div>
  </>
);

export default MenuComponent;
