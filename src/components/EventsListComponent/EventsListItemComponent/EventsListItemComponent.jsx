// @flow

import React from 'react';
import { Link } from 'react-router-dom';

import ReactHtmlParser from 'react-html-parser';

// CONSTANTS
import { MAIN_ROUTES } from '../../../constants/routes';

// STYLES
import './EventsListItemComponent.scss';

// COMPONENTS
import LazyImage from '../../LazyImageComponent';

type Props = {
  eventId: number,
  title: string,
  body: string,
  mainImageUrl: string,
}

const NewsItemComponent = (props: Props) => {
  const {
    eventId,
    title,
    body,
    mainImageUrl,
  } = props;

  return (
    <Link to={`${MAIN_ROUTES.blog}/${eventId}`} className="events-block__item">
      <div className="events-block__image">
        <LazyImage src={mainImageUrl} alt={mainImageUrl} />
      </div>
      <div className="events-block__description">
        <h3 className="events-block__title">{title}</h3>
        {ReactHtmlParser(body)}
      </div>
    </Link>
  );
};

export default NewsItemComponent;
