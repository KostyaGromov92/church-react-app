import React from 'react';
import { shallow } from 'enzyme';
import EventsListItemComponent from './EventsListItemComponent';

describe('<EventsListItemComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<EventsListItemComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
