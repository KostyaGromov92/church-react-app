// @flow

import React from 'react';

// COMPONENTS
import EventsListItemComponent from './EventsListItemComponent';

// STYLES
import './EventsListComponent.scss';

type Props = {
  postList: Array<Object>
}

const EventsListComponent = (props: Props) => {
  const { postList } = props;

  return (
    <section className="events-block">
      <div className="event-block__title title__with-line">
        <h2>События</h2>
        <hr />
      </div>
      {postList && postList.map((news) => (
        <EventsListItemComponent
          mainImageUrl={news.images[0]}
          eventId={news.id}
          key={news.id}
          title={news.title}
          body={news.text}
        />
      ))}
    </section>
  );
};

export default EventsListComponent;
