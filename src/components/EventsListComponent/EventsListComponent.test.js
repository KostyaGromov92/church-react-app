import React from 'react';
import { shallow } from 'enzyme';
import EventsListComponent from './EventsListComponent';

describe('<EventsListComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<EventsListComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
