// @flow
// BREADCRUMBS - COMPONENT

import React from 'react';
import { NavLink } from 'react-router-dom';
import withBreadcrumbs from 'react-router-breadcrumbs-hoc';

// STYLES
import './BreadCrumbs.scss';

type Props = {
  breadcrumbs: any,
  title: string
}

const Breadcrumbs = ({ breadcrumbs, title }: Props) => (
  <div className="breadcrumbs">
    {breadcrumbs.map((crumb, i, arr) => {
      const isLastElement = arr.length - 1 === i;

      let LinkElement = React.Fragment;
      let props = {};

      if (!isLastElement) {
        LinkElement = NavLink;
        props = { href: crumb.match.url, to: crumb.match.url };
      }

      return (
        isLastElement
          ? <span key={crumb.match.url}>{title}</span>
          : (
            <LinkElement key={crumb.match.url} className="breadcrumbs__item" {...props}>
              {crumb.breadcrumb}
              /
            </LinkElement>
          )
      );
    })}
  </div>
);

export default withBreadcrumbs()(Breadcrumbs);
