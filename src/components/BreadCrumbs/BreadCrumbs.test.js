import React from 'react';
import { shallow } from 'enzyme';
import BreadCrumbs from './BreadCrumbs';

describe('<BreadCrumbs />', () => {
  test('renders', () => {
    const wrapper = shallow(<BreadCrumbs />);
    expect(wrapper).toMatchSnapshot();
  });
});
