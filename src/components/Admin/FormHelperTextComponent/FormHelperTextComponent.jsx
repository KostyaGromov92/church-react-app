// @flow
// FORM HELPER TEXT - COMPONENT

import React from 'react';
import clsx from 'clsx';
import FormHelperText from '@material-ui/core/FormHelperText';

// STYLES
import useStyles from './styles';

type Props = {
  helperText: string,
  hasError?: boolean,
  additionalStyle?: ?Object,
}

const FormHelperTextComponent = (props: Props) => {
  const { helperText, hasError, additionalStyle } = props;

  const classes = useStyles();

  return (
    <FormHelperText
      className={clsx(classes.helperText, additionalStyle)}
      error={hasError}
    >
      {helperText}
    </FormHelperText>
  );
};

FormHelperTextComponent.defaultProps = {
  hasError: false,
  additionalStyle: null,
};

export default FormHelperTextComponent;
