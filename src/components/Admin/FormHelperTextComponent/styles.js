// @flow

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  helperText: {
    margin: '8px 14px 0',
  },
}));

export default useStyles;
