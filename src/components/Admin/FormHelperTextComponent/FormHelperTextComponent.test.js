import React from 'react';
import { shallow } from 'enzyme';
import FormHelperTextComponent from './FormHelperTextComponent';

describe('<FormHelperTextComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<FormHelperTextComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
