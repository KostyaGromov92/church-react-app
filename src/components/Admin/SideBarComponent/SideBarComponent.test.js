import React from 'react';
import { shallow } from 'enzyme';
import SideBarComponent from './SideBarComponent';

describe('<HeaderComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SideBarComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
