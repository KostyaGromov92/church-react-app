// @flow

import { makeStyles } from '@material-ui/core/styles';
import { DRAWER_WIDTH } from '../../../constants/materialUI';

const useStyles = makeStyles((theme) => ({
  drawer: {
    [theme.breakpoints.up('md')]: {
      width: (props) => (props.isAdminUser ? 240 : 0),
      flexShrink: 0,
    },
    whiteSpace: 'nowrap',
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: DRAWER_WIDTH,
  },
}));

export default useStyles;
