// @flow

import React from 'react';

import Hidden from '@material-ui/core/Hidden';

// CONSTANTS
import type { Element } from 'react';

// COMPONENTS
import DrawerComponent from '../DrawerComponent';

// STYLES
import useStyles from './styles';

type Props = {
  container: Object | Element<*>,
  handleDrawerToggle: () => void,
  isOpenMobileMenu: boolean,
  isAdminUser: boolean,
  menuLinks: Array<Object>
}

const SideBarComponent = (props: Props) => {
  const {
    container,
    handleDrawerToggle,
    isOpenMobileMenu,
    menuLinks,
    isAdminUser,
  } = props;

  const classes = useStyles({ isAdminUser });

  return (
    <nav className={classes.drawer} aria-label="mailbox folders">
      <Hidden smUp implementation="css">
        <DrawerComponent
          links={menuLinks}
          container={container}
          variant="temporary"
          isOpen={isOpenMobileMenu}
          handleDrawerToggle={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true,
          }}
          isAdminUser
        />
      </Hidden>
      <Hidden only={['sm', 'xs']} implementation="css">
        <DrawerComponent
          links={menuLinks}
          classes={{
            paper: classes.drawerPaper,
          }}
          variant="permanent"
          isOpen
          isAdminUser
        />
      </Hidden>
    </nav>
  );
};

export default SideBarComponent;
