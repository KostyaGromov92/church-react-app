import { makeStyles } from '@material-ui/core/styles';
import { DRAWER_WIDTH } from '../../../constants/materialUI';

const useStyles = makeStyles(themeProp => ({
  spinner: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
    height: '100%',
  },
  spinnerWrapper: {
    display: 'flex',
    position: 'absolute',
    left: `${DRAWER_WIDTH}px`,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    width: `calc(100% - ${DRAWER_WIDTH}px)`,
    height: '100vh',

    [themeProp.breakpoints.down('sm')]: {
      left: 0,
      width: '100%',
    },
  },
}));

export default useStyles;
