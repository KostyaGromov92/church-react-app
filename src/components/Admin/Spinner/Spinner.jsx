// @flow


import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import clsx from 'clsx';

// STYLES
import useStyles from './styles';


type Props = {
  isButtonSpinner?: boolean,
  size?: number,
  additionalStyle?: Object,
}

const Spinner = (props: Props) => {
  const classes = useStyles();
  const { size, additionalStyle, isButtonSpinner } = props;

  const spinnerStyle = additionalStyle ? clsx(classes.spinner, additionalStyle) : classes.spinner;

  return isButtonSpinner ? (
    <CircularProgress size={size} className={spinnerStyle} />
  ) : (
    <div className={classes.spinnerWrapper}>
      <CircularProgress size={size} className={spinnerStyle} />
    </div>
  );
};

Spinner.defaultProps = {
  isButtonSpinner: false,
  size: 36,
  additionalStyle: {},
};

export default Spinner;
