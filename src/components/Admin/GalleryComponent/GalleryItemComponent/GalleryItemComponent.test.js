import React from 'react';
import { shallow } from 'enzyme';
import GalleryItemComponent from './GalleryItemComponent';

describe('<GalleryItemComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<GalleryItemComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
