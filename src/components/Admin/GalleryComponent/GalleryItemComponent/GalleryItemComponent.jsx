// @flow
// ====================================

// GALLERY ITEM - COMPONENT

import React from 'react';

import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

// COMPONENTS
import LazyImage from '../../../LazyImageComponent';

// STYLES
import useStyles from './styles';

type Props = {
  fileUrl: string,
  fileName: string,
  removeFileImage: (fileId: string) => void,
}

const GalleryItemComponent = (props: Props) => {
  const {
    fileUrl, fileName, removeFileImage,
  } = props;
  const classes = useStyles();

  return (
    <div className={classes.previewImage}>
      <div className={classes.previewImageItem}>
        <LazyImage
          src={fileUrl}
          alt={fileName}
        />
      </div>
      <div className={classes.iconWrapper}>
        <IconButton
          onClick={() => removeFileImage(fileName)}
          aria-label="delete"
          className={classes.deleteIconPreview}
        >
          <DeleteIcon fontSize="small" />
        </IconButton>
      </div>
    </div>
  );
};

export default GalleryItemComponent;
