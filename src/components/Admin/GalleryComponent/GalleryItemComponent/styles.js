import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  previews: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  iconWrapper: {
    height: '50px',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  gallery: {
    padding: '45px 30px',
    border: '1px #c0c0c0 dashed',
    borderRadius: '5px',
    display: 'flex',
    justifyContent: 'center',
  },
  galleryWrapper: {
    margin: '10px 0',
  },
  previewImage: {
    maxWidth: '300px',
    minWidth: '300px',
    margin: '30px 10px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    position: 'relative',
  },
  uploadIconPreview: {
    position: 'absolute',
    bottom: '35px',
    right: '10px',
  },
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'center',
    marginTop: '30px',
  },
  previewImageItem: {
    display: 'flex',
    minHeight: '200px',
    alignItems: 'center',
    justifyContent: 'center',
  },
  previewNameImage: {
    marginTop: '10px',
  },
}));

export default useStyles;
