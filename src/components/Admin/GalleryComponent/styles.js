import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  previews: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gallery: {
    padding: '45px 30px',
    border: '1px #c0c0c0 dashed',
    borderRadius: '5px',
    display: 'flex',
    justifyContent: 'center',
  },
  titleError: {
    color: theme.palette.error.main,
  },
  galleryWrapper: {
    margin: '10px 0',
  },
  previewImage: {
    maxWidth: '300px',
    minWidth: '300px',
    maxHeight: '300px',
    margin: '30px 10px 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    position: 'relative',
  },
  deleteIconPreview: {
    position: 'absolute',
    top: '5px',
    right: '10px',
  },
  uploadIconPreview: {
    position: 'absolute',
    bottom: '35px',
    right: '10px',
  },
  button: {
    width: '100%',
  },
  buttonWrapper: {
    minWidth: '114px',
    display: 'inline-flex',
    flexDirection: 'row',
    marginTop: '30px',
    height: '36px',
    position: 'relative',
  },
  previewImageItem: {
    display: 'flex',
    minHeight: '200px',
    alignItems: 'center',
    justifyContent: 'center',
  },
  previewNameImage: {
    marginTop: '10px',
  },
}));

export default useStyles;
