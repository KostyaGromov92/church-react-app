/* eslint-disable max-len,react/jsx-props-no-spreading */
// @flow

import React from 'react';

import { useDropzone } from 'react-dropzone';

// COMPONENTS
import GalleryItemComponent from './GalleryItemComponent';
import FormHelperTextComponent from '../FormHelperTextComponent';
import FormLabelComponent from '../FormLabelComponent';
import Button from '../../Button';

// STYLES
import useStyles from './styles';

type Props = {
  isEmptyGallery?: boolean,
  imageFiles?: Array<Object>,
  imagesListUri?: Array<string>,
  isFetching: boolean,
  uploadPhoto: (files: Array<Object>) => void,
  handleRemoveImage: (imageUri: string) => void,
  handleChangeGallery: (acceptedFiles: Object) => void,
  removeFileImage: (fileName: string) => void,
  removeImage: (fileName: string) => void,
};

const GalleryComponent = (props: Props) => {
  const {
    imageFiles, imagesListUri, uploadPhoto, isEmptyGallery, handleChangeGallery, removeFileImage, removeImage, isFetching,
  } = props;

  const classes = useStyles();

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: 'image/*',
    onDrop: (acceptedFiles) => handleChangeGallery(acceptedFiles),
  });


  const imageFileList = imageFiles && imageFiles.length > 0 && imageFiles.map((file) => (
    <GalleryItemComponent
      key={file.name}
      fileName={file.name}
      fileUrl={URL.createObjectURL(file)}
      removeFileImage={removeFileImage}
    />
  ));

  const imageList = imagesListUri && imagesListUri.map((imageUrl) => (
    <GalleryItemComponent
      key={imageUrl}
      fileName={imageUrl}
      fileUrl={imageUrl}
      removeFileImage={removeImage}
    />
  ));

  return (
    <div className={classes.galleryWrapper}>
      <FormLabelComponent labelText="Галерея" additionalStyle={isEmptyGallery && classes.titleError} />
      <div className={classes.gallery} {...getRootProps()}>
        <input {...getInputProps()} />
        {
          isDragActive
            ? <p>Поместите файлы сюда ...</p>
            : <p>Перетащите файлы сюда или добавьте через проводник</p>
        }
      </div>
      <div>
        <div className={classes.previews}>
          {imageList}
          {imageFileList}
        </div>
        {isEmptyGallery && (
          <FormHelperTextComponent
            helperText="Пожалуйста добавтье хотя бы одну фотографию"
            hasError={isEmptyGallery}
          />
        )}
        {imageFileList && imageFileList.length > 0 && (
          <div className={classes.buttonWrapper}>
            <Button
              variant="contained"
              color="primary"
              disabled={isFetching}
              additionalStyle={classes.button}
              onClick={() => imageFiles && uploadPhoto(imageFiles)}
              withSpinner
              isFetching={isFetching}
            >
              Загрузить
            </Button>

          </div>
        )}
      </div>
    </div>
  );
};

GalleryComponent.defaultProps = {
  imageFiles: [],
  isEmptyGallery: false,
  imagesListUri: [],
};

export default GalleryComponent;
