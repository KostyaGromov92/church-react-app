import React from 'react';
import { shallow } from 'enzyme';
import GalleryComponent from './GalleryComponent';

describe('<GalleryComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<GalleryComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
