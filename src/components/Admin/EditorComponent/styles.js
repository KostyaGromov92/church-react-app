import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: '20px',
  },
  editorWrapper: {
    backgroundColor: 'white',
    padding: '20px',
    margin: 0,
    borderRadius: '4px',
    border: '1px solid transparent',
  },
  errorLabel: {
    color: theme.palette.error.main,
  },
  editorError: {
    borderColor: theme.palette.error.main,
  },
  editor: {
    minHeight: '400px',
  },
  editorDisabled: {
    opacity: '0.5',
    pointerEvents: 'none',
  },
}));

export default useStyles;
