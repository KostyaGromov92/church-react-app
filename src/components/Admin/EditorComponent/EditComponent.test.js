/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import EditorComponent from './EditorComponent';

describe('<EditorComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<EditorComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
