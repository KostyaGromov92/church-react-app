// @flow
// EDITOR - COMPONENT

import React from 'react';
import classNames from 'classnames';

import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

// COMPONENTS
import FormHelperTextComponent from '../FormHelperTextComponent';
import FormLabelComponent from '../FormLabelComponent';

// STYLES
import useStyles from './styles';

type Props = {
  editorState: Object,
  onEditorStateChange: (editorStateText: any) => void,
  isDisabled?: boolean,
  title?: ?string,
  hasError?: boolean,
  editorHelperText?: string,
};

const EditorComponent = (props: Props) => {
  const {
    isDisabled, title, hasError, editorHelperText, editorState, onEditorStateChange,
  } = props;

  const classes = useStyles();

  return (
    <div className={classes.root}>
      {title && (
        <FormLabelComponent
          additionalStyle={hasError && classes.errorLabel}
          labelText={title}
        />
      )}
      <Editor
        editorState={editorState}
        toolbarClassName="toolbarClassName"
        wrapperClassName={classNames(
          classes.editorWrapper,
          isDisabled && classes.editorDisabled,
          hasError && classes.editorError,
        )}
        editorClassName={classes.editor}
        onEditorStateChange={onEditorStateChange}
      />

      {editorHelperText && hasError && (
        <FormHelperTextComponent
          hasError={hasError}
          helperText={editorHelperText}
        />
      )}
    </div>
  );
};

EditorComponent.defaultProps = {
  isDisabled: false,
  title: null,
  hasError: false,
  editorHelperText: null,
};

export default EditorComponent;
