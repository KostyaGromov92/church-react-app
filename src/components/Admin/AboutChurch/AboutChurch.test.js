import React from 'react';
import { shallow } from 'enzyme';
import AboutChurch from './AboutChurch';

describe('<AboutChurch />', () => {
  test('renders', () => {
    const wrapper = shallow(<AboutChurch />);
    expect(wrapper).toMatchSnapshot();
  });
});
