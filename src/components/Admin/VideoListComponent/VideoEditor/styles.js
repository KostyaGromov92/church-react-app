import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  buttonWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: '40px',
  },
  description: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '10px',
  },
  textArea: {
    resize: 'none',
    borderRadius: '4px',
    padding: '5px',
  },
}));

export default useStyles;
