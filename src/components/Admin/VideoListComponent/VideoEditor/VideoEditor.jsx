// @flow

import React from 'react';
import { useHistory } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';

// COMPONENTS
import Button from '../../../Button';
import TextArea from '../../TextArea';

// STYLES
import useStyles from './styles';

// HOOKS
import useForm from '../../../../hooks/useForm';

// VALIDATION
import validate from '../../../../validation/videoEditor';
import { validateGUID } from '../../../../services/helpers';

type Props = {
  videoId: string,
  title: string,
  body: string,
  videoLink: string,
  handleUpdateVideo: (data: Object) => void,
}

const NewsEditorComponent = (props: Props) => {
  const {
    videoId, title, body, videoLink, handleUpdateVideo,
  } = props;

  const classes = useStyles();
  const history = useHistory();

  const goToVideoListPage = () => history.goBack();

  const defaultValues = {
    title,
    body,
    videoLink,
  };

  const {
    values,
    errors,
    handleChange,
    handleBlur,
    isFormHasError,
    handleFocus,
  }: Object = useForm(null, validate, defaultValues);

  const submitValue = () => {
    const fields = {
      ...(validateGUID(videoId) && { id: videoId }),
      Title: values.title,
      Link: values.videoLink.replace('watch?v=', 'v/'),
      Text: values.body,
    };

    handleUpdateVideo(fields);
  };

  return (
    <>
      <FormControl noValidate autoComplete="off" fullWidth>
        <TextField
          margin="normal"
          id="outlined-basic"
          label="Заголовок"
          helperText={errors.title}
          value={values.title}
          variant="outlined"
          error={!!errors.title}
          onChange={(event) => handleChange('title', event)}
          onBlur={(event) => handleBlur('title', event)}
          onFocus={() => handleFocus('title')}
        />

        <TextField
          margin="normal"
          id="outlined-basic"
          label="Ссылка на видео"
          helperText={errors.videoLink}
          value={values.videoLink}
          variant="outlined"
          error={!!errors.videoLink}
          onChange={(event) => handleChange('videoLink', event)}
          onBlur={(event) => handleBlur('videoLink', event)}
          onFocus={() => handleFocus('videoLink')}
        />

        <div className={classes.description}>
          <TextArea
            hasError={!!errors.body}
            title="Описание"
            defaultValue={values.body}
            rowsMax={20}
            onChange={(event) => handleChange('body', event)}
            onBlur={(event) => handleBlur('body', event)}
            errorText={errors.body}
            onFocus={() => handleFocus('body')}
          />
        </div>

      </FormControl>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="default"
          additionalStyle={classes.button}
          onClick={() => goToVideoListPage()}
        >
          Отмена
        </Button>
        <Button
          variant="contained"
          color="primary"
          disabled={isFormHasError}
          additionalStyle={classes.button}
          onClick={submitValue}
        >
          Сохранить
        </Button>
      </div>
    </>
  );
};

export default NewsEditorComponent;
