import React from 'react';
import { shallow } from 'enzyme';
import VideoEditor from './VideoEditor';

describe('<VideoEditor />', () => {
  test('renders', () => {
    const wrapper = shallow(<VideoEditor />);
    expect(wrapper).toMatchSnapshot();
  });
});
