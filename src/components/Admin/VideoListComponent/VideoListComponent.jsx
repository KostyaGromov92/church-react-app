// @flow

import React from 'react';
import { useHistory } from 'react-router-dom';

import AddIcon from '@material-ui/icons/Add';

// COMPONENTS
import VideoItem from './VideoItem';
import Button from '../../Button';

// STYLES
import useStyles from './styles';

// CONSTANTS
import { ADMIN_ROUTES } from '../../../constants/routes';

type Props = {
  videoListData: Array<Object>,
  deleteVideo: (videoId: string) => void,
  clearSingleVideoData: () => void,
}

const VideoListComponent = (props: Props) => {
  const { videoListData, deleteVideo, clearSingleVideoData } = props;

  const classes = useStyles();

  const history = useHistory();

  const addNewVideo = async () => {
    await clearSingleVideoData();
    history.push(ADMIN_ROUTES.addVideo);
  };

  return (
    <>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="primary"
          additionalStyle={classes.button}
          startIcon={<AddIcon />}
          onClick={addNewVideo}
        >
          Добавить
        </Button>
      </div>
      <div>
        {videoListData && videoListData.map((video) => (
          <VideoItem
            key={video.id}
            id={video.id}
            title={video.title}
            body={video.text}
            link={video.link}
            deleteVideo={deleteVideo}
          />
        ))}
      </div>
    </>
  );
};

export default VideoListComponent;
