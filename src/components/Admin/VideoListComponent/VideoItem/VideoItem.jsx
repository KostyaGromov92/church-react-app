// @flow

import React from 'react';

import { useHistory } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';
import Typography from '@material-ui/core/Typography';

// STYLES
import useStyles from './styles';

type Props = {
  id: number,
  title: string,
  body: string,
  link: string,
  deleteVideo: (videoId: number) => void,
};

const SingleNews = (props: Props) => {
  const {
    id, title, body, link, deleteVideo,
  } = props;

  const classes = useStyles();
  const history = useHistory();

  const goToEditVideoPage = (videoId: number) => history.push(`/admin/video/${videoId}`);

  return (
    <Paper className={classes.root}>
      <div className={classes.iconsWrapper}>
        <IconButton aria-label="edit" className={classes.margin} onClick={() => goToEditVideoPage(id)}>
          <EditIcon fontSize="small" />
        </IconButton>
        <IconButton aria-label="edit" onClick={() => deleteVideo(id)} className={classes.margin}>
          <DeleteIcon fontSize="small" />
        </IconButton>
      </div>
      <div className={classes.newsWrapper}>
        <IconButton aria-label="edit" className={classes.newsButton}>
          <ViewHeadlineIcon fontSize="small" />
        </IconButton>
        <div className={classes.videoWrapper}>
          <iframe
            width="420"
            title="Video"
            style={{ height: 'auto', maxWidth: '100%' }}
            src={link}
          />
        </div>
        <div className={classes.text}>
          <Typography variant="h4" gutterBottom>
            {title}
          </Typography>
          <Typography variant="body1" gutterBottom>
            {body}
          </Typography>
        </div>
      </div>
    </Paper>
  );
};

export default SingleNews;
