import React from 'react';
import { shallow } from 'enzyme';
import VideoItem from './VideoItem';

describe('<VideoItem />', () => {
  test('renders', () => {
    const wrapper = shallow(<VideoItem />);
    expect(wrapper).toMatchSnapshot();
  });
});
