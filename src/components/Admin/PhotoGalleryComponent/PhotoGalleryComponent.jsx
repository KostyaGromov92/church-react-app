/* eslint-disable max-len */
import React from 'react';
import { useHistory } from 'react-router-dom';

import AddIcon from '@material-ui/icons/Add';

// COMPONENTS
import SinglePhotoGalleryItem from './SinglePhotoGalleryItem';
import Button from '../../Button';

// STYLES
import useStyles from './styles';

// CONSTANTS
import { ADMIN_ROUTES } from '../../../constants/routes';

type Props = {
  photoGalleryList: Array<Object>,
  handleClearImageListUri: () => void,
  handleClearSingleGalleryData: () => void,
  removePhotoGallery: (photoGalleryId: string) => void,
}

const PhotoGalleryComponent = (props: Props) => {
  const classes = useStyles();
  const history = useHistory();
  const {
    handleClearImageListUri, photoGalleryList, removePhotoGallery, handleClearSingleGalleryData,
  } = props;

  const addGallery = async () => {
    await handleClearImageListUri();
    await handleClearSingleGalleryData();

    history.push(ADMIN_ROUTES.addGallery);
  };

  return (
    <>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="primary"
          additionalStyle={classes.button}
          startIcon={<AddIcon />}
          onClick={addGallery}
        >
          Добавить
        </Button>
      </div>
      <div>
        {photoGalleryList && photoGalleryList.map((photoGallery) => (
          <SinglePhotoGalleryItem
            key={photoGallery.id}
            id={photoGallery.id}
            title={photoGallery.title}
            description={photoGallery.text}
            imageUrl={photoGallery.images[0]}
            removePhotoGallery={removePhotoGallery}
          />
        ))}
      </div>
    </>
  );
};

export default PhotoGalleryComponent;
