import React from 'react';
import { shallow } from 'enzyme';
import PhotoGalleryEditorComponent from './PhotoGalleryEditorComponent';

describe('<PhotoGalleryEditor />', () => {
  test('renders', () => {
    const wrapper = shallow(<PhotoGalleryEditorComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
