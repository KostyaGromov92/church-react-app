// @flow

import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';

// COMPONENTS
import GalleryComponent from '../../GalleryComponent';
import TextArea from '../../TextArea';
import FormHelperTextComponent from '../../FormHelperTextComponent';
import Button from '../../../Button';

// HOOKS
import useForm from '../../../../hooks/useForm';
import useGallery from '../../../../hooks/useGallery';

// VALIDATION
import validate from '../../../../validation/galleryEditor';

// STYLES
import useStyles from './styles';

// HELPERS
import { validateGUID } from '../../../../services/helpers';

type Props = {
  imagesListUri: Array<string>,
  uploadPhoto: (files: Array<Object>) => void,
  handleRemoveImage: (imageUri: string) => void,
  handleUpdateGallery: (galleryData: Object) => void,
  galleryId: string,
  title: string,
  body: string,
  isFetchingSingleImageList: boolean,
};

const PhotoGalleryEditorComponent = (props: Props) => {
  const {
    uploadPhoto,
    imagesListUri,
    handleRemoveImage,
    title,
    body,
    isFetchingSingleImageList,
    handleUpdateGallery,
    galleryId,
  } = props;

  const [files, setFiles] = useState([]);
  const [isShowGalleryAlertMessage, setGalleryAlertMessage] = useState(false);

  const defaultValues = {
    title,
    body,
  };

  const {
    values,
    errors,
    handleChange,
    handleBlur,
    isFormHasError,
    handleFocus,
  }: Object = useForm(null, validate, defaultValues);

  const {
    handleChangeGallery,
    imagesListURL,
    isEmptyGallery,
    filesList,
    removeFileImage,
    removeImage,
    imagesListToUpload,
  } = useGallery(imagesListUri, handleRemoveImage);

  const handleFormChange = (event: any) => {
    const hasFiles = event.target && event.target.files;

    if (hasFiles) {
      setFiles([...files, ...event.target.files]);
      setGalleryAlertMessage(true);
    }
  };

  const handleSubmitGallery = () => {
    const fields = {
      ...(validateGUID(galleryId) && { id: galleryId }),
      Title: values.title,
      Text: values.body,
      Images: imagesListToUpload.length > 0
        ? [...imagesListUri, ...imagesListToUpload]
        : imagesListUri,
    };

    handleUpdateGallery(fields);
  };

  const classes = useStyles();
  const history = useHistory();

  const goToNewsListPage = () => history.goBack();

  const handleUploadImages = (images: Array<Object>) => {
    uploadPhoto(images);
    setGalleryAlertMessage(false);
  };

  return (
    <>
      <FormControl onChange={handleFormChange} noValidate autoComplete="off" fullWidth>
        <TextField
          margin="normal"
          id="outlined-basic"
          label="Заголовок"
          value={values.title}
          variant="outlined"
          helperText={errors.title}
          onChange={(event) => handleChange('title', event)}
          error={!!errors.title}
          onBlur={(event) => handleBlur('title', event)}
          onFocus={() => handleFocus('title')}
        />

        <GalleryComponent
          isFetching={isFetchingSingleImageList}
          imagesListUri={imagesListURL}
          imageFiles={filesList}
          uploadPhoto={handleUploadImages}
          handleRemoveImage={handleRemoveImage}
          handleChangeGallery={handleChangeGallery}
          removeFileImage={removeFileImage}
          removeImage={removeImage}
          isEmptyGallery={isEmptyGallery}
        />

        <div className={classes.description}>
          <TextArea
            hasError={!!errors.body}
            title="Описание"
            defaultValue={values.body}
            rowsMax={20}
            onChange={(event) => handleChange('body', event)}
            onBlur={(event) => handleBlur('body', event)}
            errorText={errors.body}
            onFocus={() => handleFocus('body')}
          />
        </div>
        {isShowGalleryAlertMessage && (
          <FormHelperTextComponent
            hasError={false}
            helperText="Пожалуйста, перед сохранением новости, загрузите Ваши новые фотографии"
          />
        )}

      </FormControl>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="default"
          additionalStyle={classes.button}
          onClick={() => goToNewsListPage()}
        >
          Отмена
        </Button>
        <Button
          variant="contained"
          color="primary"
          additionalStyle={classes.button}
          onClick={handleSubmitGallery}
          disabled={isFormHasError || isEmptyGallery || isShowGalleryAlertMessage}
        >
          Сохранить
        </Button>
      </div>
    </>
  );
};

export default PhotoGalleryEditorComponent;
