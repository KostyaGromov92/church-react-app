import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    marginBottom: theme.spacing(5),
    width: '100%',
    padding: '25px',

    '&:last-child': {
      marginBottom: '0',
    },
  },
  imageWrapper: {
    display: 'flex',
    alignItems: 'stretch',
    maxHeight: '250px',
    maxWidth: '300px',
    margin: '0 20px',
    minWidth: '300px',
    marginBottom: '45px',

    [theme.breakpoints.up('lg')]: {
      marginBottom: 0,
    },
  },
  iconsWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginBottom: '20px',
  },
  newsWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

    [theme.breakpoints.up('lg')]: {
      flexDirection: 'row',
      alignItems: 'flex-start',
    },
  },
  newsButton: {
    margin: 'auto 0',
  },
}));

export default useStyles;
