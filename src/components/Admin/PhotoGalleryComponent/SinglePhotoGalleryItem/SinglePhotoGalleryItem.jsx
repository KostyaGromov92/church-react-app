// @flow
import React from 'react';

import { useHistory } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';
import Typography from '@material-ui/core/Typography';

// COMPONENTS
import LazyImage from '../../../LazyImageComponent';

// STYLES
import useStyles from './styles';

const newImage = require('../../../../img/archway.jpg');

type Props = {
  id: number,
  title: string,
  description: string,
  imageUrl: string,
  removePhotoGallery: (photoGalleryId: number) => void,
}

const SinglePhotoGalleryItem = (props: Props) => {
  const {
    title, description, id, removePhotoGallery, imageUrl,
  } = props;
  const classes = useStyles();
  const history = useHistory();

  const goToEditPhotoGalleryPage = (photoGalleryId: number) => history.push(`/admin/gallery/${photoGalleryId}`);

  return (
    <Paper className={classes.root}>
      <div className={classes.iconsWrapper}>
        <IconButton aria-label="edit" className={classes.margin} onClick={() => goToEditPhotoGalleryPage(id)}>
          <EditIcon fontSize="small" />
        </IconButton>
        <IconButton onClick={() => removePhotoGallery(id)} aria-label="edit" className={classes.margin}>
          <DeleteIcon fontSize="small" />
        </IconButton>
      </div>
      <div className={classes.newsWrapper}>
        <IconButton aria-label="edit" className={classes.newsButton}>
          <ViewHeadlineIcon fontSize="small" />
        </IconButton>
        <div className={classes.imageWrapper}>
          <LazyImage src={imageUrl || newImage} alt={title} />
        </div>
        <div className={classes.text}>
          <Typography variant="h4" gutterBottom>
            {title}
          </Typography>
          <Typography variant="body1" gutterBottom>
            {description}
          </Typography>
        </div>
      </div>
    </Paper>
  );
};

export default SinglePhotoGalleryItem;
