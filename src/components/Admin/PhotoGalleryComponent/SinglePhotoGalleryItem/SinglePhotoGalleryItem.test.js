import React from 'react';
import { shallow } from 'enzyme';
import SinglePhotoGalleryItem from './SinglePhotoGalleryItem';

describe('<SinglePhotoGalleryItem />', () => {
  test('renders', () => {
    const wrapper = shallow(<SinglePhotoGalleryItem />);
    expect(wrapper).toMatchSnapshot();
  });
});
