import React from 'react';
import { shallow } from 'enzyme';
import NewsEditorComponent from './NewsEditorComponent';

describe('<NewsEditor />', () => {
  test('renders', () => {
    const wrapper = shallow(<NewsEditorComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
