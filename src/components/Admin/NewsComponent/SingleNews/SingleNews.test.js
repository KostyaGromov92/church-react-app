import React from 'react';
import { shallow } from 'enzyme';
import SingleNews from './SingleNews';

describe('<SingleNews />', () => {
  test('renders', () => {
    const wrapper = shallow(<SingleNews />);
    expect(wrapper).toMatchSnapshot();
  });
});
