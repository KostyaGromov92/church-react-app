// @flow

import React from 'react';

import { useHistory } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';
import Typography from '@material-ui/core/Typography';
import ReactHtmlParser from 'react-html-parser';

// COMPONENTS
import LazyImage from '../../../LazyImageComponent';

// STYLES
import useStyles from './styles';

// ROUTES
import { ADMIN_ROUTES } from '../../../../constants/routes';

type Props = {
  id: number,
  title: string,
  body: string,
  imageUrl: string,
  deleteNews: (newsId: number) => void,
};

const newsImage = require('../../../../img/archway.jpg');

const SingleNews = (props: Props) => {
  const classes = useStyles();
  const history = useHistory();
  const {
    id, title, body, deleteNews, imageUrl,
  } = props;

  const goToEditNewsPage = (newsId: number) => history.push(`${ADMIN_ROUTES.news}/${newsId}`);

  return (
    <Paper className={classes.root}>
      <div className={classes.iconsWrapper}>
        <IconButton aria-label="delete" className={classes.margin}>
          <BookmarkBorderIcon fontSize="small" />
        </IconButton>
        <IconButton aria-label="edit" className={classes.margin} onClick={() => goToEditNewsPage(id)}>
          <EditIcon fontSize="small" />
        </IconButton>
        <IconButton aria-label="edit" onClick={() => deleteNews(id)} className={classes.margin}>
          <DeleteIcon fontSize="small" />
        </IconButton>
      </div>
      <div className={classes.newsWrapper}>
        <IconButton aria-label="edit" className={classes.newsButton}>
          <ViewHeadlineIcon fontSize="small" />
        </IconButton>
        <div className={classes.imageWrapper}>
          <LazyImage src={imageUrl || newsImage} alt={title} />
        </div>
        <div className={classes.text}>
          <Typography variant="h4" gutterBottom>
            {title}
          </Typography>
          {ReactHtmlParser(body)}
        </div>
      </div>
    </Paper>
  );
};

export default SingleNews;
