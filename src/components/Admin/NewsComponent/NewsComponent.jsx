import React from 'react';
import { useHistory } from 'react-router-dom';

import AddIcon from '@material-ui/icons/Add';

// COMPONENTS
import SingleNews from './SingleNews';
import Button from '../../Button';

// TYPES
import type { SingleMockNews } from '../../../types/newsList';

// STYLES
import useStyles from './styles';

// CONSTANTS
import { ADMIN_ROUTES } from '../../../constants/routes';

type Props = {
  newsList: Array<SingleMockNews>,
  deleteNews: (newsId: string) => void,
  clearSingleNewsData: () => void,
}

const NewsComponent = (props: Props) => {
  const { clearSingleNewsData, newsList, deleteNews } = props;

  const classes = useStyles();
  const history = useHistory();

  const addNewNews = async () => {
    await clearSingleNewsData();
    history.push(ADMIN_ROUTES.addNews);
  };

  return (
    <>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="primary"
          additionalStyle={classes.button}
          startIcon={<AddIcon />}
          onClick={addNewNews}
        >
          Добавить
        </Button>
      </div>
      <div>
        {newsList && newsList.map((news) => (
          <SingleNews
            key={news.id}
            id={news.id}
            title={news.title}
            body={news.text}
            imageUrl={news.images[0]}
            deleteNews={deleteNews}
          />
        ))}
      </div>
    </>
  );
};

export default NewsComponent;
