// @flow

import React from 'react';
import { useHistory } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';

// COMPONENTS
import Button from '../../Button';

// HOOKS
import useForm from '../../../hooks/useForm';

// VALIDATION
import validate from '../../../validation/about';

// STYLES
import useStyles from './styles';

type Props = {
  title: string,
  body: string,
  handleUpdateAboutData: (data: Object) => void,
}

const AboutEditorComponent = (props: Props) => {
  const { title, body, handleUpdateAboutData } = props;

  const classes = useStyles();
  const history = useHistory();

  const goToChurchListPage = () => history.goBack();

  const defaultValues = {
    title,
    editorData: body,
  };

  const {
    values,
    errors,
    handleChange,
    handleBlur,
    isFormHasError,
    handleFocus,
  }: Object = useForm(null, validate, defaultValues);

  const handleSubmit = () => {
    const data = {
      Title: values.title,
      Body: values.editorData,
    };

    handleUpdateAboutData(data);
  };

  return (
    <div>
      <FormControl className={classes.root} noValidate autoComplete="off" fullWidth>
        <TextField
          margin="normal"
          id="outlined-basic"
          label="Заголовок"
          variant="outlined"
          helperText={errors.title}
          value={values.title}
          error={!!errors.title}
          onChange={(event) => handleChange('title', event)}
          onBlur={(event) => handleBlur('title', event)}
          onFocus={() => handleFocus('title')}
        />
        <TextField
          margin="normal"
          multiline
          rows={10}
          rowsMax={15}
          label="Описание"
          value={values.editorData}
          error={!!errors.editorData}
          helperText={errors.editorData}
          variant="outlined"
          onChange={(event) => handleChange('editorData', event)}
          onBlur={(event) => handleBlur('editorData', event)}
          onFocus={() => handleFocus('editorData')}
        />
      </FormControl>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="default"
          additionalStyle={classes.button}
          onClick={() => goToChurchListPage()}
        >
          Отмена
        </Button>
        <Button
          variant="contained"
          color="primary"
          disabled={isFormHasError}
          additionalStyle={classes.button}
          onClick={handleSubmit}
        >
          Сохранить
        </Button>
      </div>
    </div>
  );
};

export default AboutEditorComponent;
