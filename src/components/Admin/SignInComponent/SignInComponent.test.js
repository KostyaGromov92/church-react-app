import React from 'react';
import { shallow } from 'enzyme';
import SignInComponent from './SignInComponent';

describe('<SignInComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SignInComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
