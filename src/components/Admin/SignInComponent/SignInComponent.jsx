// @flow

import React from 'react';

import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

// STYLES
import useStyles from './styles';

// HOOKS
import useForm from '../../../hooks/useForm';
import validate from '../../../validation/signIn';

// COMPONENTS
import Button from '../../Button';

type Props = {
  isFetchingSign: boolean,
  handleSignIn: (userData: Object) => void,
}

const SignInComponent = (props: Props) => {
  const { handleSignIn, isFetchingSign } = props;

  const classes = useStyles();

  const defaultValues = {
    userEmail: '',
    userPassword: '',
  };

  const {
    values,
    errors,
    handleChange,
    handleBlur,
    isFormHasError,
    handleFocus,
  }: Object = useForm(null, validate, defaultValues);

  const submitValue = (event) => {
    event.preventDefault();

    const fields = {
      userName: values.userEmail,
      password: values.userPassword,
    };

    handleSignIn(fields);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography className={classes.signInTitle} component="h1" variant="h5">
          Форма входа в систему
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            value={values.userEmail}
            required
            fullWidth
            error={!!errors.userEmail}
            helperText={errors.userEmail}
            id="email"
            label="Email адресс"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(event) => handleChange('userEmail', event)}
            onBlur={(event) => handleBlur('userEmail', event)}
            onFocus={() => handleFocus('userEmail')}
          />
          <TextField
            variant="outlined"
            margin="normal"
            value={values.userPassword}
            required
            fullWidth
            error={!!errors.userPassword}
            helperText={errors.userPassword}
            name="password"
            label="Пароль"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(event) => handleChange('userPassword', event)}
            onBlur={(event) => handleBlur('userPassword', event)}
            onFocus={() => handleFocus('userPassword')}
          />
          <Button
            fullWidth
            variant="contained"
            color="primary"
            disabled={isFormHasError || isFetchingSign}
            additionalStyle={classes.additionalButton}
            onClick={submitValue}
            withSpinner
            isFetching={isFetchingSign}
          >
            Войти в систему
          </Button>
        </form>
      </div>
    </Container>
  );
};

export default SignInComponent;
