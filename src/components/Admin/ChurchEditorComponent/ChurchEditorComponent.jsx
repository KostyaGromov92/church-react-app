// @flow

import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

// COMPONENTS
import EditorComponent from '../EditorComponent';
import Button from '../../Button';

// HOOKS
import useEditor from '../../../hooks/useEditor';
import useForm from '../../../hooks/useForm';

// VALIDATION
import validate from '../../../validation/churchEditor';

// STYLES
import useStyles from './styles';

type Props = {
  title: string,
  handleUpdateChurchData: (data: Object) => void,
}

const ChurchEditorComponent = (props: Props) => {
  const { title, handleUpdateChurchData } = props;

  const [isToggleChecked, setState] = useState(true);
  const classes = useStyles();
  const history = useHistory();

  const handleToggleChange = () => setState(!isToggleChecked);

  const goToChurchListPage = () => history.goBack();

  const {
    onEditorStateChange,
    isEditorHasText,
    editorState,
  } = useEditor();

  const defaultValues = {
    title,
    subTitle: title,
  };

  const {
    values,
    errors,
    handleChange,
    handleBlur,
    isFormHasError,
    handleFocus,
  }: Object = useForm(null, validate, defaultValues);

  const handleSubmit = () => {
    const data = {
      title: values.title,
    };

    handleUpdateChurchData(data);
  };

  const disabledButton = isToggleChecked ? !!errors.title : isFormHasError || !isEditorHasText;

  return (
    <div>
      <FormControl className={classes.root} noValidate autoComplete="off" fullWidth>
        <div className={classes.inputWrapper}>
          <TextField
            margin="normal"
            id="outlined-basic"
            label="Заголовок"
            variant="outlined"
            helperText={errors.title}
            value={values.title}
            error={!!errors.title}
            onChange={(event) => handleChange('title', event)}
            onBlur={(event) => handleBlur('title', event)}
            onFocus={() => handleFocus('title')}
          />
          {isToggleChecked && <TextField margin="normal" id="outlined-basic" label="Ссылка" variant="outlined" />}
          <TextField
            disabled={isToggleChecked}
            margin="normal"
            id="outlined-basic"
            label="Подзаголовок"
            variant="outlined"
            value={isToggleChecked ? '' : values.subTitle}
            helperText={!isToggleChecked && errors.subTitle}
            error={!isToggleChecked && !!errors.subTitle}
            onChange={(event) => handleChange('subTitle', event)}
            onBlur={(event) => handleBlur('subTitle', event)}
            onFocus={() => handleFocus('subTitle')}
          />
          <EditorComponent
            editorState={editorState}
            isDisabled={isToggleChecked}
            hasError={!isEditorHasText && !isToggleChecked}
            editorHelperText={!isToggleChecked && 'Error'}
            onEditorStateChange={onEditorStateChange}
          />
        </div>
        <div className={classes.toggleWrapper}>
          <FormControlLabel
            value="top"
            control={<Switch color="primary" checked={isToggleChecked} onChange={handleToggleChange} />}
            label="Переадресация"
            labelPlacement="top"
          />
        </div>
      </FormControl>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="default"
          additionalStyle={classes.button}
          onClick={() => goToChurchListPage()}
        >
          Отмена
        </Button>
        <Button
          variant="contained"
          color="primary"
          disabled={disabledButton}
          additionalStyle={classes.button}
          onClick={handleSubmit}
        >
          Сохранить
        </Button>
      </div>
    </div>
  );
};

export default ChurchEditorComponent;
