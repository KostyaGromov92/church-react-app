import React from 'react';
import { shallow } from 'enzyme';
import ChurchEditorComponent from './ChurchEditorComponent';

describe('<ChurchEditorComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<ChurchEditorComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
