import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column-reverse',
    },
  },
  toggleWrapper: {
    marginLeft: '35px',
    [theme.breakpoints.down('sm')]: {
      marginLeft: 0,
      display: 'flex',
      justifyContent: 'center',
    },
  },
  inputWrapper: {
    display: 'flex',
    flexDirection: 'column',
  },
  button: {
    margin: theme.spacing(1),
  },
  buttonWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: '40px',
  },
}));

export default useStyles;
