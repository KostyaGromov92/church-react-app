// @flow

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  toolbar: theme.mixins.toolbar,
  link: {
    color: '#3f51b5',
    '&.active': {
      '& > div': {
        backgroundColor: '#3f51b5',
      },
    },
  },
  drawerPaper: {
    width: props => (props.isAdminUser ? 240 : 0),
  },
}));

export default useStyles;
