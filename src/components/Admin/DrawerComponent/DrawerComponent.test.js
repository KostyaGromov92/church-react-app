import React from 'react';
import { shallow } from 'enzyme';
import DrawerComponent from './DrawerComponent';

describe('<DrawerComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<DrawerComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
