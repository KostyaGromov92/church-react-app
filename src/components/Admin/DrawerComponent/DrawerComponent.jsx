import React from 'react';
import { withRouter, NavLink } from 'react-router-dom';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';

// STYLES
import useStyles from './styles';

type Props = {
  links: Array<Object>,
  variant: string,
  container: Object | Element<*>,
  isOpen?: boolean,
  isAdminUser: boolean,
  modalProps?: Object,
  handleDrawerToggle?: () => void,
}

const DrawerComponent = withRouter((props: Props) => {
  const {
    links,
    variant,
    isOpen,
    modalProps,
    handleDrawerToggle,
    container,
    isAdminUser,
  } = props;

  const classes = useStyles({ isAdminUser });

  return (
    <Drawer
      container={container}
      variant={variant}
      open={isOpen}
      onClose={handleDrawerToggle}
      classes={{
        paper: classes.drawerPaper,
      }}
      ModalProps={modalProps}
    >
      <div className={classes.toolbar} />
      <Divider />
      <List>
        {links.map((link) => (
          <NavLink
            key={link.name}
            to={link.url}
            className={classes.link}
            activeStyle={{ color: 'white' }}
          >
            <ListItem button key={link.name}>
              <ListItemText primary={link.name} />
            </ListItem>
          </NavLink>
        ))}
      </List>
      <Divider />
    </Drawer>
  );
});
export default DrawerComponent;
