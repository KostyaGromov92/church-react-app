import React from 'react';
import { shallow } from 'enzyme';
import SinglePost from './SinglePost';

describe('<SinglePost />', () => {
  test('renders', () => {
    const wrapper = shallow(<SinglePost />);
    expect(wrapper).toMatchSnapshot();
  });
});
