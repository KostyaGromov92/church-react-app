// @flow

import React from 'react';

import { useHistory } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';
import Typography from '@material-ui/core/Typography';
import ReactHtmlParser from 'react-html-parser';

// COMPONENTS
import LazyImage from '../../../LazyImageComponent';

// STYLES
import useStyles from './styles';

type Props = {
  id: number,
  title: string,
  body: string,
  imageUrl: string,
  deletePost: (postId: number) => void,
};

const newsImage = require('../../../../img/archway.jpg');

const SinglePost = (props: Props) => {
  const classes = useStyles();
  const history = useHistory();
  const {
    id, title, body, deletePost, imageUrl,
  } = props;

  const goToEditPostPage = (postId: number) => history.push(`/admin/blog/${postId}`);

  return (
    <Paper className={classes.root}>
      <div className={classes.iconsWrapper}>
        <IconButton aria-label="delete" className={classes.margin}>
          <BookmarkBorderIcon fontSize="small" />
        </IconButton>
        <IconButton aria-label="edit" className={classes.margin} onClick={() => goToEditPostPage(id)}>
          <EditIcon fontSize="small" />
        </IconButton>
        <IconButton aria-label="edit" onClick={() => deletePost(id)} className={classes.margin}>
          <DeleteIcon fontSize="small" />
        </IconButton>
      </div>
      <div className={classes.postsWrapper}>
        <IconButton aria-label="edit" className={classes.postButton}>
          <ViewHeadlineIcon fontSize="small" />
        </IconButton>
        <div className={classes.imageWrapper}>
          <LazyImage src={imageUrl || newsImage} alt={title} />
        </div>
        <div className={classes.text}>
          <Typography variant="h4" gutterBottom>
            {title}
          </Typography>
          {ReactHtmlParser(body)}
        </div>
      </div>
    </Paper>
  );
};

export default SinglePost;
