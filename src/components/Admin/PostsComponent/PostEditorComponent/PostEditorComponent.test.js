import React from 'react';
import { shallow } from 'enzyme';
import PostEditorComponent from './PostEditorComponent';

describe('<PostEditorComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<PostEditorComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
