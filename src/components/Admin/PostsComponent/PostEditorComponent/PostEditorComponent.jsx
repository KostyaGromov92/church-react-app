/* eslint-disable max-len */
// @flow

import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';

// HOOKS
import useForm from '../../../../hooks/useForm';
import useGallery from '../../../../hooks/useGallery';

// COMPONENTS
import GalleryComponent from '../../GalleryComponent';
import FormHelperTextComponent from '../../FormHelperTextComponent';
import Button from '../../../Button';

// VALIDATION
import validate from '../../../../validation/postEditor';

// HELPERS
import { validateGUID } from '../../../../services/helpers';

// STYLES
import useStyles from './styles';

type Props = {
  postId: string,
  title: string,
  subTitle: string,
  body: string,
  imagesList: Array<string>,
  isFetchingSingleImageList: boolean,
  uploadPhoto: (files: Array<Object>) => void,
  handleRemoveImage: (imageUri: string) => void,
  handleUpdatePost: (data: Object) => void,
};

const PostEditorComponent = (props: Props) => {
  const {
    title, subTitle, body, postId, handleUpdatePost, imagesList, uploadPhoto, handleRemoveImage, isFetchingSingleImageList,
  } = props;

  const [localFiles, setFiles] = useState([]);
  const [isShowGalleryAlertMessage, setGalleryAlertMessage] = useState(false);

  const classes = useStyles();
  const history = useHistory();

  const defaultValues = {
    title,
    subTitle,
    editorData: body,
  };

  const {
    values,
    errors,
    handleChange,
    handleBlur,
    isFormHasError,
    handleFocus,
  }: Object = useForm(null, validate, defaultValues);

  const {
    handleChangeGallery,
    imagesListURL,
    isEmptyGallery,
    filesList,
    removeFileImage,
    removeImage,
    imagesListToUpload,
  } = useGallery(imagesList, handleRemoveImage);

  const submitValue = () => {
    const fields = {
      ...(validateGUID(postId) && { id: postId }),
      Title: values.title,
      SubTitle: values.subTitle,
      Text: values.editorData,
      Images: imagesListToUpload.length > 0 ? [...imagesList, ...imagesListToUpload] : imagesList,
    };

    handleUpdatePost(fields);
  };

  const handleUploadImages = (images: Array<Object>) => {
    uploadPhoto(images);
    setGalleryAlertMessage(false);
  };

  const handleChangeForm = (event) => {
    const hasFiles = event && event.target && event.target.files;

    if (hasFiles) {
      const dataFiles = [...localFiles, ...event.target.files];

      setFiles(dataFiles);
      setGalleryAlertMessage(true);
    }
  };

  const goToPostListPage = () => history.goBack();

  return (
    <>
      <FormControl onChange={handleChangeForm} noValidate autoComplete="off" fullWidth>
        <TextField
          margin="normal"
          id="outlined-basic"
          label="Заголовок"
          helperText={errors.title}
          value={values.title}
          variant="outlined"
          error={!!errors.title}
          onChange={(event) => handleChange('title', event)}
          onBlur={(event) => handleBlur('title', event)}
          onFocus={() => handleFocus('title')}
        />
        <TextField
          margin="normal"
          id="outlined-basic"
          label="Подзаголовок"
          value={values.subTitle}
          error={!!errors.subTitle}
          helperText={errors.subTitle}
          variant="outlined"
          onChange={(event) => handleChange('subTitle', event)}
          onBlur={(event) => handleBlur('subTitle', event)}
          onFocus={() => handleFocus('subTitle')}
        />

        <GalleryComponent
          imagesListUri={imagesListURL}
          imageFiles={filesList}
          uploadPhoto={handleUploadImages}
          isFetching={isFetchingSingleImageList}
          handleRemoveImage={handleRemoveImage}
          handleChangeGallery={handleChangeGallery}
          removeFileImage={removeFileImage}
          removeImage={removeImage}
          isEmptyGallery={isEmptyGallery}
        />

        <TextField
          margin="normal"
          multiline
          rows={4}
          rowsMax={15}
          label="Описание"
          value={values.editorData}
          error={!!errors.editorData}
          helperText={errors.editorData}
          variant="outlined"
          onChange={(event) => handleChange('editorData', event)}
          onBlur={(event) => handleBlur('editorData', event)}
          onFocus={() => handleFocus('editorData')}
        />
      </FormControl>
      {isShowGalleryAlertMessage && (
        <FormHelperTextComponent
          hasError={false}
          helperText="Пожалуйста, перед сохранением поста, загрузите Ваши новые фотографии"
        />
      )}
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="default"
          className={classes.button}
          additionalStyle={classes.button}
          onClick={() => goToPostListPage()}
        >
          Отмена
        </Button>
        <Button
          variant="contained"
          color="primary"
          disabled={isFormHasError || isEmptyGallery || isShowGalleryAlertMessage || isFetchingSingleImageList}
          className={classes.button}
          additionalStyle={classes.button}
          onClick={submitValue}
        >
          Сохранить
        </Button>
      </div>
    </>
  );
};

export default PostEditorComponent;
