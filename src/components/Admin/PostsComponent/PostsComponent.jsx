// @flow

// POSTS - COMPONENTS

import React from 'react';
import { useHistory } from 'react-router-dom';

import AddIcon from '@material-ui/icons/Add';

// COMPONENTS
import SinglePost from './SinglePost';
import Button from '../../Button';

// STYLES
import useStyles from './styles';

// CONSTANTS
import { ADMIN_ROUTES } from '../../../constants/routes';

type Props = {
  postList: Array<Object>,
  deletePost: (postId: string) => void,
  clearSinglePostData: () => void,
}

const PostsComponent = (props: Props) => {
  const { clearSinglePostData, postList, deletePost } = props;

  const classes = useStyles();
  const history = useHistory();

  const addNewPost = async () => {
    await clearSinglePostData();
    history.push(ADMIN_ROUTES.addPost);
  };

  return (
    <>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="primary"
          additionalStyle={classes.button}
          startIcon={<AddIcon />}
          onClick={addNewPost}
        >
          Добавить
        </Button>
      </div>
      <div>
        {postList && postList.map((post) => (
          <SinglePost
            key={post.id}
            id={post.id}
            title={post.title}
            body={post.text}
            imageUrl={post.images[0]}
            deletePost={deletePost}
          />
        ))}
      </div>
    </>
  );
};

export default PostsComponent;
