import React from 'react';
import { shallow } from 'enzyme';
import PostsComponent from './PostsComponent';

describe('<PostsComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<PostsComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
