// @flow

import React, { Component } from 'react';

type State = {
  hasError: boolean,
  error: ?Object
}

class ErrorBoundary extends Component<*, State> {
  constructor(props: any) {
    super(props);
    this.state = { hasError: false, error: null };
  }

  static getDerivedStateFromError(error: Object) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, error };
  }

  // componentDidCatch(error, info) {
  //   // You can also log the error to an error reporting service
  //   logErrorToMyService(error, info);
  // }

  render() {
    const { children } = this.props;
    const { hasError, error } = this.state;

    if (hasError) {
      // You can render any custom fallback UI
      return (
        <h1>
          Something went wrong.:
          {error}
        </h1>
      );
    }

    return children;
  }
}

export default ErrorBoundary;
