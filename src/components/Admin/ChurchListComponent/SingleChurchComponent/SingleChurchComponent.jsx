import React from 'react';
import { useHistory } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Paper from '@material-ui/core/Paper';

// STYLES
import useStyles from './styles';

type Props = {
  title: string,
  churchId: number,
  deleteChurchHandler: (churchId: number) => void,
}

const SingleChurchComponent = (props: Props) => {
  const { title, churchId, deleteChurchHandler } = props;

  const classes = useStyles();
  const history = useHistory();

  const goToEditChurchPage = (id: number) => history.push(`/admin/church/${id}`);

  return (
    <Paper className={classes.root}>
      <Typography variant="h5">
        {title}
      </Typography>
      <div className={classes.buttonsWrapper}>
        <IconButton onClick={() => deleteChurchHandler(churchId)} aria-label="delete">
          <DeleteIcon fontSize="small" />
        </IconButton>
        <IconButton onClick={() => goToEditChurchPage(churchId)} aria-label="edit">
          <EditIcon fontSize="small" />
        </IconButton>
      </div>
    </Paper>
  );
};

export default SingleChurchComponent;
