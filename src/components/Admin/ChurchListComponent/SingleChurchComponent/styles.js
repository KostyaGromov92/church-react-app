// @flow

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1, 2),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: '25px',
  },
  buttonsWrapper: {
    minWidth: '95px',
    display: 'flex',
    flexDirection: 'row',
    marginLeft: '10px',
    [theme.breakpoints.down('sm')]: {
      minWidth: 'initial',
      flexDirection: 'column',
    },
  },
}));

export default useStyles;
