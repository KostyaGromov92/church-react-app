import React from 'react';
import { shallow } from 'enzyme';
import SingleChurchComponent from './SingleChurchComponent';

describe('<SingleChurchComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SingleChurchComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
