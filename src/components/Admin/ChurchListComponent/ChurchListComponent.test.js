import React from 'react';
import { shallow } from 'enzyme';
import ChurchListComponent from './ChurchListComponent';

describe('<ChurchListComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<ChurchListComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
