import React from 'react';

import AddIcon from '@material-ui/icons/Add';

// COMPONENTS
import SingleChurchComponent from './SingleChurchComponent';
import Button from '../../Button';

// STYLES
import useStyles from './styles';

// TYPES
import type { SingleChurch } from '../../../types/churchList';

type Props = {
  churchList: Array<SingleChurch>,
  deleteChurchHandler: (churchId: number) => void,
  handleClearSingleChurchData: () => void,
}

const ChurchListComponent = (props: Props) => {
  const { churchList, deleteChurchHandler } = props;
  const classes = useStyles();
  const { handleClearSingleChurchData } = props;

  return (
    <React.Fragment>
      <div className={classes.buttonWrapper}>
        <Button
          variant="contained"
          color="primary"
          additionalStyle={classes.button}
          startIcon={<AddIcon />}
          onClick={handleClearSingleChurchData}
        >
          Добавить
        </Button>
      </div>
      {churchList && churchList.length > 0 && churchList.map(singleChurch => (
        <SingleChurchComponent
          key={singleChurch.id}
          deleteChurchHandler={deleteChurchHandler}
          churchId={singleChurch.id}
          title={singleChurch.title}
        />
      ))}
    </React.Fragment>
  );
};

export default ChurchListComponent;
