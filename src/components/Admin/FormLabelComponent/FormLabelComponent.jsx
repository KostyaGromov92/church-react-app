// @flow
// FORM LABEL - COMPONENT

import React from 'react';

import clsx from 'clsx';
import FormLabel from '@material-ui/core/FormLabel';

// STYLES
import useStyles from './styles';

type Props = {
  labelText: string,
  additionalStyle?: ?Object
}

const FormLabelComponent = (props: Props) => {
  const { labelText, additionalStyle } = props;

  const classes = useStyles();

  return (
    <FormLabel
      className={clsx(classes.labelText, additionalStyle)}
    >
      {labelText}
    </FormLabel>
  );
};

FormLabelComponent.defaultProps = {
  additionalStyle: null,
};

export default FormLabelComponent;
