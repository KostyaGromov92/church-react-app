import React from 'react';
import { shallow } from 'enzyme';
import FormLabelComponent from './FormLabelComponent';

describe('<FormLabelComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<FormLabelComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
