// @flow

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  labelText: {
    marginBottom: '10px',
  },
}));

export default useStyles;
