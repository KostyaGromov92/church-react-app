import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  textArea: {
    resize: 'none',
    borderRadius: '4px',
    padding: '5px',
    minHeight: '150px',
  },
  textAreaWithError: {
    borderColor: theme.palette.error.main,
  },
  errorLabel: {
    color: theme.palette.error.main,
  },
}));

export default useStyles;
