import React from 'react';

import clsx from 'clsx';

import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import FormLabel from '@material-ui/core/FormLabel';

// COMPONENTS
import FormHelperTextComponent from '../FormHelperTextComponent';

// STYLES
import useStyles from './styles';

type Props = {
  title?: ?string,
  rowsMax: number,
  hasError: boolean,
  errorText?: ?string,
  defaultValue: ?string,
  onChange?: () => void,
  onBlur?: () => void,
  onFocus?: () => void,
}

const TextArea = (props: Props) => {
  const {
    title, rowsMax, defaultValue, onChange, onBlur, onFocus, hasError, errorText,
  } = props;

  const classes = useStyles();

  return (
    <>
      {title && <FormLabel className={clsx(hasError && classes.errorLabel)}>{title}</FormLabel>}
      <TextareaAutosize
        className={clsx(classes.textArea, hasError && classes.textAreaWithError)}
        aria-label="minimum height"
        rowsMax={rowsMax}
        defaultValue={defaultValue}
        onChange={onChange}
        onBlur={onBlur}
        onFocus={onFocus}
      />
      {hasError && errorText && (
        <FormHelperTextComponent
          hasError
          helperText={errorText}
        />
      )}
    </>
  );
};

TextArea.defaultProps = {
  title: null,
  errorText: null,
  onChange: () => {},
  onBlur: () => {},
  onFocus: () => {},
};

export default TextArea;
