// @flow

import React from 'react';
import AppBar from '@material-ui/core/AppBar';

import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';

// STYLES
import useStyles from './styles';

type Props = {
  handleDrawerToggle: () => void,
}

const HeaderComponent = (props: Props) => {
  const { handleDrawerToggle } = props;
  const classes = useStyles();

  return (
    <div>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            Провославие
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default HeaderComponent;
