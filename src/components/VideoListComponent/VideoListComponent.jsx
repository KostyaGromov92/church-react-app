import React from 'react';
import { Link } from 'react-router-dom';

// STYLES
import './VideoListComponent.scss';

// CONSTANT
import { MAIN_ROUTES } from '../../constants/routes';

type Props = {
  videoList: Array<Object>,
  showMoreVideoLink: boolean,
};

const VideoListComponent = ({ videoList, showMoreVideoLink }: Props) => (
  <section className="video">
    <div className="title__with-line">
      <h2>Видео</h2>
      <hr />
    </div>
    <div className="video__list">
      {videoList && videoList.map((video) => (
        <Link to={`${MAIN_ROUTES.video}/${video.id}`} key={video.id}>
          <article className="video__item" key={video.title}>
            <div className="video__block">
              <iframe
                width="100%"
                title="Video"
                style={{ height: 'auto', maxWidth: '100%' }}
                src={video.link}
              />
            </div>
            <div className="video__content">
              <h3 className="video__title">{video.title}</h3>
              <p className="video__description">
                {video.text}
              </p>
            </div>
          </article>
        </Link>
      ))}

    </div>
    {videoList !== 0 && showMoreVideoLink
      && <Link to={MAIN_ROUTES.video} className="video__link">Больше видео</Link>}
  </section>
);

export default VideoListComponent;
