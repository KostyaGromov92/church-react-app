import React from 'react';
import { shallow } from 'enzyme';
import VideoListComponent from './VideoListComponent';

describe('<VideoListComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<VideoListComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
