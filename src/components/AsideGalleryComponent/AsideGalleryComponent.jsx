// @flow

import React from 'react';
import { Link } from 'react-router-dom';

// COMPONENTS
import SliderComponent from '../SliderComponent';
import LazyImage from '../LazyImageComponent';

// CONSTANTS
import { MAIN_ROUTES } from '../../constants/routes';

// STYLES
import './AsideGalleryComponent.scss';

const churchGallery = require('../../img/church-gallery.jpg');

type Props = {
  photoGalleryList: Array<Object>,
}

const AsideGalleryComponent = (props: Props) => {
  const { photoGalleryList } = props;

  return (
    <div className="gallery">
      <div className="gallery__title-block">
        <h3 className="gallery__title">
          <Link to={MAIN_ROUTES.gallery}>Фотоголерея</Link>
        </h3>
      </div>
      <SliderComponent arrowsBlockClassName="arrows-slider_photo-gallery">
        {photoGalleryList && photoGalleryList.map((photoGallery) => (
          <Link to={`${MAIN_ROUTES.gallery}/${photoGallery.id}`} key={photoGallery.id}>
            <div className="gallery__item" key={photoGallery.id}>
              <LazyImage src={photoGallery.images[0] || churchGallery} alt={photoGallery.text} />
              <p className="gallery__text">
                {photoGallery.text}
              </p>
            </div>
          </Link>
        ))}
      </SliderComponent>
    </div>
  );
};

export default AsideGalleryComponent;
