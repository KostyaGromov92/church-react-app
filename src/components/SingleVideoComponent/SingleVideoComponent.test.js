import React from 'react';
import { shallow } from 'enzyme';
import SingleVideoComponent from './SingleVideoComponent';

describe('<SingleVideoComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SingleVideoComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
