// @flow

// SINGLE VIDEO - COMPONENT

import React from 'react';

// STYLES
import './SingleVideoComponent.scss';

type Props = {
  title: string,
  body: string,
  videoLink: string,
}

const SingleVideoComponent = (props: Props) => {
  const {
    title,
    body,
    videoLink,
  } = props;

  return (
    <section className="single-video">
      <div className="single-video__title title__with-line">
        <h2>Видео</h2>
        <hr />
      </div>
      <div className="single-video__frame">
        <iframe
          width="100%"
          title="Video"
          style={{ height: '100%', maxWidth: '100%' }}
          src={videoLink}
        />
      </div>
      <h2 className="single-video__title-video">{title}</h2>

      <div className="single-video__description">
        {body}
      </div>
    </section>
  );
};

export default SingleVideoComponent;
