// @flow

import React from 'react';
import ReactHtmlParser from 'react-html-parser';

// STYLES
import './History.scss';

type Props = {
  title: string,
  body: string,
}

const HistoryComponent = (props: Props) => {
  const {
    title,
    body,
  } = props;

  return (
    <section className="history">
      <div className="history__title title__with-line">
        <h2>История</h2>
        <hr />
      </div>
      <h3 className="history__sub_title">{title}</h3>

      <div className="history__description">
        {ReactHtmlParser(body)}
      </div>
    </section>
  );
};

export default HistoryComponent;
