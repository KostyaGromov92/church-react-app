import React from 'react';
import { shallow } from 'enzyme';
import SliderIconComponent from './SliderIconComponent';

describe('<SliderIconComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SliderIconComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
