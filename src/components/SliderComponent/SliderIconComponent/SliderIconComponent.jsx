import React from 'react';

import './SliderIconComponent.scss';

const SliderIconComponent = (props: Object) => {
  const {
    direction, styleClassName, clickHandler, iconSrc,
  } = props;

  return (
    <img
      className={`slick-arrow--${direction} slider-arrow--${direction} ${styleClassName}`}
      onClick={() => clickHandler(direction)}
      onKeyPress={() => clickHandler(direction)}
      alt="Icon"
      src={iconSrc}
      role="presentation"
    />
  );
};

export default SliderIconComponent;
