import React, { useState, useRef, useEffect } from 'react';
import Slider from 'react-slick';

import SliderIconComponent from './SliderIconComponent';

type Props = {
  children: Array<*> | Object,
  sliderSetting?: Object,
  arrowsBlockClassName?: string
}

const arrowLeftArrow = require('../../img/icons/left-arrow.svg');
const arrowRightArrow = require('../../img/icons/right-arrow.svg');

const defaultSliderSettings = {
  dots: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  nextArrow: null,
  prevArrow: null,
  className: 'slider',
  centerPadding: '40px',

  responsive: [
    {
      breakpoint: 1280,
      settings: {
        arrows: false,
        centerMode: false,
        centerPadding: '40px',
        slidesToShow: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: false,
        centerPadding: '40px',
        slidesToShow: 1,
      },
    },
  ],
};

const SliderComponent = (props: Props) => {
  const { children, sliderSetting, arrowsBlockClassName } = props;
  const slider = useRef(null);
  const [sliderItem, setSlider] = useState(null);

  useEffect(() => {
    setSlider(slider);
  }, []);

  const clickHandler = (direction) => {
    if (direction === 'left') {
      sliderItem.current.slickPrev();
    } else if (direction === 'right') {
      sliderItem.current.slickNext();
    }
  };

  return (
    <React.Fragment>
      <div className={`arrows-slider ${arrowsBlockClassName}`}>
        <SliderIconComponent
          styleClassName="arrow-slider arrow-slider_left arrow-slider_main"
          direction="right"
          clickHandler={clickHandler}
          iconSrc={arrowLeftArrow}
        />
        <SliderIconComponent
          styleClassName="arrow-slider arrow-slider_right arrow-slider_main"
          direction="left"
          clickHandler={clickHandler}
          iconSrc={arrowRightArrow}
        />
      </div>
      <Slider ref={slider} {...sliderSetting}>
        {children}
      </Slider>
    </React.Fragment>
  );
};

SliderComponent.defaultProps = {
  sliderSetting: defaultSliderSettings,
  arrowsBlockClassName: '',
};

export default SliderComponent;
