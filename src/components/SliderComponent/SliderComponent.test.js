import React from 'react';
import { shallow } from 'enzyme';
import SliderComponent from './SliderComponent';

describe('<SliderComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SliderComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
