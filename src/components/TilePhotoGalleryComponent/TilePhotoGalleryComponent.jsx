// eslint-disable-next-line max-len
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions,jsx-a11y/click-events-have-key-events */
// @flow

import React, { useState, useCallback } from 'react';
import ImageMasonry from 'react-image-masonry';

import Carousel, { Modal, ModalGateway } from 'react-images';

// HOOKS
import useWindowSize from '../../hooks/useWindowSize';

// STYLES
import './TilePhotoGalleryComponent.scss';

type Props = {
  photos: Array<any>
};

const TilePhotoGalleryComponent = (props: Props) => {
  const { photos } = props;
  const windowSize: Object = useWindowSize();

  const [currentImage, setCurrentImage] = useState(0);
  const [viewerIsOpen, setViewerIsOpen] = useState(false);

  const openLightbox = useCallback((event, index) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);

  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };

  const photosList = photos && photos.length > 0 ? photos.map((singleImage) => ({
    src: singleImage,
  })) : [];

  let countColsForGallery = 3;

  if (windowSize.width <= 768) {
    countColsForGallery = 2;
  }

  if (windowSize.width <= 640) {
    countColsForGallery = 1;
  }

  return (
    <div className="single-gallery__tiles">
      <ImageMasonry numCols={countColsForGallery} containerWidth="100%">
        {photos && photos.map((image) => (
          <img
            key={image}
            src={image}
            alt={image}
            onClick={openLightbox}
          />
        ))}
      </ImageMasonry>
      <ModalGateway>
        {viewerIsOpen && (
          <Modal onClose={closeLightbox}>
            <Carousel
              currentIndex={currentImage}
              views={photosList.map((x: Object) => ({
                ...x,
                srcset: x.srcSet,
                caption: x.title,
              }))}
            />
          </Modal>
        )}
      </ModalGateway>
    </div>
  );
};

export default TilePhotoGalleryComponent;
