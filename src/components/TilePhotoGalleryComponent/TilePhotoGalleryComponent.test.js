import React from 'react';
import { shallow } from 'enzyme';
import TilePhotoGalleryComponent from './TilePhotoGalleryComponent';

describe('<TilePhotoGalleryComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<TilePhotoGalleryComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
