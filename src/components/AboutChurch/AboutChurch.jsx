// @flow

import React from 'react';
import ReactHtmlParser from 'react-html-parser';

// STYLES
import './AboutChurch.scss';

type Props = {
  title: string,
  body: string,
}

const AboutChurchComponent = (props: Props) => {
  const {
    title,
    body,
  } = props;

  return (
    <section className="about-church">
      <div className="about-church__title title__with-line">
        <h2>О Церкви</h2>
        <hr />
      </div>
      <h3 className="about-church__sub_title">{title}</h3>

      <div className="about-church__description">
        {ReactHtmlParser(body)}
      </div>
    </section>
  );
};

export default AboutChurchComponent;
