import React from 'react';
import { shallow } from 'enzyme';
import NewsListItemComponent from './NewsListItemComponent';

describe('<NewsListItemComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<NewsListItemComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
