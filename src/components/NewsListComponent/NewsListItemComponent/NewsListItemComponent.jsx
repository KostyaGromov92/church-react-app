// @flow

import React from 'react';
import { Link } from 'react-router-dom';

import ReactHtmlParser from 'react-html-parser';

// STYLES
import './NewsListItemComponent.scss';

// CONSTANTS
import { MAIN_ROUTES } from '../../../constants/routes';

import LazyImage from '../../LazyImageComponent';

type Props = {
  newsId: string,
  title: string,
  body: string,
  newsImageUrl: string,
}

const NewsListItemComponent = (props: Props) => {
  const {
    newsId,
    title,
    body,
    newsImageUrl,
  } = props;

  return (
    <Link to={`${MAIN_ROUTES.news}/${newsId}`}>
      <article className="news__item">
        <div className="news__content">
          <h3 className="news__title">{title}</h3>
          {ReactHtmlParser(body)}
          <time className="news__date" dateTime="2019-09-21 18:48">21 сентября , 18:48</time>
        </div>
        <div className="news__image">
          <LazyImage src={newsImageUrl} alt={title} />
        </div>
      </article>
    </Link>
  );
};

export default NewsListItemComponent;
