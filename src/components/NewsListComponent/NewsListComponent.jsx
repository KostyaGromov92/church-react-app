// @flow

import React from 'react';
import { Link } from 'react-router-dom';

// STYLES
import './NewsListComponent.scss';

// CONSTANTS
import { MAIN_ROUTES } from '../../constants/routes';

// COMPONENTS
import NewsListItemComponent from './NewsListItemComponent';

type Props = {
  newsList: Array<Object>,
};

const NewsListComponent = (props: Props) => {
  const { newsList } = props;

  return (
    <section className="news">
      <div className="title__with-line">
        <h2>Новости</h2>
        <hr />
      </div>
      {newsList && newsList.map((singleNews) => (
        <NewsListItemComponent
          key={singleNews.id}
          newsId={singleNews.id}
          title={singleNews.title}
          body={singleNews.text}
          newsImageUrl={singleNews.images[0]}
        />
      ))}
      {newsList && newsList.length > 0
        && <Link to={MAIN_ROUTES.news} className="news__link">Больше новостей</Link>}
    </section>
  );
};

export default NewsListComponent;
