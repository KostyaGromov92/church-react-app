import React from 'react';
import { shallow } from 'enzyme';
import NewsListComponent from './NewsListComponent';

describe('<NewsListComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<NewsListComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
