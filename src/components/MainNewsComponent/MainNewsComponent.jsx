import React from 'react';

// COMPONENTS
import SliderComponent from '../SliderComponent';
import LazyImageComponent from '../LazyImageComponent';

// STYLES
import './MainNewsComponent.scss';

const prayerImage = require('../../img/prayer.jpg');
const churchWindowImage = require('../../img/church-window.jpg');
const teaLightsImage = require('../../img/tea-lights.jpg');

const MainNewsComponent = () => (
  <section className="main-news">
    <div className="main-news__title title__with-line">
      <h2>Главное</h2>
      <hr />
    </div>
    <SliderComponent arrowsBlockClassName="arrows-slider_main">
      <div className="main-news__block">
        <article className="main-news-item main-news-item_big">
          <LazyImageComponent src={churchWindowImage} alt="Church window" />
          <div className="main-news-item__content">
            <p className="main-news-item__description">
            У Херсоні в жовтні пройдуть щорічні збори тюремних
            священників УПЦ
            </p>
          </div>
          <div className="main-news-item-border" />
        </article>
        <div className="main-news__group">
          <article className="main-news-item">
            <img src={prayerImage} alt="Player" />
            <div className="main-news-item__content">
              <p className="main-news-item__description">
              Архієпископ Боярський Феодосій взяв участь у прийомі
              посольства Республіки Вірменія
              </p>
            </div>
            <div className="main-news-item-border" />
          </article>
          <article className="main-news-item">
            <img src={teaLightsImage} alt="Tea lights" />
            <div className="main-news-item__content">
              <p className="main-news-item__description">
              КИЇВ. Блаженніший Митрополит Онуфрій очолив Божественну
              літургію у Києво- Печерській Лаврі
              </p>
            </div>
            <div className="main-news-item-border" />
          </article>
        </div>
      </div>
      <div className="main-news__block">
        <article className="main-news-item main-news-item_big">
          <LazyImageComponent src={churchWindowImage} alt="Church window" />
          <div className="main-news-item__content">
            <p className="main-news-item__description">
            У Херсоні в жовтні пройдуть щорічні збори тюремних
            священників УПЦ
            </p>
          </div>
          <div className="main-news-item-border" />
        </article>
        <div className="main-news__group">
          <article className="main-news-item">
            <img src={prayerImage} alt="Player" />
            <div className="main-news-item__content">
              <p className="main-news-item__description">
              Архієпископ Боярський Феодосій взяв участь у прийомі
              посольства Республіки Вірменія
              </p>
            </div>
            <div className="main-news-item-border" />
          </article>
          <article className="main-news-item">
            <img src={teaLightsImage} alt="Tea lights" />
            <div className="main-news-item__content">
              <p className="main-news-item__description">
              КИЇВ. Блаженніший Митрополит Онуфрій очолив Божественну
              літургію у Києво- Печерській Лаврі
              </p>
            </div>
            <div className="main-news-item-border" />
          </article>
        </div>
      </div>
    </SliderComponent>
  </section>
);

export default MainNewsComponent;
