import React from 'react';
import { shallow } from 'enzyme';
import MainNewsComponent from './MainNewsComponent';

describe('<MainNewsComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<MainNewsComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
