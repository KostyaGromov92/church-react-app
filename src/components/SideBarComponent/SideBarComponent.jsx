// @flow

import React from 'react';

// COMPONENTS
import AsideGalleryComponent from '../AsideGalleryComponent';
import Pilgrimage from '../Pilgrimage';
import Donation from '../Donation';

import './SideBarComponent.scss';

const SideBarComponent = () => (
  <aside className="aside-block">
    <AsideGalleryComponent />
    <Pilgrimage />
    <Donation />


  </aside>
);

export default SideBarComponent;
