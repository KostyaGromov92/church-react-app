import React from 'react';
import { shallow } from 'enzyme';
import SideBarComponent from './SideBarComponent';

describe('<SideBarComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<SideBarComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
