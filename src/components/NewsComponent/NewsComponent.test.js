import React from 'react';
import { shallow } from 'enzyme';
import NewsComponent from './NewsComponent';

describe('<NewsComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<NewsComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
