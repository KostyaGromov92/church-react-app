// @flow

import React from 'react';

// COMPONENTS
import NewsItemComponent from './NewsItemComponent';

// STYLES
import './NewsComponent.scss';

type Props = {
  newsList: Array<Object>
}

const NewsComponent = (props: Props) => {
  const { newsList } = props;

  return (
    <section className="news-block">
      <div className="news-block__title title__with-line">
        <h2>Новости</h2>
        <hr />
      </div>
      {newsList && newsList.map((news) => (
        <NewsItemComponent
          mainImageUrl={news.images[0]}
          newsId={news.id}
          key={news.id}
          title={news.title}
          body={news.text}
        />
      ))}
    </section>
  );
};

export default NewsComponent;
