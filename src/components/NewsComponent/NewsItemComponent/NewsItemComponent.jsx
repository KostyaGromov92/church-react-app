// @flow

import React from 'react';
import { Link } from 'react-router-dom';

import ReactHtmlParser from 'react-html-parser';

// CONSTANTS
import { MAIN_ROUTES } from '../../../constants/routes';

// STYLES
import './NewsItemComponent.scss';

// COMPONENTS
import LazyImage from '../../LazyImageComponent';

const newsPlaceholderImage = require('../../../img/archway.jpg');

type Props = {
  newsId: number,
  title: string,
  body: string,
  mainImageUrl: string,
}

const NewsItemComponent = (props: Props) => {
  const {
    newsId,
    title,
    body,
    mainImageUrl,
  } = props;

  return (
    <Link to={`${MAIN_ROUTES.news}/${newsId}`} className="news-block__item">
      <div className="news-block__image">
        <LazyImage src={mainImageUrl || newsPlaceholderImage} alt={mainImageUrl} />
      </div>
      <div className="news-block__description">
        <h3 className="news-block__title">{title}</h3>
        {ReactHtmlParser(body)}
      </div>
    </Link>
  );
};

export default NewsItemComponent;
