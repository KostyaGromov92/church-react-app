import React from 'react';
import { shallow } from 'enzyme';
import NewsItemComponent from './NewsItemComponent';

describe('<NewsItemComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<NewsItemComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
