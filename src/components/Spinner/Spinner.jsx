// @flow

// SPINNER - COMPONENT

import React from 'react';

// STYLES
import './Spinner.scss';

const Spinner = () => <div className="loader" />;

export default Spinner;
