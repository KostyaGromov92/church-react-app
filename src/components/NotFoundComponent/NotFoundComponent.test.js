import React from 'react';
import { shallow } from 'enzyme';
import NotFoundComponent from './NotFoundComponent';

describe('<NotFoundComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<NotFoundComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
