/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import HeaderComponent from './HeaderComponent';

describe('<HeaderComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<HeaderComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
