import React from 'react';

// COMPONENTS
import MenuComponent from '../MenuComponent';

// STYLES
import './HeaderComponent.scss';

const crunchImage = require('../../img/header-image.jpg');

const HeaderComponent = () => (
  <header className="header">
    <div className="container">
      <MenuComponent />
      <div className="header-image">
        <img src={crunchImage} alt="Church" />
      </div>
      <div className="header-title">
        <h1>Украинская Православная Церковь</h1>
      </div>
    </div>
  </header>
);

export default HeaderComponent;
