import React from 'react';

import './Footer.scss';

const youtubeIcon = require('../../img/icons/youtube.svg');
const twitterIcon = require('../../img/icons/twitter.svg');
const facebookIcon = require('../../img/icons/facebook.svg');

const Footer = () => (


  <div className="container">
    <footer>
      <ul className="social">
        <li className="social__item youtube">
          <a href="/home" className="social__link">
            <img src={youtubeIcon} alt="YouTube" />
          </a>
        </li>
        <li className="social__item twitter">
          <a href="/home" className="social__link">
            <img src={twitterIcon} alt="Twitter" />
          </a>
        </li>
        <li className="social__item facebook">
          <a href="/home" className="social__link">
            <img src={facebookIcon} alt="facebook" />
          </a>
        </li>
      </ul>
      <div className="copyrignt">
        <p className="copyrignt__text">
         © 2019 Офіційний сайт Украинская Православная Церковь
        </p>
      </div>
    </footer>
  </div>
);

export default Footer;
