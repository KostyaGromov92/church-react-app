// @flow

import React from 'react';

import { LazyLoadImage } from 'react-lazy-load-image-component';
// eslint-disable-next-line import/no-unresolved
import 'react-lazy-load-image-component/src/effects/blur.css';

type Props = {
  src: string,
  alt: string
}

// eslint-disable-next-line react/prop-types
const LazyImage = ({ src, alt }: Props) => (
  <LazyLoadImage
    alt={alt}
    effect="blur"
    src={src}
  />
);

export default LazyImage;
