import React from 'react';
import { shallow } from 'enzyme';
import LazyImageComponent from './LazyImageComponent';

describe('<LazyImageComponent />', () => {
  test('renders', () => {
    const wrapper = shallow(<LazyImageComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
