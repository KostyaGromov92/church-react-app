// @flow

import React from 'react';
import ReactHtmlParser from 'react-html-parser';

// STYLES
import './About.scss';

type Props = {
  title: string,
  body: string,
}

const AboutComponent = (props: Props) => {
  const {
    title,
    body,
  } = props;

  return (
    <section className="about">
      <div className="about__title title__with-line">
        <h2>О нас</h2>
        <hr />
      </div>
      <h3 className="about__sub_title">{title}</h3>

      <div className="about__description">
        {ReactHtmlParser(body)}
      </div>
    </section>
  );
};

export default AboutComponent;
