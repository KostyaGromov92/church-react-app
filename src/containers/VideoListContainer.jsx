/* eslint-disable max-len */
// @flow

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router';

// COMPONENTS
import Spinner from '../components/Spinner';
import VideoListComponent from '../components/VideoListComponent';

// ACTIONS
import { setCurrentBreadCrumb } from '../actions/breadcrumbs';
import { getVideoListRequest } from '../actions/videoList';

// SELECTORS
import { getIsFetchingVideoList, getLastThreeVideo } from '../selectors/videoList';

// CONSTANTS
import { MAIN_ROUTES } from '../constants/routes';

const VideoListContainer = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const videoList = useSelector((state) => getLastThreeVideo(state));
  const isFetchingVideo = useSelector((state) => getIsFetchingVideoList(state));

  const showMoreVideoLink = location.pathname !== MAIN_ROUTES.video && videoList.length > 3;

  useEffect(() => {
    dispatch(getVideoListRequest());
    dispatch(setCurrentBreadCrumb(''));

    return () => {
      dispatch(setCurrentBreadCrumb('Видео'));
    };
  }, []);

  return isFetchingVideo
    ? <Spinner />
    : <VideoListComponent showMoreVideoLink={showMoreVideoLink} videoList={videoList} />;
};

export default VideoListContainer;
