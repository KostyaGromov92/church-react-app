/* eslint-disable max-len */
// @flow

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// COMPONENTS
import AsideGalleryComponent from '../components/AsideGalleryComponent';
import Pilgrimage from '../components/Pilgrimage';
import Donation from '../components/Donation';
import Spinner from '../components/Spinner';

// SELECTORS
import { getPhotoGalleryList, getIsFetchingGalleryList } from '../selectors/gallery';

// ACTIONS
import { getPhotoGalleryRequest } from '../actions/gallery';

const SideBarContainer = () => {
  const dispatch = useDispatch();

  const photoGalleryList = useSelector((state) => getPhotoGalleryList(state));
  const isFetchingGalleryList = useSelector((state) => getIsFetchingGalleryList(state));

  useEffect(() => {
    dispatch(getPhotoGalleryRequest());
  }, []);

  return (
    <aside className="aside-block">
      {isFetchingGalleryList ? <Spinner /> : <AsideGalleryComponent photoGalleryList={photoGalleryList} />}
      <Pilgrimage />
      <Donation />
    </aside>
  );
};

export default SideBarContainer;
