/* eslint-disable max-len */
// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

import validate from 'uuid-validate';

// SELECTORS
import {
  getIsFetchingSingleNews, getSingleNewsTitle, getSingleNewsBody, getSingleNewsSubTitle, getSingleNewsImages,
} from '../../selectors/singleNews';
import { getIsFetchingImageUriList } from '../../selectors/imageFiles';

// ACTIONS
import { getSingleNewsRequest, handleSubmitSingleNewsRequest } from '../../actions/singleNews';
import { removeImageRequest, uploadImagesRequest } from '../../actions/imageFiles';

// COMPONENTS
import NewsEditorComponent from '../../components/Admin/NewsComponent/NewsEditor';
import Spinner from '../../components/Admin/Spinner';

const AdminNewsEditorContainer = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const isFetchingSingleNews = useSelector((state) => getIsFetchingSingleNews(state));
  const singleNewsTitle = useSelector((state) => getSingleNewsTitle(state));
  const singleNewsSubTitle = useSelector((state) => getSingleNewsSubTitle(state));
  const singleNewsBody = useSelector((state) => getSingleNewsBody(state));
  const singleNewsImages = useSelector((state) => getSingleNewsImages(state));
  const isFetchingSingleImageList = useSelector((state) => getIsFetchingImageUriList(state));

  const handleUpdateNews = (newsData: Object) => {
    dispatch(handleSubmitSingleNewsRequest(newsData));
  };

  const handleUploadPhoto = (photos: Array<Object>) => dispatch(uploadImagesRequest({ photos }));
  const handleRemoveImage = (imageUri: string) => dispatch(removeImageRequest(imageUri));

  useEffect(() => {
    if (validate(id)) dispatch(getSingleNewsRequest({ newsId: id }));
  }, []);

  return isFetchingSingleNews
    ? <Spinner />
    : (
      <NewsEditorComponent
        handleUpdateNews={handleUpdateNews}
        uploadPhoto={handleUploadPhoto}
        handleRemoveImage={handleRemoveImage}
        newsId={id}
        title={singleNewsTitle}
        subTitle={singleNewsSubTitle}
        body={singleNewsBody}
        imagesList={singleNewsImages}
        isFetchingSingleImageList={isFetchingSingleImageList}
      />
    );
};

export default AdminNewsEditorContainer;
