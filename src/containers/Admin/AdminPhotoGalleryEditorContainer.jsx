// @flow
import React, { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

import validate from 'uuid-validate';

// COMPONENTS
import Spinner from '../../components/Admin/Spinner';

// ACTIONS
import { uploadImagesRequest, removeImageRequest } from '../../actions/imageFiles';
import { getSingleGalleryDataRequest, updateSingleGalleryDataRequest } from '../../actions/singleGallery';

// COMPONENTS
import PhotoGalleryEditorComponent from '../../components/Admin/PhotoGalleryComponent/PhotoGalleryEditor';

// SELECTORS
import {
  getIsFetchingSingleGalleryData,
  getSingleGalleryTitle,
  getSingleGalleryBody,
  getGalleryImagesUriList,
} from '../../selectors/singleGallery';
import { getIsFetchingImageUriList } from '../../selectors/imageFiles';


const AdminPhotoGalleryEditorContainer = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const imagesListUri = useSelector((state) => getGalleryImagesUriList(state));
  const isFetchingData = useSelector((state) => getIsFetchingSingleGalleryData(state));
  const titleGallery = useSelector((state) => getSingleGalleryTitle(state));
  const bodyGallery = useSelector((state) => getSingleGalleryBody(state));
  const isFetchingSingleImageList = useSelector((state) => getIsFetchingImageUriList(state));

  useEffect(() => {
    if (validate(id)) dispatch(getSingleGalleryDataRequest({ galleryId: id }));
  }, []);

  const handleUpdateGallery = (galleryData: Object) => {
    dispatch(updateSingleGalleryDataRequest(galleryData));
  };

  const handleUploadPhoto = (photos: Array<Object>) => dispatch(uploadImagesRequest({ photos }));
  const handleRemoveImage = (imageUri: string) => dispatch(removeImageRequest(imageUri));

  return isFetchingData
    ? <Spinner />
    : (
      <PhotoGalleryEditorComponent
        imagesListUri={imagesListUri}
        uploadPhoto={handleUploadPhoto}
        handleRemoveImage={handleRemoveImage}
        handleUpdateGallery={handleUpdateGallery}
        galleryId={id}
        title={titleGallery}
        body={bodyGallery}
        isFetchingSingleImageList={isFetchingSingleImageList}
      />
    );
};

export default AdminPhotoGalleryEditorContainer;
