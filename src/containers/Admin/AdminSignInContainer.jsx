// @flow

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

// HOOKS
import usePrevious from '../../hooks/usePrevious';

// COMPONENT
import SignInComponent from '../../components/Admin/SignInComponent';

// ACTIONS
import { getUserSignInRequest } from '../../actions/signIn';

// SELECTORS
import { getSignInIsFetching, getSignInErrors } from '../../selectors/signIn';

import { ADMIN_ROUTES } from '../../constants/routes';


const AdminSignInContainer = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const isFetchingSignIn = useSelector((state) => getSignInIsFetching(state));
  const signInErrors = useSelector((state) => getSignInErrors(state));

  const prevIsFetchingSingIn = usePrevious(isFetchingSignIn);

  useEffect(() => {
    if (prevIsFetchingSingIn && !isFetchingSignIn && !signInErrors) {
      history.push(ADMIN_ROUTES.home);
    }
  }, [isFetchingSignIn, signInErrors]);

  const handleSignIn = (userData: Object) => {
    dispatch(getUserSignInRequest(userData));
  };

  return <SignInComponent isFetchingSign={isFetchingSignIn} handleSignIn={handleSignIn} />;
};

export default AdminSignInContainer;
