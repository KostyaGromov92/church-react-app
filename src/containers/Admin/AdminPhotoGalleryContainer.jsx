// @flow
import React, { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';

// ACTIONS
import { clearImageListUri } from '../../actions/imageFiles';
import { getPhotoGalleryRequest, removePhotoGalleryRequest } from '../../actions/gallery';
import { clearSingleGalleryData } from '../../actions/singleGallery';

// SELECTORS
import { getPhotoGalleryList, getIsFetchingGalleryList } from '../../selectors/gallery';

// COMPONENTS
import PhotoGalleryComponent from '../../components/Admin/PhotoGalleryComponent';
import Spinner from '../../components/Admin/Spinner';

const AdminPhotoGalleryContainer = () => {
  const dispatch = useDispatch();

  const photoGalleryList = useSelector((state) => getPhotoGalleryList(state));
  const isFetchingGalleryList = useSelector((state) => getIsFetchingGalleryList(state));

  useEffect(() => {
    dispatch(getPhotoGalleryRequest());
  }, []);

  const removePhotoGallery = (photoGalleryId: string) => {
    dispatch(removePhotoGalleryRequest({ photoGalleryId }));
  };

  const handleClearImageListUri = () => dispatch(clearImageListUri());
  const handleClearSingleGalleryData = () => dispatch(clearSingleGalleryData());

  return isFetchingGalleryList
    ? <Spinner />
    : (
      <PhotoGalleryComponent
        photoGalleryList={photoGalleryList}
        handleClearImageListUri={handleClearImageListUri}
        handleClearSingleGalleryData={handleClearSingleGalleryData}
        removePhotoGallery={removePhotoGallery}
      />
    );
};

export default AdminPhotoGalleryContainer;
