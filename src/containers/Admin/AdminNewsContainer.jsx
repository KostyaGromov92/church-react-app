// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// SELECTORS
import { getNewsData, getIsFetchingNews } from '../../selectors/newsList';

// ACTIONS
import { getNewsRequest, deleteNewsRequest } from '../../actions/newsList';
import { clearSingleNewsData } from '../../actions/singleNews';

// COMPONENTS
import NewsComponent from '../../components/Admin/NewsComponent';
import Spinner from '../../components/Admin/Spinner';

const AdminNewsContainer = () => {
  const dispatch = useDispatch();

  const newsList = useSelector((state) => getNewsData(state));
  const isFetchingNews = useSelector((state) => getIsFetchingNews(state));

  useEffect(() => {
    dispatch(getNewsRequest());
  }, []);

  const deleteNews = (newsId: string) => dispatch(deleteNewsRequest({ newsId }));

  const handleClearSingleNewsData = () => dispatch(clearSingleNewsData());

  return isFetchingNews
    ? <Spinner />
    : (
      <NewsComponent
        newsList={newsList}
        isFetchingNews={isFetchingNews}
        deleteNews={deleteNews}
        clearSingleNewsData={handleClearSingleNewsData}
      />
    );
};

export default AdminNewsContainer;
