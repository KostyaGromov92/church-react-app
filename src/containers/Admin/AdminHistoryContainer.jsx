// @flow

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// SELECTORS
import {
  getIsFetchingHistoryData, getHistoryDataBody, getHistoryDataTitle,
} from '../../selectors/history';

// ACTIONS
import { getHistoryDataRequest, updateHistoryDataRequest } from '../../actions/history';

// COMPONENTS
import HistoryComponent from '../../components/Admin/History';
import Spinner from '../../components/Admin/Spinner';

const AdminHistoryContainer = () => {
  const dispatch = useDispatch();

  const isFetchingHistoryData = useSelector((state) => getIsFetchingHistoryData(state));
  const historyDataTitle = useSelector((state) => getHistoryDataTitle(state));
  const historyDataBody = useSelector((state) => getHistoryDataBody(state));

  const handleUpdateHistoryData = (data: Object) => {
    dispatch(updateHistoryDataRequest({ data }));
  };

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => dispatch(getHistoryDataRequest());

    if (!didCancel) fetchData();

    return () => {
      didCancel = true;
    };
  }, []);

  return isFetchingHistoryData
    ? <Spinner />
    : (
      <HistoryComponent
        title={historyDataTitle}
        body={historyDataBody}
        handleUpdateHistoryData={handleUpdateHistoryData}
      />
    );
};

export default AdminHistoryContainer;
