/* eslint-disable max-len */
// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

import validate from 'uuid-validate';

// SELECTORS
import {
  getIsFetchingSinglePost, getSinglePostTitle, getSinglePostBody, getSinglePostSubTitle, getSinglePostImages,
} from '../../selectors/singlePost';
import { getIsFetchingImageUriList } from '../../selectors/imageFiles';

// ACTIONS
import { getSinglePostRequest, handleSubmitSinglePostRequest } from '../../actions/singlePost';
import { removeImageRequest, uploadImagesRequest } from '../../actions/imageFiles';

// COMPONENTS
import PostEditorComponent from '../../components/Admin/PostsComponent/PostEditorComponent';
import Spinner from '../../components/Admin/Spinner';

const AdminNewsEditorContainer = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const isFetchingSinglePost = useSelector((state) => getIsFetchingSinglePost(state));
  const singlePostTitle = useSelector((state) => getSinglePostTitle(state));
  const singlePostSubTitle = useSelector((state) => getSinglePostSubTitle(state));
  const singlePostBody = useSelector((state) => getSinglePostBody(state));
  const singlePostImages = useSelector((state) => getSinglePostImages(state));
  const isFetchingSingleImageList = useSelector((state) => getIsFetchingImageUriList(state));

  const handleUpdatePost = (newsData: Object) => {
    dispatch(handleSubmitSinglePostRequest(newsData));
  };

  const handleUploadPhoto = (photos: Array<Object>) => dispatch(uploadImagesRequest({ photos }));
  const handleRemoveImage = (imageUri: string) => dispatch(removeImageRequest(imageUri));

  useEffect(() => {
    if (validate(id)) dispatch(getSinglePostRequest({ postId: id }));
  }, []);

  return isFetchingSinglePost
    ? <Spinner />
    : (
      <PostEditorComponent
        handleUpdatePost={handleUpdatePost}
        uploadPhoto={handleUploadPhoto}
        handleRemoveImage={handleRemoveImage}
        postId={id}
        title={singlePostTitle}
        subTitle={singlePostSubTitle}
        body={singlePostBody}
        imagesList={singlePostImages}
        isFetchingSingleImageList={isFetchingSingleImageList}
      />
    );
};

export default AdminNewsEditorContainer;
