// @flow

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

// SELECTORS
import {
  getIsFetchingSingleChurch, getSingleChurchTitle,
} from '../../selectors/singleChurch';

// ACTIONS
import { getSingleChurchRequest, updateSingleChurchRequest } from '../../actions/singleChurch';
import Spinner from '../../components/Admin/Spinner';

// COMPONENTS
import ChurchEditorComponent from '../../components/Admin/ChurchEditorComponent';

const AdminChurchEditorContainer = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const isFetchingSingleChurch = useSelector((state) => getIsFetchingSingleChurch(state));
  const singleChurchTitle = useSelector((state) => getSingleChurchTitle(state));

  const handleUpdateChurchData = (data: Object) => {
    dispatch(updateSingleChurchRequest({ data }));
  };

  useEffect(() => {
    if (id) dispatch(getSingleChurchRequest({ churchId: id }));
  }, []);

  return isFetchingSingleChurch
    ? <Spinner />
    : (
      <ChurchEditorComponent
        title={singleChurchTitle}
        handleUpdateChurchData={handleUpdateChurchData}
      />
    );
};

export default AdminChurchEditorContainer;
