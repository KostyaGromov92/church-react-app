// @flow

// ADMIN VIDEO EDITOR - CONTAINER

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

import validate from 'uuid-validate';

// COMPONENTS
import VideoEditorComponent from '../../components/Admin/VideoListComponent/VideoEditor';
import Spinner from '../../components/Admin/Spinner';

import {
  getIsFetchingSingleVideo,
  getSingleVideoTitle,
  getSingleVideoBody,
  getSingleVideoLink,
} from '../../selectors/singleVideo';

// ACTIONS
import { getSingleVideoRequest, handleSubmitSingleVideoRequest } from '../../actions/singleVideo';

const AdminVideoEditorContainer = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const isFetchingSingleVideo = useSelector((state) => getIsFetchingSingleVideo(state));
  const singleVideoTitle = useSelector((state) => getSingleVideoTitle(state));
  const singleVideoBody = useSelector((state) => getSingleVideoBody(state));
  const singleVideoLink = useSelector((state) => getSingleVideoLink(state));

  const handleUpdateVideo = (newsData: Object) => {
    dispatch(handleSubmitSingleVideoRequest(newsData));
  };

  useEffect(() => {
    if (validate(id)) dispatch(getSingleVideoRequest({ videoId: id }));
  }, []);

  return isFetchingSingleVideo
    ? <Spinner />
    : (
      <VideoEditorComponent
        handleUpdateVideo={handleUpdateVideo}
        videoId={id}
        title={singleVideoTitle}
        body={singleVideoBody}
        videoLink={singleVideoLink}
      />
    );
};

export default AdminVideoEditorContainer;
