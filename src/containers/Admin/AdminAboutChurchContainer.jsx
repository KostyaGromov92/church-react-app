// @flow

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// SELECTORS
import {
  getIsFetchingAboutChurch, getAboutChurchTitle, getAboutChurchBody,
} from '../../selectors/aboutChurch';

// ACTIONS
import { getAboutChurchRequest, updateAboutChurchRequest } from '../../actions/aboutChurch';
import { getImagesUriRequest } from '../../actions/imageFiles';

// COMPONENTS
import AboutChurchComponent from '../../components/Admin/AboutChurch';
import Spinner from '../../components/Admin/Spinner';

const AdminAboutContainer = () => {
  const dispatch = useDispatch();

  const isFetchingAboutChurch = useSelector((state) => getIsFetchingAboutChurch(state));
  const aboutChurchTitle = useSelector((state) => getAboutChurchTitle(state));
  const aboutChurchBody = useSelector((state) => getAboutChurchBody(state));

  const handleUpdateAboutChurch = (data: Object) => {
    dispatch(updateAboutChurchRequest({ data }));
  };

  useEffect(() => {
    dispatch(getAboutChurchRequest());
    dispatch(getImagesUriRequest());
  }, []);

  return isFetchingAboutChurch
    ? <Spinner />
    : (
      <AboutChurchComponent
        title={aboutChurchTitle}
        body={aboutChurchBody}
        handleUpdateAboutChurch={handleUpdateAboutChurch}
      />
    );
};

export default AdminAboutContainer;
