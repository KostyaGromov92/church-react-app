// @flow

import React from 'react';

import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1, 2),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: '25px',
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function ResponsiveDrawer() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Paper className={classes.root}>
        <Typography variant="h5">
          First article
        </Typography>
        <div>
          <IconButton aria-label="delete" className={classes.margin}>
            <DeleteIcon fontSize="small" />
          </IconButton>
          <IconButton aria-label="edit" className={classes.margin}>
            <EditIcon fontSize="small" />
          </IconButton>
        </div>
      </Paper>
      <Paper className={classes.root}>
        <Typography variant="h5">
          First article
        </Typography>
        <div>
          <IconButton aria-label="delete" className={classes.margin}>
            <DeleteIcon fontSize="small" />
          </IconButton>
          <IconButton aria-label="edit" className={classes.margin}>
            <EditIcon fontSize="small" />
          </IconButton>
        </div>
      </Paper>
      <Paper className={classes.root}>
        <Typography variant="h5">
          First article
        </Typography>
        <div>
          <IconButton aria-label="delete" className={classes.margin}>
            <DeleteIcon fontSize="small" />
          </IconButton>
          <IconButton aria-label="edit" className={classes.margin}>
            <EditIcon fontSize="small" />
          </IconButton>
        </div>
      </Paper>
    </React.Fragment>
  );
}


export default ResponsiveDrawer;
