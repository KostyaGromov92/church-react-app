// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// SELECTORS
import { getPostsListData, getIsFetchingPostsList } from '../../selectors/postsList';

// ACTIONS
import { getPostsListRequest, deletePostRequest } from '../../actions/postsList';
import { clearSinglePostData } from '../../actions/singlePost';

// COMPONENTS
import PostsComponent from '../../components/Admin/PostsComponent';
import Spinner from '../../components/Admin/Spinner';

const AdminNewsContainer = () => {
  const dispatch = useDispatch();

  const postList = useSelector((state) => getPostsListData(state));
  const isFetchingPostList = useSelector((state) => getIsFetchingPostsList(state));

  useEffect(() => {
    dispatch(getPostsListRequest());
  }, []);

  const deletePost = (postId: string) => dispatch(deletePostRequest({ postId }));

  const handleClearSinglePostData = () => dispatch(clearSinglePostData());

  return isFetchingPostList
    ? <Spinner />
    : (
      <PostsComponent
        postList={postList}
        isFetchingNews={isFetchingPostList}
        deletePost={deletePost}
        clearSinglePostData={handleClearSinglePostData}
      />
    );
};

export default AdminNewsContainer;
