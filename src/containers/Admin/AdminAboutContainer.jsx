// @flow

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// SELECTORS
import {
  getIsFetchingAboutData, getAboutDataTitle, getAboutDataBody,
} from '../../selectors/aboutData';

// ACTIONS
import { getAboutDataRequest, updateAboutDataRequest } from '../../actions/aboutData';

// COMPONENTS
import AboutEditor from '../../components/Admin/About';
import Spinner from '../../components/Admin/Spinner';

const AdminAboutContainer = () => {
  const dispatch = useDispatch();

  const isFetchingAbout = useSelector((state) => getIsFetchingAboutData(state));
  const aboutDataTitle = useSelector((state) => getAboutDataTitle(state));
  const aboutDataBody = useSelector((state) => getAboutDataBody(state));

  const handleUpdateAboutData = (data: Object) => {
    dispatch(updateAboutDataRequest({ data }));
  };

  useEffect(() => {
    dispatch(getAboutDataRequest());
  }, []);

  return isFetchingAbout
    ? <Spinner />
    : (
      <AboutEditor
        title={aboutDataTitle}
        body={aboutDataBody}
        handleUpdateAboutData={handleUpdateAboutData}
      />
    );
};

export default AdminAboutContainer;
