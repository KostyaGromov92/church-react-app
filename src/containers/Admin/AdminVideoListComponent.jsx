// @flow

// VIDEO LiST -CONTAINER
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// SELECTORS
import { getVideoListData, getIsFetchingVideoList } from '../../selectors/videoList';

// ACTIONS
import { getVideoListRequest, deleteVideoRequest } from '../../actions/videoList';
import { clearSingleVideoData } from '../../actions/singleVideo';

// COMPONENTS
import VideoListComponent from '../../components/Admin/VideoListComponent';
import Spinner from '../../components/Admin/Spinner';

const AdminVideoListContainer = () => {
  const dispatch = useDispatch();

  const videoListData = useSelector((state) => getVideoListData(state));
  const isFetchingVideoList = useSelector((state) => getIsFetchingVideoList(state));

  useEffect(() => {
    dispatch(getVideoListRequest());
  }, []);

  const deleteVideo = (videoId: string) => dispatch(deleteVideoRequest({ videoId }));

  const handleClearSingleVideoData = () => dispatch(clearSingleVideoData());

  return isFetchingVideoList
    ? <Spinner />
    : (
      <VideoListComponent
        videoListData={videoListData}
        clearSingleVideoData={handleClearSingleVideoData}
        deleteVideo={deleteVideo}
      />
    );
};

export default AdminVideoListContainer;
