// @flow
import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

// ACTIONS
import { getChurchListRequest, deleteChurchRequest } from '../../actions/churchList';
import { clearSingleChurchData } from '../../actions/singleChurch';

// SELECTORS
import { getChurchList, getIsFetchingChurchList } from '../../selectors/churchList';

// CONSTANTS
import { ADMIN_ROUTES } from '../../constants/routes';

// COMPONENTS
import ChurchListComponent from '../../components/Admin/ChurchListComponent';
import Spinner from '../../components/Admin/Spinner';

const AdminChurchListContainer = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(getChurchListRequest());
  }, []);

  const deleteChurchHandler = (churchId: number) => {
    dispatch(deleteChurchRequest({ churchId }));
  };

  const handleClearSingleChurchData = async () => {
    await dispatch(clearSingleChurchData());
    history.push(ADMIN_ROUTES.addChurch);
  };

  const churchList = useSelector((state) => getChurchList(state));
  const isFetchingChurchList = useSelector((state) => getIsFetchingChurchList(state));

  return isFetchingChurchList
    ? <Spinner />
    : (
      <ChurchListComponent
        deleteChurchHandler={deleteChurchHandler}
        churchList={churchList}
        handleClearSingleChurchData={handleClearSingleChurchData}
      />
    );
};

export default AdminChurchListContainer;
