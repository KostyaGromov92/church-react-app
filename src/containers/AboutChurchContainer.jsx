// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// SELECTORS
import {
  getIsFetchingAboutChurch, getAboutChurchTitle, getAboutChurchBody,
} from '../selectors/aboutChurch';

// COMPONENTS
import Spinner from '../components/Spinner';
import AboutChurchComponent from '../components/AboutChurch';

// ACTIONS
import { getAboutChurchRequest } from '../actions/aboutChurch';

const AboutContainer = () => {
  const dispatch = useDispatch();

  const isFetchingAboutChurch = useSelector((state) => getIsFetchingAboutChurch(state));
  const aboutChurchTitle = useSelector((state) => getAboutChurchTitle(state));
  const aboutChurchBody = useSelector((state) => getAboutChurchBody(state));

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => dispatch(getAboutChurchRequest());

    if (!didCancel) fetchData();

    return () => {
      didCancel = true;
    };
  }, []);

  return isFetchingAboutChurch
    ? <Spinner />
    : (
      <AboutChurchComponent
        title={aboutChurchTitle}
        body={aboutChurchBody}
      />
    );
};

export default AboutContainer;
