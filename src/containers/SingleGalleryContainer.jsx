/* eslint-disable max-len */
// @flow
import React, { useEffect } from 'react';

import { useParams } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';

// SELECTORS
import {
  getSingleGalleryTitle,
  getSingleGalleryBody,
  getGalleryImagesUriList,
  getIsFetchingSingleGalleryData,
} from '../selectors/singleGallery';

// COMPONENTS
import SingleGalleryComponent from '../components/SingleGalleryComponent';
import Spinner from '../components/Spinner';

// ACTIONS
import { getSingleGalleryDataRequest } from '../actions/singleGallery';
import { setCurrentBreadCrumb } from '../actions/breadcrumbs';

const SingleGalleryContainer = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const isFetchingSingleNews = useSelector(state => getIsFetchingSingleGalleryData(state));
  const singleGalleryTitle = useSelector(state => getSingleGalleryTitle(state));
  const singleGalleryBody = useSelector(state => getSingleGalleryBody(state));
  const singleGalleryImageList = useSelector(state => getGalleryImagesUriList(state));

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => {
      if (id) await dispatch(getSingleGalleryDataRequest({ galleryId: id }));
    };

    if (!didCancel) fetchData();

    return () => {
      didCancel = true;
    };
  }, [id]);

  useEffect(() => {
    dispatch(setCurrentBreadCrumb(singleGalleryTitle));

    return () => dispatch(setCurrentBreadCrumb(''));
  }, [singleGalleryTitle]);

  const mainGalleryImage = singleGalleryImageList && singleGalleryImageList[0];
  // const photosList = singleGalleryImageList && singleGalleryImageList.length > 0 ? singleGalleryImageList.map(singleImage => ({
  //   src: singleImage,
  //   width: (Math.random() * (4 - 3) + 3).toFixed(),
  //   height: (Math.random() * (4 - 3) + 3).toFixed(),
  // })) : [];

  return isFetchingSingleNews
    ? <Spinner />
    : (
      <SingleGalleryComponent
        singleGalleryTitle={singleGalleryTitle}
        singleGalleryBody={singleGalleryBody}
        singleGalleryImageList={singleGalleryImageList}
        singleGalleryMainImage={mainGalleryImage}
      />
    );
};

export default SingleGalleryContainer;
