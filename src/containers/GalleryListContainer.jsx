/* eslint-disable max-len */
// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// COMPONENTS
import GalleryListComponent from '../components/GalleryListComponent';
import Spinner from '../components/Spinner';

// SELECTORS
import { getIsFetchingGalleryList, getPhotoGalleryList } from '../selectors/gallery';

// ACTIONS
import { getPhotoGalleryRequest } from '../actions/gallery';
import { setCurrentBreadCrumb } from '../actions/breadcrumbs';

const GalleryListContainer = () => {
  const dispatch = useDispatch();

  const photoGalleryList = useSelector((state) => getPhotoGalleryList(state));
  const isFetchingGalleryList = useSelector((state) => getIsFetchingGalleryList(state));

  useEffect(() => {
    if (photoGalleryList && !photoGalleryList.data) {
      dispatch(getPhotoGalleryRequest());
    }
  }, []);

  useEffect(() => {
    dispatch(setCurrentBreadCrumb('Gallery'));

    return () => dispatch(setCurrentBreadCrumb(''));
  }, []);

  return isFetchingGalleryList ? <Spinner /> : <GalleryListComponent photoGalleryList={photoGalleryList} />;
};

export default GalleryListContainer;
