// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// COMPONENTS
import NewsComponent from '../components/NewsComponent';
import Spinner from '../components/Spinner';

// ACTIONS
import { getNewsRequest } from '../actions/newsList';

// SELECTORS
import { getNewsData, getIsFetchingNews } from '../selectors/newsList';

const NewsContainer = () => {
  const dispatch = useDispatch();

  const newsList = useSelector((state) => getNewsData(state));
  const isFetchingNews = useSelector((state) => getIsFetchingNews(state));

  useEffect(() => {
    dispatch(getNewsRequest());
  }, []);

  return isFetchingNews ? <Spinner /> : <NewsComponent newsList={newsList} />;
};

export default NewsContainer;
