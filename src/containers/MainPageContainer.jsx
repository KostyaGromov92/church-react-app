// @flow
import React, { lazy, Suspense } from 'react';
// import { useDispatch } from 'react-redux';

// COMPONENTS
import MainNewsComponent from '../components/MainNewsComponent';
import Spinner from '../components/Spinner';

// ACTIONS
// import { getMainPageDataRequest } from '../actions/mainPageData';

// CONTAINERS
const LastNewsContainer = lazy(() => import('./LastNewsContainer'));
const VideoListContainer = lazy(() => import('./VideoListContainer'));
const EventsContainer = lazy(() => import('./EventsContainer'));

const MainPageContainer = () => (
  <Suspense fallback={<Spinner />}>
    <MainNewsComponent />
    <LastNewsContainer />
    <EventsContainer />
    <VideoListContainer />
  </Suspense>
);
export default MainPageContainer;
