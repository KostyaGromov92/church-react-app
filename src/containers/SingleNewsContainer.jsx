// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

// SELECTORS
import {
  getIsFetchingSingleNews, getSingleNewsTitle, getSingleNewsBody, getSingleNewsImages,
} from '../selectors/singleNews';

// COMPONENTS
import Spinner from '../components/Spinner';
import SingleNewsComponent from '../components/SingleNewsComponent';

// ACTIONS
import { getSingleNewsRequest } from '../actions/singleNews';
import { setCurrentBreadCrumb } from '../actions/breadcrumbs';

const SingleNewsContainer = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const isFetchingSingleNews = useSelector((state) => getIsFetchingSingleNews(state));
  const singleNewsTitle = useSelector((state) => getSingleNewsTitle(state));
  const singleNewsBody = useSelector((state) => getSingleNewsBody(state));
  const singleNewsImage = useSelector((state) => getSingleNewsImages(state));

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => {
      if (id) await dispatch(getSingleNewsRequest({ newsId: id }));
    };

    if (!didCancel) fetchData();

    return () => {
      didCancel = true;
    };
  }, []);

  useEffect(() => {
    dispatch(setCurrentBreadCrumb(singleNewsTitle));

    return () => dispatch(setCurrentBreadCrumb(''));
  }, [singleNewsTitle]);

  return isFetchingSingleNews
    ? <Spinner />
    : (
      <SingleNewsComponent
        newsId={id}
        title={singleNewsTitle}
        subTitle={singleNewsTitle}
        body={singleNewsBody}
        imageUrl={singleNewsImage[0]}
      />
    );
};

export default SingleNewsContainer;
