// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

// SELECTORS
import {
  getIsFetchingSinglePost,
  getSinglePostTitle,
  getSinglePostBody,
  getSinglePostImages,
  getSinglePostSubTitle,
} from '../selectors/singlePost';

// COMPONENTS
import Spinner from '../components/Spinner';
import SingleEventComponent from '../components/SingleEventComponent';

// ACTIONS
import { getSinglePostRequest } from '../actions/singlePost';
import { setCurrentBreadCrumb } from '../actions/breadcrumbs';

const SingleNewsContainer = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const isFetchingSinglePost = useSelector((state) => getIsFetchingSinglePost(state));
  const singlePostTitle = useSelector((state) => getSinglePostTitle(state));
  const singlePostSubTitle = useSelector((state) => getSinglePostSubTitle(state));
  const singlePostBody = useSelector((state) => getSinglePostBody(state));
  const singlePostImage = useSelector((state) => getSinglePostImages(state));

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => {
      if (id) await dispatch(getSinglePostRequest({ postId: id }));
    };

    if (!didCancel) fetchData();

    return () => {
      didCancel = true;
    };
  }, []);

  useEffect(() => {
    dispatch(setCurrentBreadCrumb(singlePostTitle));

    return () => dispatch(setCurrentBreadCrumb(''));
  }, [singlePostTitle]);

  return isFetchingSinglePost
    ? <Spinner />
    : (
      <SingleEventComponent
        title={singlePostTitle}
        subTitle={singlePostSubTitle}
        body={singlePostBody}
        imageUrl={singlePostImage[0]}
      />
    );
};

export default SingleNewsContainer;
