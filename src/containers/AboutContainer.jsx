// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// SELECTORS
import {
  getIsFetchingAboutData, getAboutDataTitle, getAboutDataBody,
} from '../selectors/aboutData';

// COMPONENTS
import Spinner from '../components/Spinner';
import AboutComponent from '../components/About';

// ACTIONS
import { getAboutDataRequest } from '../actions/aboutData';

const AboutContainer = () => {
  const dispatch = useDispatch();

  const isFetchingAboutData = useSelector((state) => getIsFetchingAboutData(state));
  const aboutDataTitle = useSelector((state) => getAboutDataTitle(state));
  const aboutDataBody = useSelector((state) => getAboutDataBody(state));

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => dispatch(getAboutDataRequest());

    if (!didCancel) fetchData();

    return () => {
      didCancel = true;
    };
  }, []);

  return isFetchingAboutData
    ? <Spinner />
    : (
      <AboutComponent
        title={aboutDataTitle}
        body={aboutDataBody}
      />
    );
};

export default AboutContainer;
