// @flow

// SINGLE VIDEO - CONTAINER

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';

// SELECTORS
import {
  getIsFetchingSingleVideo, getSingleVideoTitle, getSingleVideoBody, getSingleVideoLink
} from '../selectors/singleVideo';

// COMPONENTS
import Spinner from '../components/Spinner';
import SingleVideoComponent from '../components/SingleVideoComponent';

// ACTIONS
import { getSingleVideoRequest } from '../actions/singleVideo';
import { setCurrentBreadCrumb } from '../actions/breadcrumbs';

const SingleNewsContainer = () => {
  const dispatch = useDispatch();
  const { id } = useParams();

  const isFetchingSingleVideo = useSelector((state) => getIsFetchingSingleVideo(state));
  const singleVideoTitle = useSelector((state) => getSingleVideoTitle(state));
  const singleVideoBody = useSelector((state) => getSingleVideoBody(state));
  const singleVideoLink = useSelector((state) => getSingleVideoLink(state));

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => {
      if (id) await dispatch(getSingleVideoRequest({ videoId: id }));
    };

    if (!didCancel) fetchData();

    return () => {
      didCancel = true;
    };
  }, []);

  useEffect(() => {
    dispatch(setCurrentBreadCrumb(singleVideoTitle));

    return () => dispatch(setCurrentBreadCrumb(''));
  }, [singleVideoTitle]);

  return isFetchingSingleVideo
    ? <Spinner />
    : (
      <SingleVideoComponent
        title={singleVideoTitle}
        body={singleVideoBody}
        videoLink={singleVideoLink}
      />
    );
};

export default SingleNewsContainer;
