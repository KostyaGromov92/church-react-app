// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// COMPONENTS
import EventsComponent from '../components/EventsComponent';
import Spinner from '../components/Spinner';

// ACTIONS
import { getPostsListRequest } from '../actions/postsList';

// SELECTORS
import { getLastPostListData, getIsFetchingPostsList } from '../selectors/postsList';

const EventsContainer = () => {
  const dispatch = useDispatch();

  const postList = useSelector((state) => getLastPostListData(state));
  const isFetchingPostList = useSelector((state) => getIsFetchingPostsList(state));

  useEffect(() => {
    dispatch(getPostsListRequest());
  }, []);

  return isFetchingPostList
    ? <Spinner />
    : <EventsComponent postList={postList} />;
};

export default EventsContainer;
