// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// COMPONENTS
import EventsListComponent from '../components/EventsListComponent';
import Spinner from '../components/Spinner';

// ACTIONS
import { getPostsListRequest } from '../actions/postsList';

// SELECTORS
import { getPostsListData, getIsFetchingPostsList } from '../selectors/postsList';

const EventsListContainer = () => {
  const dispatch = useDispatch();

  const postList = useSelector((state) => getPostsListData(state));
  const isFetchingPostList = useSelector((state) => getIsFetchingPostsList(state));

  useEffect(() => {
    dispatch(getPostsListRequest());
  }, []);

  return isFetchingPostList ? <Spinner /> : <EventsListComponent postList={postList} />;
};

export default EventsListContainer;
