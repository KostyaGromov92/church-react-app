// @flow
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// SELECTORS
import {
  getIsFetchingHistoryData, getHistoryDataTitle, getHistoryDataBody,
} from '../selectors/history';

// COMPONENTS
import Spinner from '../components/Spinner';
import HistoryComponent from '../components/History';

// ACTIONS
import { getHistoryDataRequest } from '../actions/history';

const HistoryContainer = () => {
  const dispatch = useDispatch();

  const isFetchingHistoryData = useSelector((state) => getIsFetchingHistoryData(state));
  const historyDataTitle = useSelector((state) => getHistoryDataTitle(state));
  const historyDataBody = useSelector((state) => getHistoryDataBody(state));

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => dispatch(getHistoryDataRequest());

    if (!didCancel) fetchData();

    return () => {
      didCancel = true;
    };
  }, []);

  return isFetchingHistoryData
    ? <Spinner />
    : (
      <HistoryComponent
        title={historyDataTitle}
        body={historyDataBody}
      />
    );
};

export default HistoryContainer;
