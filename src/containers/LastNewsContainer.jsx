/* eslint-disable max-len */
// @flow

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// COMPONENTS
import Spinner from '../components/Spinner';
import NewsListComponent from '../components/NewsListComponent';

// ACTIONS
import { setCurrentBreadCrumb } from '../actions/breadcrumbs';
import { getNewsRequest } from '../actions/newsList';

// SELECTORS
import { getIsFetchingNews, getLastThreeNews } from '../selectors/newsList';

const LastNewsContainer = () => {
  const dispatch = useDispatch();

  const lastNews = useSelector((state) => getLastThreeNews(state));
  const isFetchingNews = useSelector((state) => getIsFetchingNews(state));

  useEffect(() => {
    dispatch(getNewsRequest());
    dispatch(setCurrentBreadCrumb(''));

    return () => {
      dispatch(setCurrentBreadCrumb('Новости'));
    };
  }, []);

  return isFetchingNews ? <Spinner /> : <NewsListComponent newsList={lastNews} />;
};

export default LastNewsContainer;
