// @flow

import React from 'react';
import ReactDOM from 'react-dom';

import { createBrowserHistory } from 'history';

import { Provider } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.min.css';
import CssBaseline from '@material-ui/core/CssBaseline';

import {
  Router,
} from 'react-router-dom';

import Store from './store';

import App from './App';
import Routes from './Routes';

import './index.css';
import './App.scss';

const history = createBrowserHistory();
const root = document.getElementById('root');

if (root) {
  ReactDOM.render(
    <Provider store={Store}>
      <CssBaseline />
      <Router history={history}>
        <App>
          <Routes />
        </App>
      </Router>
    </Provider>,
    root,
  );
}
