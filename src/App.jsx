// @flow

import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router';

// eslint-disable-next-line import/no-extraneous-dependencies
import 'react-app-polyfill/ie9'; // For IE 9-11 support
import 'react-app-polyfill/ie11'; // For IE 11 support

// $FlowFixMe
import 'slick-carousel/slick/slick.css';
// $FlowFixMe
import 'slick-carousel/slick/slick-theme.css';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'lightgallery.js/dist/css/lightgallery.css';

// HELPERS
import { isUserAuthenticated } from './services/helpers';

// HOOKS
import usePrevious from './hooks/usePrevious';

// CONTAINERS
import SideBarContainer from './containers/SideBarContainer';

// CONSTANTS
import { ADMIN_MENU_ROUTES } from './constants/routes';

// SELECTORS
import { getSignInIsFetching, getSignInErrors } from './selectors/signIn';

// COMPONENTS
import SideBarAdminComponent from './components/Admin/SideBarComponent';
import HeaderAdminComponent from './components/Admin/HeaderComponent';

import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/Footer';

type Props = {
  children: Array<*> | Object,
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    paddingTop: '64px',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const App = (props: Props) => {
  const { children } = props;

  const location = useLocation();

  const isAdminRoute = window && window.location.pathname.includes('admin');

  const isFetchingSignIn = useSelector((state) => getSignInIsFetching(state));
  const signInErrors = useSelector((state) => getSignInErrors(state));
  const previousIsFetchingSignIn = usePrevious(isFetchingSignIn);

  const [mobileOpen, setMobileOpen] = useState(false);
  const [isAdminUser, setIsAdminUser] = useState(isUserAuthenticated());

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  useEffect(() => {
    if (previousIsFetchingSignIn && !isFetchingSignIn && !signInErrors) {
      setIsAdminUser(true);
    }
  }, [isFetchingSignIn, location.pathname, previousIsFetchingSignIn, signInErrors]);


  const classes = useStyles();

  const renderAdminWrapper = () => (
    <div className={classes.root}>
      {isAdminUser
        ? (
          <SideBarAdminComponent
            handleDrawerToggle={handleDrawerToggle}
            isOpenMobileMenu={mobileOpen}
            menuLinks={ADMIN_MENU_ROUTES}
            isAdminUser={isAdminUser}
          />
        ) : null}
      <HeaderAdminComponent
        handleDrawerToggle={handleDrawerToggle}
      />
      <main className={classes.content}>
        <Container fixed>
          {children}
        </Container>
      </main>
    </div>
  );

  const renderAppWrapper = () => (
    <main>
      <HeaderComponent />
      <div className="content container">
        <div className="content__block">
          {children}
        </div>
        <SideBarContainer />
      </div>
      <FooterComponent />
    </main>
  );

  return isAdminRoute ? renderAdminWrapper() : renderAppWrapper();
};

export default App;
