// @flow

import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

// REDUCERS
import mockData from './reducers/mockData';
import newsList from './reducers/newsList';
import singleNews from './reducers/singleNews';
import singleGallery from './reducers/singleGallery';
import churchList from './reducers/churchList';
import singleChurch from './reducers/singleChurch';
import imageFiles from './reducers/imageFiles';
import gallery from './reducers/gallery';
import breadcrumbs from './reducers/breadcrumbs';
import signIn from './reducers/signIn';
import postsList from './reducers/postsList';
import singlePost from './reducers/singlePost';
import videoList from './reducers/videoList';
import singleVideo from './reducers/singleVideo';
import mainPageData from './reducers/mainPageData';
import aboutData from './reducers/about';
import aboutChurch from './reducers/aboutChurch';
import history from './reducers/history';

export default combineReducers({
  aboutData,
  aboutChurch,
  routing,
  mockData,
  newsList,
  singleNews,
  history,
  singleGallery,
  singleChurch,
  churchList,
  imageFiles,
  gallery,
  breadcrumbs,
  signIn,
  postsList,
  singlePost,
  videoList,
  singleVideo,
  mainPageData,
});
