/* eslint-disable max-len */
// @flow

// USE EDITOR - HOOK

import { useState } from 'react';
import { stateFromHTML } from 'draft-js-import-html';

import { convertToRaw, EditorState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';

const useEditor = (defaultValues?: Object) => {
  const editorDefaultValue = defaultValues && defaultValues.body
    ? EditorState.createWithContent(stateFromHTML(defaultValues.body))
    : EditorState.createEmpty();
  const [editorState, setEditorState] = useState(editorDefaultValue);

  const onEditorStateChange = (editorStateText: any) => {
    setEditorState(editorStateText);
  };

  const isEditorHasText = editorState.getCurrentContent().hasText();
  const currentEditorContent = draftToHtml(convertToRaw(editorState.getCurrentContent()));


  return {
    onEditorStateChange,
    currentEditorContent,
    isEditorHasText,
    editorState,
  };
};

export default useEditor;
