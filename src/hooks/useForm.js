// @flow

import { useState, useEffect } from 'react';

const useForm = (callback?: any, validate: Object, defaultValues: Object) => {
  const [values, setValues] = useState(defaultValues);
  const [errors, setErrors] = useState({});
  const [isFormHasError, setIsFormHasError] = useState(true);
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      if (callback) {
        callback();
      }
    }
  }, [errors]);

  useEffect(() => {
    const hasError = Object.values(errors).some((x) => x && x !== '');

    setIsFormHasError(hasError);
  }, [errors]);

  useEffect(() => {
    const hasError = Object.values(defaultValues).some((x) => x === '');
    setIsFormHasError(hasError);

    setValues(defaultValues);
  }, []);

  const handleSubmit = (event: any) => {
    if (event) event.preventDefault();

    setErrors(validate(values));
    setIsSubmitting(true);
  };

  const handleChange = (key: string, event: any) => {
    event.persist();

    setValues((prevValues) => ({ ...prevValues, [key]: event.target.value }));
  };

  const handleBlur = (key: string, event: any) => {
    event.persist();

    const filteredObject = Object.keys(validate(values))
      .filter((keyName) => keyName === key)
      .reduce((obj, keyName) => {
        // eslint-disable-next-line no-param-reassign
        obj[keyName] = validate(values)[keyName];
        return obj;
      }, {});

    setErrors((prevValues) => ({ ...prevValues, ...filteredObject }));
  };

  const handleFocus = (key: string) => {
    setErrors((prevValues) => ({ ...prevValues, [key]: null }));
  };

  return {
    handleBlur,
    handleChange,
    handleSubmit,
    handleFocus,
    isFormHasError,
    values,
    errors,
  };
};

export default useForm;
