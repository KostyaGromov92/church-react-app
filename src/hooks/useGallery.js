/* eslint-disable max-len */
// @flow

// USE GALLERY - HOOK

import { useState } from 'react';
import uuidv4 from 'uuid/v4';

// HELPERS
import { getFileExtension } from '../services/helpers';

// CONSTANTS
import { PHOTO_API_TOKEN, PHOTO_API_URL } from '../constants/api';

const useGallery = (
  imagesListUri: ?Array<string>,
  handleRemoveImage?: any,
) => {
  const [filesList, setFiles] = useState([]);
  const [imagesListURL, setImagesList] = useState(imagesListUri);
  const [imagesListToUpload, setImagesListToUpload] = useState([]);

  // useEffect(() => {
  //   const imageList = imageFiles && imageFiles.forEach(file => URL.revokeObjectURL(file.preview));

  //   if (imageList) setFiles(imageList);
  // }, [filesList]);

  const handleChangeGallery = (acceptedFiles: Object) => {
    const images = acceptedFiles.map((file) => {
      const imageExtension = getFileExtension(file.name);
      const fileRequest = new File([file], `${uuidv4()}.${imageExtension}`, { type: file.type });

      return fileRequest;
    });

    const imagesList = [...filesList, ...images];
    setFiles(imagesList);

    const imagesToUpload = images.map((imageFile) => `${PHOTO_API_URL}index.php/s/${PHOTO_API_TOKEN}/download?files=${imageFile.name}`);
    const imagesToUploadList = [...imagesListToUpload, ...imagesToUpload];

    setImagesListToUpload(imagesToUploadList);
  };

  const getParameterByName = (name: string, url) => {
    const match = RegExp(`[?&]${name}=([^&]*)`).exec(url);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  };

  const removeFileImage = (name: string) => {
    const newImageList: ?Array<Object> = filesList && filesList.filter((file) => file.name !== name);

    if (newImageList) setFiles(newImageList);
  };

  const removeImage = (url: string) => {
    const newImageList: ?Array<string> = imagesListURL && imagesListURL.filter((imageUrl) => imageUrl !== url);
    const imageUrl = getParameterByName('files', url);

    if (imageUrl && handleRemoveImage) handleRemoveImage(imageUrl);

    if (newImageList) {
      setImagesList(newImageList);
      setImagesListToUpload(newImageList);
    }
  };

  const isEmptyGallery = filesList && filesList.length === 0 && ((imagesListURL && imagesListURL.length === 0) || !imagesListURL);

  return {
    handleChangeGallery,
    imagesListToUpload,
    isEmptyGallery,
    imagesListURL,
    filesList,
    removeFileImage,
    removeImage,
  };
};

export default useGallery;
