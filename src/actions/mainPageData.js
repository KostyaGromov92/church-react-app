// @flow
// MAIN PAGE DATA - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as mainPageDataTypes from './types/mainPageData';

// GET MAIN PAGE DATA
export const getMainPageDataRequest = createAction(mainPageDataTypes.GET_MAIN_PAGE_DATA_REQUEST);
export const getMainPageDataSuccess = createAction(mainPageDataTypes.GET_MAIN_PAGE_DATA_SUCCESS);
export const getMainPageDataFailure = createAction(mainPageDataTypes.GET_MAIN_PAGE_DATA_FAILURE);
