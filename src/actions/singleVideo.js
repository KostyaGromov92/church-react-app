/* eslint-disable */
// @flow

import { createAction } from 'redux-actions';
import * as singleVideoTypes from './types/singleVideo';

// GET SINGLE NEWS
export const getSingleVideoRequest = createAction(singleVideoTypes.GET_SINGLE_VIDEO_REQUEST);
export const getSingleVideoSuccess = createAction(singleVideoTypes.GET_SINGLE_VIDEO_SUCCESS);
export const getSingleVideoFailure = createAction(singleVideoTypes.GET_SINGLE_VIDEO_FAILURE);

// UPDATE, CREATE SINGLE NEWS
export const handleSubmitSingleVideoRequest = createAction(singleVideoTypes.HANDLE_SUBMIT_SINGLE_VIDEO_REQUEST);
export const handleSubmitSingleVideoSuccess = createAction(singleVideoTypes.HANDLE_SUBMIT_SINGLE_VIDEO_SUCCESS);
export const handleSubmitSingleVideoFailure = createAction(singleVideoTypes.HANDLE_SUBMIT_SINGLE_VIDEO_FAILURE);

export const clearSingleVideoData = createAction(singleVideoTypes.CLEAR_SINGLE_VIDEO_DATA);
