/* eslint-disable max-len */
// @flow

// HISTORY DATA - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as historyDataTypes from './types/history';

// GET ABOUT DATA
export const getHistoryDataRequest = createAction(historyDataTypes.GET_HISTORY_DATA_REQUEST);
export const getHistoryDataSuccess = createAction(historyDataTypes.GET_HISTORY_DATA_SUCCESS);
export const getHistoryDataFailure = createAction(historyDataTypes.GET_HISTORY_DATA_FAILURE);

// UPDATE ABOUT DATA
export const updateHistoryDataRequest = createAction(historyDataTypes.UPDATE_HISTORY_DATA_REQUEST);
export const updateHistoryDataSuccess = createAction(historyDataTypes.UPDATE_HISTORY_DATA_SUCCESS);
export const updateHistoryDataFailure = createAction(historyDataTypes.UPDATE_HISTORY_DATA_FAILURE);
