// @flow
// VIDEO LIST - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as videoListTypes from './types/videoList';

// GET VIDEO LIST
export const getVideoListRequest = createAction(videoListTypes.GET_VIDEO_LIST_REQUEST);
export const getVideoListSuccess = createAction(videoListTypes.GET_VIDEO_LIST_SUCCESS);
export const getVideoListFailure = createAction(videoListTypes.GET_VIDEO_LIST_FAILURE);

// REMOVE VIDEO
export const deleteVideoRequest = createAction(videoListTypes.DELETE_VIDEO_REQUEST);
export const deleteVideoSuccess = createAction(videoListTypes.DELETE_VIDEO_SUCCESS);
export const deleteVideoFailure = createAction(videoListTypes.DELETE_VIDEO_FAILURE);
