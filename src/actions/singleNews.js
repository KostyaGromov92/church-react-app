/* eslint-disable max-len */
// SINGLE NEWS - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as singleNewsTypes from './types/singleNews';

// GET SINGLE NEWS
export const getSingleNewsRequest = createAction(singleNewsTypes.GET_SINGLE_NEWS_REQUEST);
export const getSingleNewsSuccess = createAction(singleNewsTypes.GET_SINGLE_NEWS_SUCCESS);
export const getSingleNewsFailure = createAction(singleNewsTypes.GET_SINGLE_NEWS_FAILURE);

// UPDATE, CREATE SINGLE NEWS
export const handleSubmitSingleNewsRequest = createAction(singleNewsTypes.HANDLE_SUBMIT_SINGLE_NEWS_REQUEST);
export const handleSubmitSingleNewsSuccess = createAction(singleNewsTypes.HANDLE_SUBMIT_SINGLE_NEWS_SUCCESS);
export const handleSubmitSingleNewsFailure = createAction(singleNewsTypes.HANDLE_SUBMIT_SINGLE_NEWS_FAILURE);

// CLEAR SINGLE NEWS DATA
export const clearSingleNewsData = createAction(singleNewsTypes.CLEAR_SINGLE_NEWS_DATA);
