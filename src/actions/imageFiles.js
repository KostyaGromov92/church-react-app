// PHOTOS - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as photoTypes from './types/imageFiles';

// ADMIN PHOTOS UPDATE
export const uploadImagesRequest = createAction(photoTypes.UPLOAD_IMAGES_REQUEST);
export const uploadImagesSuccess = createAction(photoTypes.UPLOAD_IMAGES_SUCCESS);
export const uploadImagesFailure = createAction(photoTypes.UPLOAD_IMAGES_FAILURE);

// GET ADMIN PHOTOS
export const getImagesUriRequest = createAction(photoTypes.GET_IMAGES_URI_REQUEST);
export const getImagesUriSuccess = createAction(photoTypes.GET_IMAGES_URI_SUCCESS);
export const getImagesUriFailure = createAction(photoTypes.GET_IMAGES_URI_FAILURE);

// REMOVE ADMIN PHOTOS
export const removeImageRequest = createAction(photoTypes.REMOVE_IMAGE_REQUEST);
export const removeImageSuccess = createAction(photoTypes.REMOVE_IMAGE_SUCCESS);
export const removeImageFailure = createAction(photoTypes.REMOVE_IMAGE_FAILURE);

export const clearImageListUri = createAction(photoTypes.CLEAR_IMAGE_LIST_URI);
