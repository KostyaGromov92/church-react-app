// @flow
// VIDEO LIST - ACTION TYPES
// =============================================================================

export const GET_VIDEO_LIST_REQUEST = 'GET_VIDEO_LIST_REQUEST';
export const GET_VIDEO_LIST_SUCCESS = 'GET_VIDEO_LIST_SUCCESS';
export const GET_VIDEO_LIST_FAILURE = 'GET_VIDEO_LIST_FAILURE';

export const DELETE_VIDEO_REQUEST = 'DELETE_VIDEO_REQUEST';
export const DELETE_VIDEO_SUCCESS = 'DELETE_VIDEO_SUCCESS';
export const DELETE_VIDEO_FAILURE = 'DELETE_VIDEO_FAILURE';
