// @flow
// NEWS - ACTION TYPES
// =============================================================================

export const GET_NEWS_REQUEST = 'GET_NEWS_REQUEST';
export const GET_NEWS_SUCCESS = 'GET_NEWS_SUCCESS';
export const GET_NEWS_FAILURE = 'GET_NEWS_FAILURE';

export const DELETE_NEWS_REQUEST = 'DELETE_NEWS_REQUEST';
export const DELETE_NEWS_SUCCESS = 'DELETE_NEWS_SUCCESS';
export const DELETE_NEWS_FAILURE = 'DELETE_NEWS_FAILURE';

export const UPLOAD_NEWS_REQUEST = 'UPLOAD_NEWS_REQUEST';
export const UPLOAD_NEWS_SUCCESS = 'UPLOAD_NEWS_SUCCESS';
export const UPLOAD_NEWS_FAILURE = 'UPLOAD_NEWS_FAILURE';
