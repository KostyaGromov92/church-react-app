// CHURCH LIST - ACTION TYPES
// =============================================================================

export const GET_СHURCHES_REQUEST = 'GET_СHURCHES_REQUEST';
export const GET_СHURCHES_SUCCESS = 'GET_СHURCHES_SUCCESS';
export const GET_СHURCHES_FAILURE = 'GET_СHURCHES_FAILURE';

export const DELETE_СHURCH_REQUEST = 'DELETE_СHURCH_REQUEST';
export const DELETE_СHURCH_SUCCESS = 'DELETE_СHURCH_SUCCESS';
export const DELETE_СHURCH_FAILURE = 'DELETE_СHURCH_FAILURE';
