// @flow
// POSTS LIST - ACTION TYPES
// =============================================================================

export const GET_POSTS_LIST_REQUEST = 'GET_POSTS_LIST_REQUEST';
export const GET_POSTS_LIST_SUCCESS = 'GET_POSTS_LIST_SUCCESS';
export const GET_POSTS_LIST_FAILURE = 'GET_POSTS_LIST_FAILURE';

export const DELETE_POST_REQUEST = 'DELETE_POST_REQUEST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_FAILURE = 'DELETE_POST_FAILURE';
