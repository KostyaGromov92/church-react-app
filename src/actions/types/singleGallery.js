// @flow

// SINGLE GALLERY - ACTION TYPES
// =============================================================================

export const GET_SINGLE_GALLERY_DATA_REQUEST = 'GET_SINGLE_GALLERY_DATA_REQUEST';
export const GET_SINGLE_GALLERY_DATA_SUCCESS = 'GET_SINGLE_GALLERY_DATA_SUCCESS';
export const GET_SINGLE_GALLERY_DATA_FAILURE = 'GET_SINGLE_GALLERY_DATA_FAILURE';

export const UPDATE_SINGLE_GALLERY_DATA_REQUEST = 'UPDATE_SINGLE_GALLERY_DATA_REQUEST';
export const UPDATE_SINGLE_GALLERY_DATA_SUCCESS = 'UPDATE_SINGLE_GALLERY_DATA_SUCCESS';
export const UPDATE_SINGLE_GALLERY_DATA_FAILURE = 'UPDATE_SINGLE_NEWS_FAILURE';

export const CLEAR_SINGLE_GALLERY_DATA = 'CLEAR_SINGLE_GALLERY_DATA';
