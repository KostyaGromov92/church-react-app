// @flow
// SIGN IN - ACTION TYPES
// =============================================================================

export const GET_USER_SIGN_IN_REQUEST = 'GET_USER_SIGN_IN_REQUEST';
export const GET_USER_SIGN_IN_SUCCESS = 'GET_USER_SIGN_IN_SUCCESS';
export const GET_USER_SIGN_IN_FAILURE = 'GET_USER_SIGN_IN_FAILURE';
