/* eslint-disable import/prefer-default-export */
// @flow
// BREADCRUMBS - ACTION TYPES
// =============================================================================

// SET CURRENT BREADCRUMB
export const SET_CURRENT_BREADCRUMB = 'SET_CURRENT_BREADCRUMB';
