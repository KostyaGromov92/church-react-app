// @flow
// MAIN PAGE DATA - ACTION TYPES
// =============================================================================

export const GET_MAIN_PAGE_DATA_REQUEST = 'GET_MAIN_PAGE_DATA_REQUEST';
export const GET_MAIN_PAGE_DATA_SUCCESS = 'GET_MAIN_PAGE_DATA_SUCCESS';
export const GET_MAIN_PAGE_DATA_FAILURE = 'GET_MAIN_PAGE_DATA_FAILURE';
