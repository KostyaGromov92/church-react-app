/* eslint-disable import/prefer-default-export */
// @flow

// BREADCRUMBS - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as breadcrumbsTypes from './types/breadcrumbs';

// SET CURRENT BREADCRUMB
export const setCurrentBreadCrumb = createAction(breadcrumbsTypes.SET_CURRENT_BREADCRUMB);
