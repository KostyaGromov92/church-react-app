// @flow
// POSTS LIST - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as postsListTypes from './types/postsList';

// GET POSTS LIST
export const getPostsListRequest = createAction(postsListTypes.GET_POSTS_LIST_REQUEST);
export const getPostsListSuccess = createAction(postsListTypes.GET_POSTS_LIST_SUCCESS);
export const getPostsListFailure = createAction(postsListTypes.GET_POSTS_LIST_FAILURE);

// REMOVE POST
export const deletePostRequest = createAction(postsListTypes.DELETE_POST_REQUEST);
export const deletePostSuccess = createAction(postsListTypes.DELETE_POST_SUCCESS);
export const deletePostFailure = createAction(postsListTypes.DELETE_POST_FAILURE);
