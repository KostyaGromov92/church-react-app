/* eslint-disable max-len */
// @flow

// SINGLE NEWS - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as singleChurchTypes from './types/singleChurch';

// GET SINGLE CHURCH
export const getSingleChurchRequest = createAction(singleChurchTypes.GET_SINGLE_CHURCH_REQUEST);
export const getSingleChurchSuccess = createAction(singleChurchTypes.GET_SINGLE_CHURCH_SUCCESS);
export const getSingleChurchFailure = createAction(singleChurchTypes.GET_SINGLE_CHURCH_FAILURE);

// UPDATE SINGLE CHURCH
export const updateSingleChurchRequest = createAction(singleChurchTypes.UPDATE_SINGLE_CHURCH_REQUEST);
export const updateSingleChurchSuccess = createAction(singleChurchTypes.UPDATE_SINGLE_CHURCH_SUCCESS);
export const updateSingleChurchFailure = createAction(singleChurchTypes.UPDATE_SINGLE_CHURCH_FAILURE);


// CLEAR SINGLE CHURCH DATA
export const clearSingleChurchData = createAction(singleChurchTypes.CLEAR_SINGLE_CHURCH_DATA);
