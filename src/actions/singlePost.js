/* eslint-disable max-len */
// SINGLE POST - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as singlePostTypes from './types/singlePost';

// GET SINGLE NEWS
export const getSinglePostRequest = createAction(singlePostTypes.GET_SINGLE_POST_REQUEST);
export const getSinglePostSuccess = createAction(singlePostTypes.GET_SINGLE_POST_SUCCESS);
export const getSinglePostFailure = createAction(singlePostTypes.GET_SINGLE_POST_FAILURE);

// UPDATE, CREATE SINGLE NEWS
export const handleSubmitSinglePostRequest = createAction(singlePostTypes.HANDLE_SUBMIT_SINGLE_POST_REQUEST);
export const handleSubmitSinglePostSuccess = createAction(singlePostTypes.HANDLE_SUBMIT_SINGLE_POST_SUCCESS);
export const handleSubmitSinglePostFailure = createAction(singlePostTypes.HANDLE_SUBMIT_SINGLE_POST_FAILURE);

// CLEAR SINGLE NEWS DATA
export const clearSinglePostData = createAction(singlePostTypes.CLEAR_SINGLE_POST_DATA);
