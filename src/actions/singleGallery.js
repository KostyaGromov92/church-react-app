/* eslint-disable max-len */
// @flow

// SINGLE GALLERY DATA - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as singleGalleryTypes from './types/singleGallery';

// GET SINGLE GALLERY DATA
export const getSingleGalleryDataRequest = createAction(singleGalleryTypes.GET_SINGLE_GALLERY_DATA_REQUEST);
export const getSingleGalleryDataSuccess = createAction(singleGalleryTypes.GET_SINGLE_GALLERY_DATA_SUCCESS);
export const getSingleGalleryDataFailure = createAction(singleGalleryTypes.GET_SINGLE_GALLERY_DATA_FAILURE);

// UPDATE SINGLE GALLERY DATA
export const updateSingleGalleryDataRequest = createAction(singleGalleryTypes.UPDATE_SINGLE_GALLERY_DATA_REQUEST);
export const updateSingleGalleryDataSuccess = createAction(singleGalleryTypes.UPDATE_SINGLE_GALLERY_DATA_SUCCESS);
export const updateSingleGalleryDataFailure = createAction(singleGalleryTypes.UPDATE_SINGLE_GALLERY_DATA_FAILURE);

// CLEAR SINGLE GALLERY DATA
export const clearSingleGalleryData = createAction(singleGalleryTypes.CLEAR_SINGLE_GALLERY_DATA);
