// @flow

// PHOTO GALLERY - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as galleryTypes from './types/gallery';

// GET PHOTO GALLERY
export const getPhotoGalleryRequest = createAction(galleryTypes.GET_PHOTO_GALLERY_REQUEST);
export const getPhotoGallerySuccess = createAction(galleryTypes.GET_PHOTO_GALLERY_SUCCESS);
export const getPhotoGalleryFailure = createAction(galleryTypes.GET_PHOTO_GALLERY_FAILURE);

// UPLOAD PHOTO GALLERY
export const uploadPhotoGalleryRequest = createAction(galleryTypes.UPLOAD_PHOTO_GALLERY_REQUEST);
export const uploadPhotoGallerySuccess = createAction(galleryTypes.UPLOAD_PHOTO_GALLERY_SUCCESS);
export const uploadPhotoGalleryFailure = createAction(galleryTypes.UPLOAD_PHOTO_GALLERY_FAILURE);

// REMOVE PHOTO GALLERY
export const removePhotoGalleryRequest = createAction(galleryTypes.REMOVE_PHOTO_GALLERY_REQUEST);
export const removePhotoGallerySuccess = createAction(galleryTypes.REMOVE_PHOTO_GALLERY_SUCCESS);
export const removePhotoGalleryFailure = createAction(galleryTypes.REMOVE_PHOTO_GALLERY_FAILURE);
