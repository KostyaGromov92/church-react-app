/* eslint-disable max-len */
// @flow

// ABOUT DATA - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as aboutDataTypes from './types/aboutData';

// GET ABOUT DATA
export const getAboutDataRequest = createAction(aboutDataTypes.GET_ABOUT_DATA_REQUEST);
export const getAboutDataSuccess = createAction(aboutDataTypes.GET_ABOUT_DATA_SUCCESS);
export const getAboutDataFailure = createAction(aboutDataTypes.GET_ABOUT_DATA_FAILURE);

// UPDATE ABOUT DATA
export const updateAboutDataRequest = createAction(aboutDataTypes.UPDATE_ABOUT_DATA_REQUEST);
export const updateAboutDataSuccess = createAction(aboutDataTypes.UPDATE_ABOUT_DATA_SUCCESS);
export const updateAboutDataFailure = createAction(aboutDataTypes.UPDATE_ABOUT_DATA_FAILURE);
