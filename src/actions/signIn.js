// @flow
// NEWS - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as signInTypes from './types/signIn';

// SIGN IN
export const getUserSignInRequest = createAction(signInTypes.GET_USER_SIGN_IN_REQUEST);
export const getUserSignInSuccess = createAction(signInTypes.GET_USER_SIGN_IN_SUCCESS);
export const getUserSignInFailure = createAction(signInTypes.GET_USER_SIGN_IN_FAILURE);
