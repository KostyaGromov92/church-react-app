/* eslint-disable max-len */
// @flow

// ABOUT CHURCH - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as aboutChurchTypes from './types/aboutChurch';

// GET ABOUT CHURCH
export const getAboutChurchRequest = createAction(aboutChurchTypes.GET_ABOUT_CHURCH_REQUEST);
export const getAboutChurchSuccess = createAction(aboutChurchTypes.GET_ABOUT_CHURCH_SUCCESS);
export const getAboutChurchFailure = createAction(aboutChurchTypes.GET_ABOUT_CHURCH_FAILURE);

// UPDATE ABOUT CHURCH
export const updateAboutChurchRequest = createAction(aboutChurchTypes.UPDATE_ABOUT_CHURCH_REQUEST);
export const updateAboutChurchSuccess = createAction(aboutChurchTypes.UPDATE_ABOUT_CHURCH_SUCCESS);
export const updateAboutChurchFailure = createAction(aboutChurchTypes.UPDATE_ABOUT_CHURCH_FAILURE);
