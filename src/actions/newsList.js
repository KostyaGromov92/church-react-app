// @flow
// NEWS - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as newsTypes from './types/newsList';

// GET NEWS
export const getNewsRequest = createAction(newsTypes.GET_NEWS_REQUEST);
export const getNewsSuccess = createAction(newsTypes.GET_NEWS_SUCCESS);
export const getNewsFailure = createAction(newsTypes.GET_NEWS_FAILURE);

// UPLOAD NEWS
export const uploadNewsRequest = createAction(newsTypes.UPLOAD_NEWS_REQUEST);
export const uploadNewsSuccess = createAction(newsTypes.UPLOAD_NEWS_SUCCESS);
export const uploadNewsFailure = createAction(newsTypes.UPLOAD_NEWS_FAILURE);

// REMOVE NEWS
export const deleteNewsRequest = createAction(newsTypes.DELETE_NEWS_REQUEST);
export const deleteNewsSuccess = createAction(newsTypes.DELETE_NEWS_SUCCESS);
export const deleteNewsFailure = createAction(newsTypes.DELETE_NEWS_FAILURE);
