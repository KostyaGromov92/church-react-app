// @flow

// CHURCH LIST - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as churchListTypes from './types/churchList';

// GET CHURCH LIST
export const getChurchListRequest = createAction(churchListTypes.GET_СHURCHES_REQUEST);
export const getChurchListSuccess = createAction(churchListTypes.GET_СHURCHES_SUCCESS);
export const getChurchListFailure = createAction(churchListTypes.GET_СHURCHES_FAILURE);

// REMOVE CHURCH
export const deleteChurchRequest = createAction(churchListTypes.DELETE_СHURCH_REQUEST);
export const deleteChurchSuccess = createAction(churchListTypes.DELETE_СHURCH_SUCCESS);
export const deleteChurchFailure = createAction(churchListTypes.DELETE_СHURCH_FAILURE);
