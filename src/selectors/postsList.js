/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// POSTS LIST DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';
import type { PostsListState } from '../reducers/postsList';

type State = { postsList: PostsListState };

export const getPostsListData = (state: State) => (state.postsList ? state.postsList.posts.data : null);
export const getIsFetchingPostsList = (state: State) => (state.postsList ? state.postsList.isFetching : false);


export const getLastPostListData = createSelector(
  getPostsListData,
  (postList) => (postList ? postList.slice(0, 4) : []),
);
