/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// CHURCH LIST DATA - SELECTORS
// =============================================================================

import type { ChurchListState } from '../reducers/churchList';

type State = { churchList: ChurchListState };

export const getChurchList = (state: State) => (state.churchList ? state.churchList.churchList : null);
export const getIsFetchingChurchList = (state: State) => (state.churchList ? state.churchList.isFetching : false);
