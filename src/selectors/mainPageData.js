/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// MAIN PAGE DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { MainPageDataState } from '../reducers/mainPageData';

type State = { mainPageData: MainPageDataState };

export const getMainPageData = (state: State) => (state.mainPageData
  && state.mainPageData.mainPageDataList
  && state.mainPageData.mainPageDataList.data ? state.mainPageData.mainPageDataList.data[0] : null);

export const getIsFetchingMainPageData = (state: State) => (state.mainPageData ? state.mainPageData.isFetching : false);

export const getMainPageDataNews = createSelector(
  getMainPageData,
  (mainPageDataList) => mainPageDataList && mainPageDataList.news && mainPageDataList.news.reverse(),
);

export const getMainPageDataGallery = createSelector(
  getMainPageData,
  (mainPageDataList) => mainPageDataList && mainPageDataList.photoGalleries,
);

export const getMainPageDataBlogs = createSelector(
  getMainPageData,
  (mainPageDataList) => mainPageDataList && mainPageDataList.blogs && mainPageDataList.blogs.reverse(),
);


export const getMainPageDataVideoList = createSelector(
  getMainPageData,
  (mainPageDataList) => mainPageDataList && mainPageDataList.videos && mainPageDataList.videos.reverse(),
);
