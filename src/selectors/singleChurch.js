/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// SINGLE CHURCH DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { SingleChurchState } from '../reducers/singleChurch';

type State = { singleChurch: SingleChurchState };

export const getSingleChurch = (state: State) => (state.singleChurch ? state.singleChurch.data : null);
export const getIsFetchingSingleChurch = (state: State) => (state.singleChurch ? state.singleChurch.isFetching : false);

export const getSingleChurchTitle = createSelector(
  getSingleChurch,
  (singleChurch) => (singleChurch ? singleChurch.title : ''),
);

export const getSingleChurchBody = createSelector(
  getSingleChurch,
  (singleChurch) => (singleChurch ? singleChurch.body : ''),
);
