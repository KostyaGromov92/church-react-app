/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// SINGLE POST DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { SinglePostState } from '../reducers/singlePost';

type State = { singlePost: SinglePostState };

export const getSinglePostData = (state: State) => (
  state.singlePost && state.singlePost.singlePostData && state.singlePost.singlePostData.data
    ? state.singlePost.singlePostData.data[0]
    : null
);

export const getIsFetchingSinglePost = (state: State) => (state.singlePost ? state.singlePost.isFetching : false);

export const getSinglePostId = createSelector(
  getSinglePostData,
  (singlePost) => (singlePost ? singlePost.id : ''),
);

export const getSinglePostTitle = createSelector(
  getSinglePostData,
  (singlePost) => (singlePost ? singlePost.title : ''),
);

export const getSinglePostSubTitle = createSelector(
  getSinglePostData,
  (singlePost) => (singlePost ? singlePost.subtitle : ''),
);

export const getSinglePostBody = createSelector(
  getSinglePostData,
  (singlePost) => (singlePost ? singlePost.text : ''),
);

export const getSinglePostImages = createSelector(
  getSinglePostData,
  (singlePost) => (singlePost ? singlePost.images : ''),
);
