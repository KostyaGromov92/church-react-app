/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// SIGN IN - SELECTORS
// =============================================================================

import type { SignInState } from '../reducers/signIn';

type State = { signIn: SignInState };

export const getSignInIsFetching = (state: State) => (state.signIn ? state.signIn.isFetching : false);
export const getSignInErrors = (state: State) => (state.signIn ? state.signIn.error : null);
