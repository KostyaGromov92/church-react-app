/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// BREADCRUMBS - SELECTORS
// =============================================================================

import type { BreadcrumbsState } from '../reducers/breadcrumbs';

type State = { breadcrumbs: BreadcrumbsState };

export const getCurrentBreadCrumb = (state: State) => (state.breadcrumbs ? state.breadcrumbs.breadcrumb : null);
