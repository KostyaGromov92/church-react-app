/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// NEWS DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { NewsListState } from '../reducers/newsList';

type State = { newsList: NewsListState };

export const getNewsData = (state: State) => (state.newsList ? state.newsList.news.data : null);
export const getIsFetchingNews = (state: State) => (state.newsList ? state.newsList.isFetching : false);

export const getLastThreeNews = createSelector(
  getNewsData,
  (newsList) => (newsList ? newsList.slice(0, 3) : []),
);
