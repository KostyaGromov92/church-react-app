/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// ABOUT CHURCH DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { AboutChurchState } from '../reducers/aboutChurch';

type State = { aboutChurch: AboutChurchState };

export const getAboutChurchData = (state: State) => (state.aboutChurch ? state.aboutChurch.data.data : {});

export const getIsFetchingAboutChurch = (state: State) => (state.aboutChurch ? state.aboutChurch.isFetching : false);

export const getAboutChurchTitle = createSelector(
  getAboutChurchData,
  (aboutChurch) => (aboutChurch && aboutChurch[0] ? aboutChurch[0].title : ''),
);

export const getAboutChurchBody = createSelector(
  getAboutChurchData,
  (aboutChurch) => (aboutChurch && aboutChurch[0] ? aboutChurch[0].body : ''),
);
