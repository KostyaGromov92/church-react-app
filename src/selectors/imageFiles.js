/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// IMAGE FILES - SELECTORS
// =============================================================================

import type { ImageFilesState } from '../reducers/imageFiles';

type State = { imageFiles: ImageFilesState };

export const getImageUriList = (state: State) => (state.imageFiles ? state.imageFiles.imageUriList : null);
export const getIsFetchingImageUriList = (state: State) => (state.imageFiles ? state.imageFiles.isFetching : null);
