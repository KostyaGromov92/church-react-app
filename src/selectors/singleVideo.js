/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// SINGLE VIDEO DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { SingleVideoState } from '../reducers/singleVideo';

type State = { singleVideo: SingleVideoState };

export const getSingleVideoData = (state: State) => (state.singleVideo && state.singleVideo.singleVideoData && state.singleVideo.singleVideoData.data
  ? state.singleVideo.singleVideoData.data[0]
  : null
);

export const getIsFetchingSingleVideo = (state: State) => (state.singleVideo ? state.singleVideo.isFetching : false);

export const getSingleVideoId = createSelector(
  getSingleVideoData,
  (singleVideo) => (singleVideo ? singleVideo.id : ''),
);

export const getSingleVideoTitle = createSelector(
  getSingleVideoData,
  (singleVideo) => (singleVideo ? singleVideo.title : ''),
);


export const getSingleVideoBody = createSelector(
  getSingleVideoData,
  (singleVideo) => (singleVideo ? singleVideo.text : ''),
);

export const getSingleVideoLink = createSelector(
  getSingleVideoData,
  (singleVideo) => (singleVideo ? singleVideo.link : ''),
);
