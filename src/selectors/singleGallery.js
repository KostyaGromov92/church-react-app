/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// SINGLE GALLERY DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { SingleGalleryState } from '../reducers/singleGallery';

type State = { singleGallery: SingleGalleryState };

export const getSingleGalleryData = (state: State) => (state.singleGallery && state.singleGallery.photoGallery ? state.singleGallery.photoGallery.data[0] : null);
export const getIsFetchingSingleGalleryData = (state: State) => (state.singleGallery ? state.singleGallery.isFetching : false);

export const getSingleGalleryTitle = createSelector(
  getSingleGalleryData,
  (singleGallery) => (singleGallery ? singleGallery.title : ''),
);

export const getSingleGalleryBody = createSelector(
  getSingleGalleryData,
  (singleGallery) => (singleGallery ? singleGallery.text : ''),
);

export const getGalleryImagesUriList = createSelector(
  getSingleGalleryData,
  (singleGallery) => (singleGallery ? singleGallery.images : ''),
);
