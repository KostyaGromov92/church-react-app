/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// PHOTO GALLERY LIST - SELECTORS
// =============================================================================

import type { PhotoGalleryState } from '../reducers/gallery';

type State = { gallery: PhotoGalleryState };

export const getPhotoGalleryList = (state: State) => (state.gallery ? state.gallery.photoGallery.data : null);

export const getIsFetchingGalleryList = (state: State) => (state.gallery ? state.gallery.isFetching : false);
