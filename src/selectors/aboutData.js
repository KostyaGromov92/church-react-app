/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// ABOUT DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { AboutDataState } from '../reducers/about';

type State = { aboutData: AboutDataState };

export const getAboutData = (state: State) => (state.aboutData ? state.aboutData.data.data : {});

export const getIsFetchingAboutData = (state: State) => (state.aboutData ? state.aboutData.isFetching : false);

export const getAboutDataTitle = createSelector(
  getAboutData,
  (aboutData) => (aboutData && aboutData[0] ? aboutData[0].title : ''),
);

export const getAboutDataBody = createSelector(
  getAboutData,
  (aboutData) => (aboutData && aboutData[0] ? aboutData[0].body : ''),
);
