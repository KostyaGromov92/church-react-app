/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// SINGLE NEWS DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { SingleNewsState } from '../reducers/singleNews';

type State = { singleNews: SingleNewsState };

export const getSingleNewsData = (state: State) => (state.singleNews && state.singleNews.singleNewsData && state.singleNews.singleNewsData.data ? state.singleNews.singleNewsData.data[0] : null);

export const getIsFetchingSingleNews = (state: State) => (state.singleNews ? state.singleNews.isFetching : false);

export const getSingleNewsId = createSelector(
  getSingleNewsData,
  (singleNews) => (singleNews ? singleNews.id : ''),
);

export const getSingleNewsTitle = createSelector(
  getSingleNewsData,
  (singleNews) => (singleNews ? singleNews.title : ''),
);

export const getSingleNewsSubTitle = createSelector(
  getSingleNewsData,
  (singleNews) => (singleNews ? singleNews.subtitle : ''),
);

export const getSingleNewsBody = createSelector(
  getSingleNewsData,
  (singleNews) => (singleNews ? singleNews.text : ''),
);

export const getSingleNewsImages = createSelector(
  getSingleNewsData,
  (singleNews) => (singleNews ? singleNews.images : ''),
);
