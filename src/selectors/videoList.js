/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// VIDEO LIST DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { VideoListState } from '../reducers/videoList';

type State = { videoList: VideoListState };

export const getVideoListData = (state: State) => (state.videoList ? state.videoList.videoList.data : null);
export const getIsFetchingVideoList = (state: State) => (state.videoList ? state.videoList.isFetching : false);

export const getLastThreeVideo = createSelector(
  getVideoListData,
  (videoList) => (videoList ? videoList.reverse() : []),
);
