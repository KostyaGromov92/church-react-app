/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// HISTORY DATA - SELECTORS
// =============================================================================

import { createSelector } from 'reselect';

import type { HistoryDataState } from '../reducers/history';

type State = { history: HistoryDataState };

export const getHistoryData = (state: State) => (state.history ? state.history.data.data : {});

export const getIsFetchingHistoryData = (state: State) => (state.history ? state.history.isFetching : false);

export const getHistoryDataTitle = createSelector(
  getHistoryData,
  (history) => (history && history[0] ? history[0].title : ''),
);

export const getHistoryDataBody = createSelector(
  getHistoryData,
  (history) => (history && history[0] ? history[0].body : ''),
);
