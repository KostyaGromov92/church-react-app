// @flow
// CHURCH LIST - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as churchListActionTypes from '../actions/types/churchList';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as churchListAction from '../actions/churchList';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetChurchListSaga(): Generator<PutEffect, *, *> {
  try {
    const requestData = {
      additionalUrl: 'posts?_limit=10',
      method: 'GET',
      baseUrl: 'https://jsonplaceholder.typicode.com/',
    };

    const response = yield call(processRequest, requestData);

    yield put(churchListAction.getChurchListSuccess(response.data));
  } catch (e) {
    yield put(churchListAction.getChurchListFailure(e));
  }
}

export function* handleDeleteChurchSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;
    if (payload && payload.churchId) {
      const { churchId } = payload;

      const requestData = {
        additionalUrl: `posts/${churchId}`,
        method: 'DELETE',
        baseUrl: 'https://jsonplaceholder.typicode.com/',
      };

      yield call(processRequest, requestData);

      yield put(churchListAction.deleteChurchSuccess(churchId));
    }
  } catch (e) {
    yield put(churchListAction.deleteChurchFailure(e));
  }
}

export function* watchHandleGetChurchListSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(churchListActionTypes.GET_СHURCHES_REQUEST, handleGetChurchListSaga);
}

export function* watchDeleteChurchSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(churchListActionTypes.DELETE_СHURCH_REQUEST, handleDeleteChurchSaga);
}
