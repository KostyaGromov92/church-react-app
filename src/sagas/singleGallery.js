// @flow

/* eslint-disable max-len */

// SINGLE GALLERY - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import {
  call,
  put,
  select,
  takeEvery,
} from 'redux-saga/effects';

// TYPES
import * as singleGalleryActionTypes from '../actions/types/singleGallery';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as singleGalleryAction from '../actions/singleGallery';

// SELECTORS
import { getSingleGalleryData } from '../selectors/singleGallery';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetSingleGalleryDataSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload && payload.galleryId) {
      const { galleryId } = payload;

      const requestData = {
        additionalUrl: `prphotogallery/${galleryId}`,
        method: 'GET',
      };

      const response = yield call(processRequest, requestData);

      yield put(singleGalleryAction.getSingleGalleryDataSuccess(response.data));
    }
  } catch (e) {
    yield put(singleGalleryAction.getSingleGalleryDataFailure(e));
  }
}

export function* handleUpdateSingleGallerySaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;
    const singleGalleryData = yield select(getSingleGalleryData);

    const payloadData = {
      data: payload,
    };

    const requestData = {
      additionalUrl: 'prphotogallery',
      method: singleGalleryData ? 'PUT' : 'POST',
      data: payloadData,
    };

    const response = yield call(processRequest, requestData);

    yield put(singleGalleryAction.updateSingleGalleryDataSuccess(response.data));
  } catch (e) {
    yield put(singleGalleryAction.updateSingleGalleryDataFailure(e));
  }
}

export function* watchHandleGetSingleGalleryDataSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singleGalleryActionTypes.GET_SINGLE_GALLERY_DATA_REQUEST, handleGetSingleGalleryDataSaga);
}

export function* watchHandleUpdateSingleGalleryDataSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singleGalleryActionTypes.UPDATE_SINGLE_GALLERY_DATA_REQUEST, handleUpdateSingleGallerySaga);
}
