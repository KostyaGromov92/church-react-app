/* eslint-disable max-len */
// @flow
// SINGLE NEWS - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import {
  call,
  put,
  takeEvery,
  select,
} from 'redux-saga/effects';

// TYPES
import * as singleNewsActionTypes from '../actions/types/singleNews';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as singleNewsAction from '../actions/singleNews';

// SELECTORS
import { getSingleNewsData } from '../selectors/singleNews';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetSingleNewsSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload && payload.newsId) {
      const { newsId } = payload;

      const requestData = {
        additionalUrl: `prnews/${newsId}`,
        method: 'GET',
      };

      const response = yield call(processRequest, requestData);

      yield put(singleNewsAction.getSingleNewsSuccess(response.data));
    }
  } catch (e) {
    yield put(singleNewsAction.getSingleNewsFailure(e));
  }
}

export function* handleUpdateSingleNewsSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;
    const singleNewsData = yield select(getSingleNewsData);

    const payloadData = {
      data: payload,
    };

    const requestData = {
      additionalUrl: 'prnews',
      method: singleNewsData ? 'PUT' : 'POST',
      data: payloadData,
    };

    const response = yield call(processRequest, requestData);

    yield put(singleNewsAction.handleSubmitSingleNewsSuccess(response.data));
  } catch (e) {
    yield put(singleNewsAction.handleSubmitSingleNewsFailure(e));
  }
}

export function* watchHandleGetSingleNewsSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singleNewsActionTypes.GET_SINGLE_NEWS_REQUEST, handleGetSingleNewsSaga);
}

export function* watchHandleUpdateSingleNewsSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singleNewsActionTypes.HANDLE_SUBMIT_SINGLE_NEWS_REQUEST, handleUpdateSingleNewsSaga);
}
