/* eslint-disable max-len */
// @flow
// SINGLE CHURCH - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as singleChurchActionTypes from '../actions/types/singleChurch';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as singleChurchAction from '../actions/singleChurch';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetSingleChurchSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload && payload.churchId) {
      const { churchId } = payload;

      const requestData = {
        additionalUrl: `posts/${churchId}`,
        method: 'GET',
        baseUrl: 'https://jsonplaceholder.typicode.com/',
      };

      const response = yield call(processRequest, requestData);

      yield put(singleChurchAction.getSingleChurchSuccess(response.data));
    }
  } catch (e) {
    yield put(singleChurchAction.getSingleChurchFailure(e));
  }
}

export function* handleUpdateSingleChurchSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload && payload.data) {
      const { data } = payload;

      const requestData = {
        additionalUrl: `posts/${data.churchId}`,
        method: 'PUT',
        data,
        baseUrl: 'https://jsonplaceholder.typicode.com/',
      };

      const response = yield call(processRequest, requestData);

      yield put(singleChurchAction.updateSingleChurchSuccess(response.data));
    }
  } catch (e) {
    yield put(singleChurchAction.updateSingleChurchFailure(e));
  }
}

export function* watchHandleGetSingleChurchSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singleChurchActionTypes.GET_SINGLE_CHURCH_REQUEST, handleGetSingleChurchSaga);
}

export function* watchHandleUpdateSingleChurchSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singleChurchActionTypes.UPDATE_SINGLE_CHURCH_REQUEST, handleUpdateSingleChurchSaga);
}
