/* eslint-disable max-len */
// @flow
// HISTORY DATA - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as historyDataActionTypes from '../actions/types/history';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as historyDataAction from '../actions/history';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetHistoryDataSaga(): Generator<PutEffect, *, *> {
  try {
    const requestData = {
      additionalUrl: 'prhistory',
      method: 'GET',
    };

    const response = yield call(processRequest, requestData);

    yield put(historyDataAction.getHistoryDataSuccess(response.data));
  } catch (e) {
    yield put(historyDataAction.getHistoryDataFailure(e));
  }
}

export function* handleUpdateHistoryDataSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload && payload.data) {
      const { data } = payload;

      const payloadData = {
        data,
      };

      const requestData = {
        additionalUrl: 'prhistory',
        method: 'PUT',
        data: payloadData,
      };

      const response = yield call(processRequest, requestData);

      yield put(historyDataAction.updateHistoryDataSuccess(response.data));
    }
  } catch (e) {
    yield put(historyDataAction.updateHistoryDataFailure(e));
  }
}

export function* watchHandleGetHistoryDataSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(historyDataActionTypes.GET_HISTORY_DATA_REQUEST, handleGetHistoryDataSaga);
}

export function* watchHandleUpdateHistoryDataSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(historyDataActionTypes.UPDATE_HISTORY_DATA_REQUEST, handleUpdateHistoryDataSaga);
}
