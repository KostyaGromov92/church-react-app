// @flow
// MAIN PAGE DATA - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as mainPageDataActionTypes from '../actions/types/mainPageData';

// ACTIONS
import * as mainPageDataAction from '../actions/mainPageData';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetMainPageDataSaga(): Generator<PutEffect, *, *> {
  try {
    const requestData = {
      additionalUrl: 'prtranding/tranding',
      method: 'GET',
    };

    const response = yield call(processRequest, requestData);

    yield put(mainPageDataAction.getMainPageDataSuccess(response.data));
  } catch (e) {
    yield put(mainPageDataAction.getMainPageDataFailure(e));
  }
}

export function* watchHandleGetMainPageDataSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(mainPageDataActionTypes.GET_MAIN_PAGE_DATA_REQUEST, handleGetMainPageDataSaga);
}
