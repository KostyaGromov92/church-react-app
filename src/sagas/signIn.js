// @flow
// SIGN IN - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as signInActionTypes from '../actions/types/signIn';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as signInAction from '../actions/signIn';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleSignInSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    const payloadData = {
      data: payload,
    };

    const requestData = {
      additionalUrl: 'login',
      method: 'POST',
      apiMarket: 'login',
      data: payloadData,
    };

    const response = yield call(processRequest, requestData);

    yield put(signInAction.getUserSignInSuccess(response.data));
  } catch (e) {
    yield put(signInAction.getUserSignInFailure(e));
  }
}

export function* watchHandleSignInSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(signInActionTypes.GET_USER_SIGN_IN_REQUEST, handleSignInSaga);
}
