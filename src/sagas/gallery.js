/* eslint-disable max-len */
// @flow
// PHOTO GALLERY - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as galleryActionTypes from '../actions/types/gallery';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as galleryAction from '../actions/gallery';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetPhotoGallerySaga(): Generator<PutEffect, *, *> {
  try {
    const requestData = {
      additionalUrl: 'prphotogallery',
      method: 'GET',
    };

    const response = yield call(processRequest, requestData);

    yield put(galleryAction.getPhotoGallerySuccess(response.data));
  } catch (e) {
    yield put(galleryAction.getPhotoGalleryFailure(e));
  }
}

export function* handleRemovePhotoGallerySaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;
    if (payload && payload.photoGalleryId) {
      const { photoGalleryId } = payload;

      const requestData = {
        additionalUrl: `prphotogallery/${photoGalleryId}`,
        method: 'DELETE',
      };

      yield call(processRequest, requestData);

      yield put(galleryAction.removePhotoGallerySuccess(photoGalleryId));
    }
  } catch (e) {
    yield put(galleryAction.removePhotoGalleryFailure(e));
  }
}

export function* watchHandleGetPhotoGallerySaga(): Generator<IOEffect, *, *> {
  yield takeEvery(galleryActionTypes.GET_PHOTO_GALLERY_REQUEST, handleGetPhotoGallerySaga);
}

export function* watchRemovePhotoGallerySaga(): Generator<IOEffect, *, *> {
  yield takeEvery(galleryActionTypes.REMOVE_PHOTO_GALLERY_REQUEST, handleRemovePhotoGallerySaga);
}
