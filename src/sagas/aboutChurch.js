/* eslint-disable max-len */
// @flow
// ABOUT CHURCH - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as aboutChurchActionTypes from '../actions/types/aboutChurch';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as aboutChurchAction from '../actions/aboutChurch';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetAboutChurchSaga(): Generator<PutEffect, *, *> {
  try {
    const requestData = {
      additionalUrl: 'praboutchurch',
      method: 'GET',
    };

    const response = yield call(processRequest, requestData);

    yield put(aboutChurchAction.getAboutChurchSuccess(response.data));
  } catch (e) {
    yield put(aboutChurchAction.getAboutChurchFailure(e));
  }
}

export function* handleUpdateAboutChurchSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload && payload.data) {
      const { data } = payload;

      const payloadData = {
        data,
      };

      const requestData = {
        additionalUrl: 'praboutchurch',
        method: 'PUT',
        data: payloadData,
      };

      const response = yield call(processRequest, requestData);

      yield put(aboutChurchAction.updateAboutChurchSuccess(response.data));
    }
  } catch (e) {
    yield put(aboutChurchAction.updateAboutChurchFailure(e));
  }
}

export function* watchHandleGetAboutChurchSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(aboutChurchActionTypes.GET_ABOUT_CHURCH_REQUEST, handleGetAboutChurchSaga);
}

export function* watchHandleUpdateAboutChurchSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(aboutChurchActionTypes.UPDATE_ABOUT_CHURCH_REQUEST, handleUpdateAboutChurchSaga);
}
