// @flow
// MOCK DATA - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import {
  call, put, takeEvery, all,
} from 'redux-saga/effects';

import type { FluxStandardAction } from '../types/flux';

// CONSTANTS
import { PHOTO_API_URL, PHOTO_API_TOKEN } from '../constants/api';

// TYPES
import * as imageFilesActionTypes from '../actions/types/imageFiles';

// ACTIONS
import * as imageFilesAction from '../actions/imageFiles';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleUploadImageSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;


    if (payload && payload.photos) {
      yield all(payload.photos.map((photo) => {
        const requestData = {
          additionalUrl: `public.php/webdav/${photo.name}`,
          method: 'PUT',
          data: photo,
          apiMarket: 'photo',
          baseUrl: PHOTO_API_URL,
        };

        return call(processRequest, requestData);
      }));

      yield put(imageFilesAction.uploadImagesSuccess());
    }
  } catch (e) {
    yield put(imageFilesAction.uploadImagesFailure(e));
  }
}

export function* handleRemoveImageSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload) {
      const requestData = {
        additionalUrl: `public.php/webdav/${payload}`,
        method: 'DELETE',
        token: PHOTO_API_TOKEN,
        apiMarket: 'photo',
        baseUrl: PHOTO_API_URL,
      };

      yield call(processRequest, requestData);
      yield put(imageFilesAction.removeImageSuccess());
    }
  } catch (e) {
    yield put(imageFilesAction.removeImageFailure(e));
  }
}

export function* handleGetImageSaga(): Generator<PutEffect, *, *> {
  try {
    const fileName = 'fe1a0a44-2374-4fbf-8fed-5b0b34fc1df1.png';

    // https://s1.studme.1m.ua/index.php/s/52bXb5NwLxBKJgO/download?files=81b9edf7-e058-44a2-a090-9ee66c9071b1.png
    // https://s1.studme.1m.ua/index.php/s/52bXb5NwLxBKJgO/download?files=4be57b90-2d28-49e0-9da9-d767b815625f.png

    const requestData = {
      additionalUrl: `public.php/webdav/${fileName}`,
      method: 'GET',
      apiMarket: 'photo',
      baseUrl: PHOTO_API_URL,
    };

    const response = yield call(processRequest, requestData);

    yield put(imageFilesAction.getImagesUriSuccess(response.data));
  } catch (e) {
    yield put(imageFilesAction.getImagesUriFailure(e));
  }
}

export function* watchHandleUploadImageSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(imageFilesActionTypes.UPLOAD_IMAGES_REQUEST, handleUploadImageSaga);
}

export function* watchHandleGetImageSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(imageFilesActionTypes.GET_IMAGES_URI_REQUEST, handleGetImageSaga);
}

export function* watchHandleRemoveImageSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(imageFilesActionTypes.REMOVE_IMAGE_REQUEST, handleRemoveImageSaga);
}
