// @flow
// NEWS LIST - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as newsActionTypes from '../actions/types/newsList';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as newsAction from '../actions/newsList';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetNewsSaga(): Generator<PutEffect, *, *> {
  try {
    const requestData = {
      additionalUrl: 'prnews',
      method: 'GET',
    };

    const response = yield call(processRequest, requestData);

    yield put(newsAction.getNewsSuccess(response.data));
  } catch (e) {
    yield put(newsAction.getNewsFailure(e));
  }
}

export function* handleDeleteNewsSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;
    if (payload && payload.newsId) {
      const { newsId } = payload;

      const requestData = {
        additionalUrl: `prnews/${newsId}`,
        method: 'DELETE',
      };

      yield call(processRequest, requestData);

      yield put(newsAction.deleteNewsSuccess(newsId));
    }
  } catch (e) {
    yield put(newsAction.deleteNewsFailure(e));
  }
}

export function* watchHandleGetNewsSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(newsActionTypes.GET_NEWS_REQUEST, handleGetNewsSaga);
}

export function* watchDeleteNewsSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(newsActionTypes.DELETE_NEWS_REQUEST, handleDeleteNewsSaga);
}
