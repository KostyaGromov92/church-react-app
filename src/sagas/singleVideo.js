/* eslint-disable max-len */
// @flow
// SINGLE VIDEO - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import {
  call,
  put,
  takeEvery,
  select,
} from 'redux-saga/effects';

// TYPES
import * as singleVideoActionTypes from '../actions/types/singleVideo';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as singleVideoAction from '../actions/singleVideo';

// SELECTORS
import { getSingleVideoData } from '../selectors/singleVideo';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetSingleVideoSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload && payload.videoId) {
      const { videoId } = payload;

      const requestData = {
        additionalUrl: `prvideo/${videoId}`,
        method: 'GET',
      };

      const response = yield call(processRequest, requestData);

      yield put(singleVideoAction.getSingleVideoSuccess(response.data));
    }
  } catch (e) {
    yield put(singleVideoAction.getSingleVideoFailure(e));
  }
}

export function* handleUpdateSingleVideoSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;
    const singleVideoData = yield select(getSingleVideoData);

    const payloadData = {
      data: payload,
    };

    const requestData = {
      additionalUrl: 'prvideo',
      method: singleVideoData ? 'PUT' : 'POST',
      data: payloadData,
    };

    const response = yield call(processRequest, requestData);

    yield put(singleVideoAction.handleSubmitSingleVideoSuccess(response.data));
  } catch (e) {
    yield put(singleVideoAction.handleSubmitSingleVideoFailure(e));
  }
}

export function* watchHandleGetSingleVideoSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singleVideoActionTypes.GET_SINGLE_VIDEO_REQUEST, handleGetSingleVideoSaga);
}

export function* watchHandleUpdateSingleVideoSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singleVideoActionTypes.HANDLE_SUBMIT_SINGLE_VIDEO_REQUEST, handleUpdateSingleVideoSaga);
}
