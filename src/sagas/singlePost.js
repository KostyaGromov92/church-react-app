/* eslint-disable max-len */
// @flow
// SINGLE POST - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import {
  call,
  put,
  takeEvery,
  select,
} from 'redux-saga/effects';

// TYPES
import * as singlePostActionTypes from '../actions/types/singlePost';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as singlePostAction from '../actions/singlePost';

// SELECTORS
import { getSinglePostData } from '../selectors/singlePost';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetSinglePostSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload && payload.postId) {
      const { postId } = payload;

      const requestData = {
        additionalUrl: `prblog/${postId}`,
        method: 'GET',
      };

      const response = yield call(processRequest, requestData);

      yield put(singlePostAction.getSinglePostSuccess(response.data));
    }
  } catch (e) {
    yield put(singlePostAction.getSinglePostFailure(e));
  }
}

export function* handleUpdateSinglePostSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;
    const singlePostData = yield select(getSinglePostData);

    const payloadData = {
      data: payload,
    };

    const requestData = {
      additionalUrl: 'prblog',
      method: singlePostData ? 'PUT' : 'POST',
      data: payloadData,
    };

    const response = yield call(processRequest, requestData);

    yield put(singlePostAction.handleSubmitSinglePostSuccess(response.data));
  } catch (e) {
    yield put(singlePostAction.handleSubmitSinglePostFailure(e));
  }
}

export function* watchHandleGetSinglePostSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singlePostActionTypes.GET_SINGLE_POST_REQUEST, handleGetSinglePostSaga);
}

export function* watchHandleUpdateSinglePostSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(singlePostActionTypes.HANDLE_SUBMIT_SINGLE_POST_REQUEST, handleUpdateSinglePostSaga);
}
