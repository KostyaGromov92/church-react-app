// @flow
// POSTS LIST - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as postsListActionTypes from '../actions/types/postsList';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as postsListAction from '../actions/postsList';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetPostsListSaga(): Generator<PutEffect, *, *> {
  try {
    const requestData = {
      additionalUrl: 'prblog',
      method: 'GET',
    };

    const response = yield call(processRequest, requestData);

    yield put(postsListAction.getPostsListSuccess(response.data));
  } catch (e) {
    yield put(postsListAction.getPostsListFailure(e));
  }
}

export function* handleDeletePostSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;
    if (payload && payload.postId) {
      const { postId } = payload;

      const requestData = {
        additionalUrl: `prblog/${postId}`,
        method: 'DELETE',
      };

      yield call(processRequest, requestData);

      yield put(postsListAction.deletePostSuccess(postId));
    }
  } catch (e) {
    yield put(postsListAction.deletePostFailure(e));
  }
}

export function* watchHandleGetPostsListSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(postsListActionTypes.GET_POSTS_LIST_REQUEST, handleGetPostsListSaga);
}

export function* watchHandleDeletePostSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(postsListActionTypes.DELETE_POST_REQUEST, handleDeletePostSaga);
}
