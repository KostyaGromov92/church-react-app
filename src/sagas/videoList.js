// @flow
// VIDEO LIST - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as videoListActionTypes from '../actions/types/videoList';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as videoListAction from '../actions/videoList';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetVideoListSaga(): Generator<PutEffect, *, *> {
  try {
    const requestData = {
      additionalUrl: 'prvideo',
      method: 'GET',
    };

    const response = yield call(processRequest, requestData);

    yield put(videoListAction.getVideoListSuccess(response.data));
  } catch (e) {
    yield put(videoListAction.getVideoListFailure(e));
  }
}

export function* handleDeleteVideoSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;
    if (payload && payload.videoId) {
      const { videoId } = payload;

      const requestData = {
        additionalUrl: `prvideo/${videoId}`,
        method: 'DELETE',
      };

      yield call(processRequest, requestData);

      yield put(videoListAction.deleteVideoSuccess(videoId));
    }
  } catch (e) {
    yield put(videoListAction.deleteVideoFailure(e));
  }
}

export function* watchHandleGetVideoListSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(videoListActionTypes.GET_VIDEO_LIST_REQUEST, handleGetVideoListSaga);
}

export function* watchHandleDeleteVideoSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(videoListActionTypes.DELETE_VIDEO_REQUEST, handleDeleteVideoSaga);
}
