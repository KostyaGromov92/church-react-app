/* eslint-disable max-len */
// @flow
// ABOUT US DATA - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';

// TYPES
import * as aboutDataActionTypes from '../actions/types/aboutData';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as aboutDataAction from '../actions/aboutData';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetAboutDataSaga(): Generator<PutEffect, *, *> {
  try {
    const requestData = {
      additionalUrl: 'praboutus',
      method: 'GET',
    };

    const response = yield call(processRequest, requestData);

    yield put(aboutDataAction.getAboutDataSuccess(response.data));
  } catch (e) {
    yield put(aboutDataAction.getAboutDataFailure(e));
  }
}

export function* handleUpdateAboutDataSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload && payload.data) {
      const { data } = payload;

      const payloadData = {
        data,
      };

      const requestData = {
        additionalUrl: 'praboutus',
        method: 'PUT',
        data: payloadData,
      };

      const response = yield call(processRequest, requestData);

      yield put(aboutDataAction.updateAboutDataSuccess(response.data));
    }
  } catch (e) {
    yield put(aboutDataAction.updateAboutDataFailure(e));
  }
}

export function* watchHandleGetAboutDataSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(aboutDataActionTypes.GET_ABOUT_DATA_REQUEST, handleGetAboutDataSaga);
}

export function* watchHandleUpdateAboutDataSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(aboutDataActionTypes.UPDATE_ABOUT_DATA_REQUEST, handleUpdateAboutDataSaga);
}
