// @flow

import { fork } from 'redux-saga/effects';

import * as mockDataSagas from './sagas/mockData';
import * as imageFilesSaga from './sagas/imageFiles';
import * as newsListSagas from './sagas/newsList';
import * as singleNews from './sagas/singleNews';
import * as churchList from './sagas/churchList';
import * as singleChurch from './sagas/singleChurch';
import * as gallery from './sagas/gallery';
import * as singleGallery from './sagas/singleGallery';
import * as signIn from './sagas/signIn';

import * as postsList from './sagas/postsList';
import * as singlePost from './sagas/singlePost';

import * as videoList from './sagas/videoList';
import * as singleVideo from './sagas/singleVideo';

import * as aboutData from './sagas/aboutData';

import * as aboutChurch from './sagas/aboutChurch';

import * as historyData from './sagas/history';

import * as mainPageData from './sagas/mainPageData';

export default function* rootSaga(): Generator<*, *, *> {
  yield fork(mockDataSagas.watchHandleGetMockDataSaga);

  yield fork(imageFilesSaga.watchHandleGetImageSaga);
  yield fork(imageFilesSaga.watchHandleUploadImageSaga);
  yield fork(imageFilesSaga.watchHandleRemoveImageSaga);

  yield fork(newsListSagas.watchHandleGetNewsSaga);
  yield fork(newsListSagas.watchDeleteNewsSaga);

  yield fork(postsList.watchHandleGetPostsListSaga);
  yield fork(postsList.watchHandleDeletePostSaga);

  yield fork(singlePost.watchHandleGetSinglePostSaga);
  yield fork(singlePost.watchHandleUpdateSinglePostSaga);

  yield fork(singleNews.watchHandleGetSingleNewsSaga);
  yield fork(singleNews.watchHandleUpdateSingleNewsSaga);

  yield fork(churchList.watchHandleGetChurchListSaga);
  yield fork(churchList.watchDeleteChurchSaga);

  yield fork(videoList.watchHandleDeleteVideoSaga);
  yield fork(videoList.watchHandleGetVideoListSaga);

  yield fork(singleVideo.watchHandleGetSingleVideoSaga);
  yield fork(singleVideo.watchHandleUpdateSingleVideoSaga);

  yield fork(singleChurch.watchHandleGetSingleChurchSaga);
  yield fork(singleChurch.watchHandleUpdateSingleChurchSaga);

  yield fork(gallery.watchHandleGetPhotoGallerySaga);
  yield fork(gallery.watchRemovePhotoGallerySaga);

  yield fork(singleGallery.watchHandleGetSingleGalleryDataSaga);
  yield fork(singleGallery.watchHandleUpdateSingleGalleryDataSaga);

  yield fork(mainPageData.watchHandleGetMainPageDataSaga);

  yield fork(aboutData.watchHandleGetAboutDataSaga);
  yield fork(aboutData.watchHandleUpdateAboutDataSaga);

  yield fork(aboutChurch.watchHandleGetAboutChurchSaga);
  yield fork(aboutChurch.watchHandleUpdateAboutChurchSaga);

  yield fork(historyData.watchHandleGetHistoryDataSaga);
  yield fork(historyData.watchHandleUpdateHistoryDataSaga);

  yield fork(signIn.watchHandleSignInSaga);
}
