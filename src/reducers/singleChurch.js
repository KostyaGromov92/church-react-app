// @flow

/* eslint-disable max-len */
// SINGLE CHURCH - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as singleChurchActionTypes from '../actions/types/singleChurch';

// TYPES
import type { SingleChurch } from '../types/churchList';

export type SingleChurchState = {
  isFetching: boolean,
  data: ?SingleChurch,
  error: ?Object,
};

export const initialState: SingleChurchState = {
  isFetching: false,
  data: null,
  error: null,
};

const handlerMap = {
  [singleChurchActionTypes.GET_SINGLE_CHURCH_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singleChurchActionTypes.GET_SINGLE_CHURCH_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    data: action.payload,
  }),

  [singleChurchActionTypes.GET_SINGLE_CHURCH_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singleChurchActionTypes.UPDATE_SINGLE_CHURCH_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singleChurchActionTypes.UPDATE_SINGLE_CHURCH_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    data: action.payload,
  }),

  [singleChurchActionTypes.UPDATE_SINGLE_CHURCH_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singleChurchActionTypes.CLEAR_SINGLE_CHURCH_DATA]: (state) => ({
    ...state,
    isFetching: false,
    data: null,
  }),
};

export default handleActions(handlerMap, initialState);
