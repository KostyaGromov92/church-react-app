// @flow
/* eslint-disable max-len */
// PHOTO GALLERY - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as photoGalleryActionTypes from '../actions/types/gallery';

export type PhotoGalleryState = {
  isFetching: boolean,
  photoGallery: Object,
  error: ?Object,
};

export const initialState: PhotoGalleryState = {
  isFetching: false,
  photoGallery: {},
  error: null,
};

const handlerMap = {
  [photoGalleryActionTypes.GET_PHOTO_GALLERY_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [photoGalleryActionTypes.GET_PHOTO_GALLERY_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    photoGallery: action.payload,
  }),

  [photoGalleryActionTypes.GET_PHOTO_GALLERY_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [photoGalleryActionTypes.REMOVE_PHOTO_GALLERY_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [photoGalleryActionTypes.REMOVE_PHOTO_GALLERY_SUCCESS]: (state, action) => {
    const photoGalleryId = action.payload;
    const photoGalleryList = state.photoGallery && state.photoGallery.data && state.photoGallery.data.filter((gallery) => gallery.id !== photoGalleryId);

    return {
      ...state,
      isFetching: false,
      photoGallery: {
        data: photoGalleryList,
      },
    };
  },

  [photoGalleryActionTypes.REMOVE_PHOTO_GALLERY_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),


};

export default handleActions(handlerMap, initialState);
