// @flow

/* eslint-disable max-len */
// NEWS - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as newsActionTypes from '../actions/types/newsList';

export type NewsListState = {
  isFetching: boolean,
  news: Object,
  error: ?Object,
};

export const initialState: NewsListState = {
  isFetching: false,
  news: {},
  error: null,
};

const handlerMap = {
  [newsActionTypes.GET_NEWS_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [newsActionTypes.GET_NEWS_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    news: action.payload,
  }),

  [newsActionTypes.GET_NEWS_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [newsActionTypes.DELETE_NEWS_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [newsActionTypes.DELETE_NEWS_SUCCESS]: (state, action) => {
    const newsId = action.payload;
    const newNewsList = state.news && state.news.data && state.news.data.filter((news) => news.id !== newsId);

    return {
      ...state,
      isFetching: false,
      news: {
        data: newNewsList,
      },
    };
  },

  [newsActionTypes.GET_NEWS_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
