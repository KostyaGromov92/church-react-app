// @flow

/* eslint-disable max-len */
// ABOUT DATA - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as aboutDataActionTypes from '../actions/types/aboutData';

export type AboutDataState = {
  isFetching: boolean,
  data: Object,
  error: ?Object,
};

export const initialState: AboutDataState = {
  isFetching: false,
  data: {},
  error: null,
};

const handlerMap = {
  [aboutDataActionTypes.GET_ABOUT_DATA_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [aboutDataActionTypes.GET_ABOUT_DATA_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    data: action.payload,
  }),

  [aboutDataActionTypes.GET_ABOUT_DATA_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [aboutDataActionTypes.UPDATE_ABOUT_DATA_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [aboutDataActionTypes.UPDATE_ABOUT_DATA_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    data: action.payload,
  }),

  [aboutDataActionTypes.UPDATE_ABOUT_DATA_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
