// @flow

/* eslint-disable max-len */
// SINGLE POST - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as singlePostActionTypes from '../actions/types/singlePost';

export type SinglePostState = {
  isFetching: boolean,
  singlePostData: ?Object,
  error: ?Object,
};

export const initialState: SinglePostState = {
  isFetching: false,
  singlePostData: null,
  error: null,
};

const handlerMap = {
  [singlePostActionTypes.GET_SINGLE_POST_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singlePostActionTypes.GET_SINGLE_POST_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    singlePostData: action.payload,
  }),

  [singlePostActionTypes.GET_SINGLE_POST_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singlePostActionTypes.HANDLE_SUBMIT_SINGLE_POST_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singlePostActionTypes.HANDLE_SUBMIT_SINGLE_POST_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    singlePostData: action.payload,
  }),

  [singlePostActionTypes.HANDLE_SUBMIT_SINGLE_POST_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singlePostActionTypes.CLEAR_SINGLE_POST_DATA]: (state) => ({
    ...state,
    isFetching: false,
    singlePostData: null,
  }),
};

export default handleActions(handlerMap, initialState);
