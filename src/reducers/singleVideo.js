// @flow

/* eslint-disable max-len */
// SINGLE VIDEO - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as singleVideoActionTypes from '../actions/types/singleVideo';

export type SingleVideoState = {
  isFetching: boolean,
  singleVideoData: ?Object,
  error: ?Object,
};

export const initialState: SingleVideoState = {
  isFetching: false,
  singleVideoData: null,
  error: null,
};

const handlerMap = {
  [singleVideoActionTypes.GET_SINGLE_VIDEO_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singleVideoActionTypes.GET_SINGLE_VIDEO_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    singleVideoData: action.payload,
  }),

  [singleVideoActionTypes.GET_SINGLE_VIDEO_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singleVideoActionTypes.HANDLE_SUBMIT_SINGLE_VIDEO_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singleVideoActionTypes.HANDLE_SUBMIT_SINGLE_VIDEO_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    singleVideoData: action.payload,
  }),

  [singleVideoActionTypes.HANDLE_SUBMIT_SINGLE_VIDEO_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singleVideoActionTypes.CLEAR_SINGLE_VIDEO_DATA]: (state) => ({
    ...state,
    isFetching: false,
    singleVideoData: null,
  }),
};

export default handleActions(handlerMap, initialState);
