// @flow

/* eslint-disable max-len */
// POSTS LIST - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as postsListActionTypes from '../actions/types/postsList';

export type PostsListState = {
  isFetching: boolean,
  posts: Object,
  error: ?Object,
};

export const initialState: PostsListState = {
  isFetching: false,
  posts: [],
  error: null,
};

const handlerMap = {
  [postsListActionTypes.GET_POSTS_LIST_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [postsListActionTypes.GET_POSTS_LIST_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    posts: action.payload,
  }),

  [postsListActionTypes.GET_POSTS_LIST_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [postsListActionTypes.DELETE_POST_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [postsListActionTypes.DELETE_POST_SUCCESS]: (state, action) => {
    const postId = action.payload;
    const newPostsList = state.posts && state.posts.data && state.posts.data.filter((post) => post.id !== postId);

    return {
      ...state,
      isFetching: false,
      posts: {
        data: newPostsList,
      },
    };
  },

  [postsListActionTypes.DELETE_POST_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
