// @flow

/* eslint-disable max-len */
// ABOUT CHURCH - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as aboutChurchActionTypes from '../actions/types/aboutChurch';

export type AboutChurchState = {
  isFetching: boolean,
  data: Object,
  error: ?Object,
};

export const initialState: AboutChurchState = {
  isFetching: false,
  data: {},
  error: null,
};

const handlerMap = {
  [aboutChurchActionTypes.GET_ABOUT_CHURCH_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [aboutChurchActionTypes.GET_ABOUT_CHURCH_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    data: action.payload,
  }),

  [aboutChurchActionTypes.GET_ABOUT_CHURCH_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [aboutChurchActionTypes.UPDATE_ABOUT_CHURCH_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [aboutChurchActionTypes.UPDATE_ABOUT_CHURCH_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    data: action.payload,
  }),

  [aboutChurchActionTypes.UPDATE_ABOUT_CHURCH_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
