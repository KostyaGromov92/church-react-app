// @flow
/* eslint-disable max-len */
// IMAGE FILES - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as imageFilesActionTypes from '../actions/types/imageFiles';

export type ImageFilesState = {
  isFetching: boolean,
  imageUriList: ?Array<string>,
  imageFiles: ?Array<Object>,
  error: ?Object,
};

export const initialState: ImageFilesState = {
  isFetching: false,
  imageUriList: null,
  imageFiles: null,
  error: null,
};

const handlerMap = {
  [imageFilesActionTypes.GET_IMAGES_URI_REQUEST]: state => ({
    ...state,
    isFetching: true,
  }),

  [imageFilesActionTypes.GET_IMAGES_URI_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    imageUriList: action.payload,
  }),

  [imageFilesActionTypes.GET_IMAGES_URI_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [imageFilesActionTypes.UPLOAD_IMAGES_REQUEST]: state => ({
    ...state,
    isFetching: true,
  }),

  [imageFilesActionTypes.UPLOAD_IMAGES_SUCCESS]: (state, action) => {
    const imageFiles = [...state.imageFiles, ...action.payload];

    return {
      ...state,
      isFetching: false,
      imageFiles,
    };
  },

  [imageFilesActionTypes.UPLOAD_IMAGES_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [imageFilesActionTypes.REMOVE_IMAGE_REQUEST]: state => ({
    ...state,
    isFetching: true,
  }),

  [imageFilesActionTypes.REMOVE_IMAGE_SUCCESS]: (state, action) => {
    const imageUriList = state.imageUriList.filter(imageUri => imageUri !== action.payload);

    return {
      ...state,
      isFetching: false,
      imageUriList,
    };
  },

  [imageFilesActionTypes.REMOVE_IMAGE_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [imageFilesActionTypes.CLEAR_IMAGE_LIST_URI]: state => ({
    ...state,
    imageUriList: [],
  }),
};

export default handleActions(handlerMap, initialState);
