/* eslint-disable max-len */
// CHURCH LIST - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as churchListActionTypes from '../actions/types/churchList';

// TYPES
import type { SingleChurch } from '../types/churchList';

export type ChurchListState = {
  isFetching: boolean,
  сhurchList: ?Array<SingleChurch>,
  error: ?Object,
};

export const initialState: ChurchListState = {
  isFetching: false,
  churchList: null,
  error: null,
};

const handlerMap = {
  [churchListActionTypes.GET_СHURCHES_REQUEST]: state => ({
    ...state,
    isFetching: true,
  }),

  [churchListActionTypes.GET_СHURCHES_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    churchList: action.payload,
  }),

  [churchListActionTypes.GET_СHURCHES_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [churchListActionTypes.DELETE_СHURCH_REQUEST]: state => ({
    ...state,
    isFetching: true,
  }),

  [churchListActionTypes.DELETE_СHURCH_SUCCESS]: (state, action) => {
    const churchId = action.payload;
    const churchList = state.churchList.filter(church => church.id !== churchId);

    return {
      ...state,
      isFetching: false,
      churchList,
    };
  },

  [churchListActionTypes.DELETE_СHURCH_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
