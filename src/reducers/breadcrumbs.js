// @flow
/* eslint-disable max-len */

// BREADCRUMBS - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as breadcrumbsActionTypes from '../actions/types/breadcrumbs';

export type BreadcrumbsState = {
  breadcrumb: string,
};

export const initialState: BreadcrumbsState = {
  breadcrumb: '',
};

const handlerMap = {
  [breadcrumbsActionTypes.SET_CURRENT_BREADCRUMB]: (state, action) => ({
    ...state,
    breadcrumb: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
