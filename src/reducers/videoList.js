// @flow

/* eslint-disable max-len */
// VIDEO LIST - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as videoListActionTypes from '../actions/types/videoList';

export type VideoListState = {
  isFetching: boolean,
  videoList: Object,
  error: ?Object,
};

export const initialState: VideoListState = {
  isFetching: false,
  videoList: {},
  error: null,
};

const handlerMap = {
  [videoListActionTypes.GET_VIDEO_LIST_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [videoListActionTypes.GET_VIDEO_LIST_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    videoList: action.payload,
  }),

  [videoListActionTypes.GET_VIDEO_LIST_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [videoListActionTypes.DELETE_VIDEO_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [videoListActionTypes.DELETE_VIDEO_SUCCESS]: (state, action) => {
    const videoId = action.payload;
    const newVideoList = state.videoList && state.videoList.data && state.videoList.data.filter((video) => video.id !== videoId);

    return {
      ...state,
      isFetching: false,
      videoList: {
        data: newVideoList,
      },
    };
  },

  [videoListActionTypes.DELETE_VIDEO_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
