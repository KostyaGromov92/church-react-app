// @flow

/* eslint-disable max-len */
// SINGLE NEWS - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as singleNewsActionTypes from '../actions/types/singleNews';

export type SingleNewsState = {
  isFetching: boolean,
  singleNewsData: ?Object,
  error: ?Object,
};

export const initialState: SingleNewsState = {
  isFetching: false,
  singleNewsData: {},
  error: null,
};

const handlerMap = {
  [singleNewsActionTypes.GET_SINGLE_NEWS_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singleNewsActionTypes.GET_SINGLE_NEWS_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    singleNewsData: action.payload,
  }),

  [singleNewsActionTypes.GET_SINGLE_NEWS_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singleNewsActionTypes.HANDLE_SUBMIT_SINGLE_NEWS_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singleNewsActionTypes.HANDLE_SUBMIT_SINGLE_NEWS_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    singleNewsData: action.payload,
  }),

  [singleNewsActionTypes.HANDLE_SUBMIT_SINGLE_NEWS_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singleNewsActionTypes.CLEAR_SINGLE_NEWS_DATA]: (state) => ({
    ...state,
    isFetching: false,
    singleNewsData: null,
  }),
};

export default handleActions(handlerMap, initialState);
