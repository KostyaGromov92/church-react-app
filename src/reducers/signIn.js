// @flow

/* eslint-disable max-len */
// SIGN IN - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as signInActionTypes from '../actions/types/signIn';

export type SignInState = {
  isFetching: boolean,
  data: Object,
  error: ?Object,
};

export const initialState: SignInState = {
  isFetching: false,
  data: null,
  error: null,
};

const handlerMap = {
  [signInActionTypes.GET_USER_SIGN_IN_REQUEST]: state => ({
    ...state,
    isFetching: true,
    error: null,
  }),

  [signInActionTypes.GET_USER_SIGN_IN_SUCCESS]: (state, action) => {
    const token = action && action.payload && action.payload.token;

    if (token) localStorage.setItem('auth_token', token);

    return {
      ...state,
      isFetching: false,
      data: action.payload,
    };
  },

  [signInActionTypes.GET_USER_SIGN_IN_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
