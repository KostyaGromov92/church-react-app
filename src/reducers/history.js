// @flow

/* eslint-disable max-len */
// HISTORY DATA - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as historyDataActionTypes from '../actions/types/history';

export type HistoryDataState = {
  isFetching: boolean,
  data: Object,
  error: ?Object,
};

export const initialState: HistoryDataState = {
  isFetching: false,
  data: {},
  error: null,
};

const handlerMap = {
  [historyDataActionTypes.GET_HISTORY_DATA_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [historyDataActionTypes.GET_HISTORY_DATA_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    data: action.payload,
  }),

  [historyDataActionTypes.GET_HISTORY_DATA_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [historyDataActionTypes.UPDATE_HISTORY_DATA_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [historyDataActionTypes.UPDATE_HISTORY_DATA_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    data: action.payload,
  }),

  [historyDataActionTypes.UPDATE_HISTORY_DATA_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
