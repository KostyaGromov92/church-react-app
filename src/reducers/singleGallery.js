// @flow

/* eslint-disable max-len */
// SINGLE GALLERY - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as singleGalleryActionTypes from '../actions/types/singleGallery';

export type SingleGalleryState = {
  isFetching: boolean,
  photoGallery: ?Object,
  error: ?Object,
};

export const initialState: SingleGalleryState = {
  isFetching: false,
  photoGallery: null,
  error: null,
};

const handlerMap = {
  [singleGalleryActionTypes.GET_SINGLE_GALLERY_DATA_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singleGalleryActionTypes.GET_SINGLE_GALLERY_DATA_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    photoGallery: action.payload,
  }),

  [singleGalleryActionTypes.GET_SINGLE_GALLERY_DATA_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singleGalleryActionTypes.UPDATE_SINGLE_GALLERY_DATA_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [singleGalleryActionTypes.UPDATE_SINGLE_GALLERY_DATA_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    photoGallery: action.payload,
  }),

  [singleGalleryActionTypes.UPDATE_SINGLE_GALLERY_DATA_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),

  [singleGalleryActionTypes.CLEAR_SINGLE_GALLERY_DATA]: (state) => ({
    ...state,
    isFetching: false,
    photoGallery: null,
  }),
};

export default handleActions(handlerMap, initialState);
