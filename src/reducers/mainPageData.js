// @flow

/* eslint-disable max-len */
// MAIN PAGE DATA - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as mainPageDataActionTypes from '../actions/types/mainPageData';

export type MainPageDataState = {
  isFetching: boolean,
  mainPageDataList: Object,
  error: ?Object,
};

export const initialState: MainPageDataState = {
  isFetching: false,
  mainPageDataList: [],
  error: null,
};

const handlerMap = {
  [mainPageDataActionTypes.GET_MAIN_PAGE_DATA_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [mainPageDataActionTypes.GET_MAIN_PAGE_DATA_SUCCESS]: (state, action) => ({
    ...state,
    isFetching: false,
    mainPageDataList: action.payload,
  }),

  [mainPageDataActionTypes.GET_MAIN_PAGE_DATA_FAILURE]: (state, action) => ({
    ...state,
    isFetching: false,
    error: action.payload,
  }),
};

export default handleActions(handlerMap, initialState);
