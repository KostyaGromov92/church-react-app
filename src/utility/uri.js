// @flow

export type ContentTypeParseResult = {
  isContentType: boolean,
  value: string,
  boundary?: string,
};

const contentTypeRegex = /([a-z]+\/[a-z-]+)(;\s*boundary=([^\s]+))?/i;

// eslint-disable-next-line import/prefer-default-export
export const parseContentType = (contentTypeString: ?string): ContentTypeParseResult => {
  const matches = contentTypeString && contentTypeString.match(contentTypeRegex);
  const isContentType = !!(matches && matches.length > 0);

  return {
    isContentType,
    value: (isContentType && matches && matches.length >= 1) ? matches[1] : (contentTypeString || ''),
    ...(isContentType && matches && matches.length >= 3) ? { boundary: matches[3] } : undefined,
  };
};
