// @flow

import type { ContentTypeParseResult } from './uri';
import { parseContentType } from './uri';

// eslint-disable-next-line import/prefer-default-export
export function handleBody(response: Response): Promise<Object | null | string> {
  if (!response) {
    return Promise.reject(new Error('No response supplied to handleBody'));
  }

  const contentType = response.headers.get('content-Type');
  let parsedContentType: ?ContentTypeParseResult;

  if (contentType) {
    parsedContentType = parseContentType(contentType);

    if (!parsedContentType.isContentType) {
      return Promise.reject(new Error('Content-Type parsing failed'));
    }
  } else {
    return Promise.resolve(null);
  }

  switch (parsedContentType.value.toLowerCase()) {
    case 'application/json':
    case 'text/json':
      try {
        return response.json();
      } catch (error) {
        return Promise.reject(new Error("Got a JSON response, but it couldn't be parsed"));
      }

    case 'text/plain':
    case 'text/html':
      // These may be returned on a 500 status, or other unexpected outcome.
      try {
        return response.text().then(text => ({ text }));
      } catch (error) {
        return Promise.reject(new Error("Got a text response, but it couldn't be parsed"));
      }

    case 'image/png':
    case 'image/jpeg':
    case 'image/jpg':
    case 'image/gif':
    case 'image/svg':
      try {
        return response.blob().then(data => URL.createObjectURL(data));
      } catch (e) {
        return Promise.reject(new Error('Unexpected image response'));
      }

    case 'application/xml':
    case 'text/xml':
      return Promise.reject(new Error('Unexpected XML response'));

    default:
      return Promise.reject(new Error(`Unrecognized Content-Type: ${contentType}`));
  }
}
