/* eslint-disable import/prefer-default-export */
// MATERIAL UI - CONSTANTS

// HELPERS
import { isUserAuthenticated } from '../services/helpers';

export const DRAWER_WIDTH = isUserAuthenticated() ? 240 : 0;
