// eslint-disable-next-line import/prefer-default-export
export const ADMIN_MENU_ROUTES = [
  { name: 'О Церкви', url: '/admin/church' },
  { name: 'История', url: '/admin/history' },
  { name: 'Фотогоалерея', url: '/admin/gallery' },
  { name: 'Блог', url: '/admin/blog' },
  { name: 'О нас', url: '/admin/about' },
  { name: 'Новости', url: '/admin/news' },
  { name: 'Видео', url: '/admin/video' },

  // { name: 'События', url: '/admin/blog' },

  // { name: 'Форум', url: '/forum' },
  // { name: 'Контакты', url: '/contacts' },
];

export const ADMIN_ROUTES = {
  home: '/admin',
  church: '/admin/church',
  addVideo: '/admin/video/add-video',
  video: '/admin/video',
  signIn: '/admin/sign-in',
  about: '/admin/about',
  post: '/admin/blog',
  addPost: '/admin/blog/add-post',
  gallery: '/admin/gallery',
  addGallery: '/admin/gallery/add-gallery',
  history: '/admin/history',
  addNews: '/admin/news/add-news',
  news: '/admin/news',
  addChurch: '/admin/church/add-church',
};

export const MAIN_ROUTES = {
  home: '/',
  church: '/church',
  history: '/history',
  gallery: '/gallery',
  blog: '/blog',
  about: '/about',
  news: '/news',
  video: '/video',
};
