// @flow
// SINGLE GALLERY TYPE - TYPES
// =============================================================================

export type SingleGalleryData = {
  userId: number,
  id: number,
  title: string,
  body: string
}
