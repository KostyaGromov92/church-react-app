// @flow
// NEWS LIST - TYPES
// =============================================================================

export type SingleMockNews = {
  userId: number,
  id: number,
  title: string,
  body: string
}
