// @flow

export type LocationState = {
  accepted?: boolean,
  from?: string,
  stripeToken?: { id: string },
  textConfirm?: boolean,
  hasPrevious?: boolean,
};

export type Location = {
  state?: LocationState,
  pathname: string,
  search?: string,
  hash?: string,
};

export type History = {
  location: Location,
  push: (url: string, state?: Object) => void,
  replace: (url: string, state?: Object) => void,
  goBack: () => void,
};

export type RoutingState = {
  location: Location,
  previousLocation: Location,
  isErrorPage: boolean,
};
