// @flow
// CHURCH LIST - TYPES
// =============================================================================

// TODO Need change after get data from server
export type SingleChurch = {
  userId: number,
  id: number,
  title: string,
  body: string
}
