// @flow
// VALIDATION RULES FOR HISTORY COMPONENT

const validate = (values: any) => {
  const errors = {};

  if (!values.title) {
    errors.title = 'Заголовок обязателен';
  } else if (values.title.length < 5) {
    errors.title = 'Заголовок должен состоят из 5 или больше символов';
  }

  if (!values.editorData) {
    errors.editorData = 'Описание не может быть пустым';
  }


  return errors;
};

export default validate;
