// @flow
// VALIDATION RULES FOR CHURCH EDITOR COMPONENT

const validate = (values: any) => {
  const errors = {};

  if (!values.title) {
    errors.title = 'Заголовок обязателен';
  } else if (values.title.length < 5) {
    errors.title = 'Заголовок должен состоят из 5 или больше символов';
  }
  //
  if (!values.subTitle) {
    errors.subTitle = 'Подзаголовок обязателен';
  } else if (values.subTitle.length < 5) {
    errors.subTitle = 'Подзаголовок должен состоят из 5 или больше символов';
  }


  return errors;
};

export default validate;
