// @flow
// VALIDATION RULES FOR POST EDITOR COMPONENT

const validate = (values: Object) => {
  const errors = {};

  if (!values.title) {
    errors.title = 'Заголовок обязателен';
  } else if (values.title.length < 5) {
    errors.title = 'Заголовок должен состоят из 5 или больше символов';
  }

  if (!values.subTitle) {
    errors.subTitle = 'Подзаголовок обязателен';
  } else if (values.subTitle.length < 5) {
    errors.subTitle = 'Подзаголовок должен состоят из 5 или больше символов';
  }

  if (!values.editorData) {
    errors.editorData = 'Описание не может быть пустым';
  }

  return errors;
};

export default validate;
