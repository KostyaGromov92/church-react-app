// @flow
// VALIDATION RULES FOR VIDEO EDITOR COMPONENT

const validate = (values: Object) => {
  const errors = {};

  if (!values.title) {
    errors.title = 'Заголовок обязателен';
  } else if (values.title.length < 5) {
    errors.title = 'Заголовок должен состоят из 5 или больше символов';
  }

  if (!values.videoLink) {
    errors.videoLink = 'Пожалуйста добавьте ссылку на видео';
  }

  if (!values.body) {
    errors.body = 'Описание обязательное';
  } else if (values.body.length < 8) {
    errors.body = 'Описание должно состоят из 8 или больше символов';
  }

  return errors;
};

export default validate;
