// @flow

// VALIDATION RULES FOR SIGN IN PAGE

// HELPERS
import { isEmail } from '../services/helpers';

const validate = (values: Object) => {
  const errors = {};

  if (!values.userEmail) {
    errors.userEmail = 'Email обязателен';
  } else if (!isEmail(values.userEmail)) {
    errors.userEmail = 'Пожалуйста введите валидный email';
  }

  if (!values.userPassword) {
    errors.userPassword = 'Пароль обязателен';
  } else if (values.userPassword.length < 5) {
    errors.userPassword = 'Пароль должен состоят из 5 или больше символов';
  }

  return errors;
};

export default validate;
