// @flow
// VALIDATION RULES FOR NEWS EDITOR COMPONENT

const validate = (values: Object) => {
  const errors = {};

  if (!values.title) {
    errors.title = 'Заголовок обязателен';
  } else if (values.title.length < 5) {
    errors.title = 'Заголовок должен состоят из 5 или больше символов';
  }

  if (!values.body) {
    errors.body = 'Описание обязательное';
  } else if (values.body.length < 8) {
    errors.body = 'Описание должно состоят из 8 или больше символов';
  }

  return errors;
};

export default validate;
