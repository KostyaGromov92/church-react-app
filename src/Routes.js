/* eslint-disable max-len */
/* eslint-disable react/prefer-stateless-function */
// @flow

import React, { Component, Suspense, lazy } from 'react';
import { withRouter, Switch } from 'react-router-dom';

// LINKS
import { ADMIN_ROUTES, MAIN_ROUTES } from './constants/routes';

// COMPONENTS
import Route from './components/Route';
import PrivateRoute from './components/Route/PrivateRoute';
import NotFoundComponent from './components/NotFoundComponent';

// CONTAINERS
const MainPageContainer = lazy(() => import('./containers/MainPageContainer'));

const NewsContainer = lazy(() => import('./containers/NewsContainer'));
const SingleNewsContainer = lazy(() => import('./containers/SingleNewsContainer'));

const EventsListContainer = lazy(() => import('./containers/EventsListContainer'));
const SingleEventContainer = lazy(() => import('./containers/SingleEventContainer'));

const VideoListContainer = lazy(() => import('./containers/VideoListContainer'));
const SingleVideoContainer = lazy(() => import('./containers/SingleVideoContainer'));

const GalleryListContainer = lazy(() => import('./containers/GalleryListContainer'));
const SingleGalleryContainer = lazy(() => import('./containers/SingleGalleryContainer'));

const AboutContainer = lazy(() => import('./containers/AboutContainer'));

const HistoryContainer = lazy(() => import('./containers/HistoryContainer'));

// ADMIN CONTAINER
const AdminSignInContainer = lazy(() => import('./containers/Admin/AdminSignInContainer'));

const AdminNewsContainer = lazy(() => import('./containers/Admin/AdminNewsContainer'));
const AdminNewsEditorContainer = lazy(() => import('./containers/Admin/AdminNewsEditorContainer'));

const AdminPostsContainer = lazy(() => import('./containers/Admin/AdminPostsContainer'));
const AdminPostEditorContainer = lazy(() => import('./containers/Admin/AdminPostEditorContainer'));

const AdminChurchEditorContainer = lazy(() => import('./containers/Admin/AdminChurchEditorContainer'));
const AdminAboutChurchContainer = lazy(() => import('./containers/Admin/AdminAboutChurchContainer'));

const AdminPhotoGalleryContainer = lazy(() => import('./containers/Admin/AdminPhotoGalleryContainer'));
const AdminPhotoGalleryEditorContainer = lazy(() => import('./containers/Admin/AdminPhotoGalleryEditorContainer'));

const AdminVideoListComponent = lazy(() => import('./containers/Admin/AdminVideoListComponent'));
const AdminVideoEditorComponent = lazy(() => import('./containers/Admin/AdminVideoEditorComponent'));

const AdminAboutContainer = lazy(() => import('./containers/Admin/AdminAboutContainer'));

const AdminHistoryContainer = lazy(() => import('./containers/Admin/AdminHistoryContainer'));

const AboutChurchContainer = lazy(() => import('./containers/AboutChurchContainer'));

type Props = {
  location: Object
};

class Routes extends Component<Props> {
  render() {
    const { location } = this.props;

    return (
      <Suspense fallback={null}>
        <Switch location={location}>
          <Route exact path="/" component={MainPageContainer} />
          <Route exact path={MAIN_ROUTES.home} component={MainPageContainer} />

          <Route exact path={MAIN_ROUTES.news} component={NewsContainer} />
          <Route exact path={`${MAIN_ROUTES.news}/:id`} component={SingleNewsContainer} />

          <Route exact path={MAIN_ROUTES.video} component={VideoListContainer} />
          <Route exact path={`${MAIN_ROUTES.video}/:id`} component={SingleVideoContainer} />

          <Route exact path={MAIN_ROUTES.gallery} component={GalleryListContainer} />
          <Route exact path={`${MAIN_ROUTES.gallery}/:id`} component={SingleGalleryContainer} />

          <Route exact path={MAIN_ROUTES.blog} component={EventsListContainer} />
          <Route exact path={`${MAIN_ROUTES.blog}/:id`} component={SingleEventContainer} />

          <Route exact path={MAIN_ROUTES.about} component={AboutContainer} />

          <Route exact path={MAIN_ROUTES.church} component={AboutChurchContainer} />

          <Route exact path={MAIN_ROUTES.history} component={HistoryContainer} />

          <Route exact path={ADMIN_ROUTES.signIn} component={AdminSignInContainer} checkAuth />

          <PrivateRoute exact path={ADMIN_ROUTES.home} component={AdminAboutChurchContainer} checkAuth />

          <PrivateRoute exact path={ADMIN_ROUTES.church} component={AdminAboutChurchContainer} checkAuth />
          <PrivateRoute exact path={`${ADMIN_ROUTES.church}/:id`} component={AdminChurchEditorContainer} checkAuth />
          <PrivateRoute exact path={ADMIN_ROUTES.addChurch} component={AdminChurchEditorContainer} checkAuth />

          <PrivateRoute exact path={ADMIN_ROUTES.news} component={AdminNewsContainer} checkAuth />
          <PrivateRoute exact path={`${ADMIN_ROUTES.news}/:id`} component={AdminNewsEditorContainer} checkAuth />
          <PrivateRoute exact path={ADMIN_ROUTES.addNews} component={AdminNewsEditorContainer} checkAuth />

          <PrivateRoute exact path={ADMIN_ROUTES.post} component={AdminPostsContainer} checkAuth />
          <PrivateRoute exact path={`${ADMIN_ROUTES.post}/:id`} component={AdminPostEditorContainer} checkAuth />
          <PrivateRoute exact path={ADMIN_ROUTES.addPost} component={AdminPostEditorContainer} checkAuth />

          <PrivateRoute exact path={ADMIN_ROUTES.gallery} component={AdminPhotoGalleryContainer} checkAuth />
          <PrivateRoute exact path={`${ADMIN_ROUTES.gallery}/:id`} component={AdminPhotoGalleryEditorContainer} checkAuth />
          <PrivateRoute exact path={ADMIN_ROUTES.addGallery} component={AdminPhotoGalleryEditorContainer} checkAuth />

          <PrivateRoute exact path={ADMIN_ROUTES.about} component={AdminAboutContainer} checkAuth />

          <PrivateRoute exact path={ADMIN_ROUTES.history} component={AdminHistoryContainer} checkAuth />

          <PrivateRoute exact path={ADMIN_ROUTES.video} component={AdminVideoListComponent} checkAuth />
          <PrivateRoute exact path={`${ADMIN_ROUTES.video}/:id`} component={AdminVideoEditorComponent} checkAuth />

          <Route exact path="/not-found" component={NotFoundComponent} />
          <Route exact path="*" component={NotFoundComponent} />

        </Switch>
      </Suspense>
    );
  }
}

export default withRouter(Routes);
