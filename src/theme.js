// @flow

import { makeStyles } from '@material-ui/core/styles';

// CONSTANTS
import { DRAWER_WIDTH } from './constants/materialUI';

const theme = makeStyles((themeProp) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [themeProp.breakpoints.up('sm')]: {
      width: DRAWER_WIDTH,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${DRAWER_WIDTH}px)`,
      marginLeft: DRAWER_WIDTH,
    },
  },
  menuButton: {
    marginRight: themeProp.spacing(2),
    [themeProp.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: themeProp.mixins.toolbar,
  drawerPaper: {
    width: DRAWER_WIDTH,
  },
  content: {
    flexGrow: 1,
    padding: themeProp.spacing(3),
  },
}));


export default theme;
