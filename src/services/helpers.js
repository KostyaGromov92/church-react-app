/* eslint-disable func-names */
/* eslint-disable max-len */
/* eslint-disable radix */
/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable no-bitwise */
/* eslint-disable no-use-before-define */
/* eslint-disable no-nested-ternary */

import moment from 'moment';
import * as md5 from 'blueimp-md5';
import _indexOf from 'lodash/indexOf';
import _get from 'lodash/get';
import _reduce from 'lodash/reduce';
import _isEqual from 'lodash/isEqual';
import 'url-polyfill';
import * as _isBase64 from 'is-base64';
import validate from 'uuid-validate';

export const deepCompare = (a, b) => _reduce(a, (result, value, key) => (_isEqual(value, b[key])
  ? result
  : result.concat(key)), []);

// System
export const getDisplayName = (Component) => Component.displayName || Component.name || 'Component';

// Checking
export const isEmpty = (value) => {
  try {
    return value === undefined || value === '' || value === null || JSON.stringify(value) === JSON.stringify({}) || value.length === 0;
  } catch (e) {
    return false;
  }
};
export const isZeroValuesInObject = (value) => !isEmpty(value) && !Object.values(value).some((item) => item > 0);
export const isFalseInArray = (value) => isArray(value) && !isEmpty(value) && value.some((item) => !item);
export const isEmail = (value) => !!value.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
export const isNumber = (value) => !Number.isNaN(parseFloat(value)) && Number.isFinite(parseFloat(value));
export const isString = (value) => typeof value === 'string' || value instanceof String;
export const isPhone = (value) => !!value.match(/^\(\d\d\d\) \d\d\d-\d\d\d\d$/);
export const isUrl = (value) => {
  try {
    const url = new URL(value);
    return !isBase64(url);
  } catch (e) {
    return false;
  }
};
export const isFile = (value) => value instanceof File;
export const isArray = (value) => Array.isArray(value);

export const isUniqueArray = (value) => {
  if (isArray(value) && !isEmpty(value)) {
    const unique = value.filter((v, i, a) => a.indexOf(v) === i);

    return unique.length === value.length;
  }

  return false;
};

// [{value: 1}, {value: 2}]
export const isUniqueArrayOfObjects = (value) => {
  if (isArray(value) && !isEmpty(value)) {
    const arrayOfValues = value.map((item) => item.value);

    return isUniqueArray(arrayOfValues);
  }

  return false;
};

export const isUserAuthenticated = () => !!localStorage.getItem('auth_token');

export const validateGUID = (guid) => validate(guid);

// Strings
export const getEmailDomain = (value) => value.split('@')[1];
export const transliteration = (str) => {
  const text = str.toLowerCase();
  // Символ, на который будут заменяться все спецсимволы
  const space = '-';

  // Массив для транслитерации
  const transl = {
    а: 'a',
    б: 'b',
    в: 'v',
    г: 'g',
    ґ: 'g',
    д: 'd',
    е: 'e',
    ё: 'yo',
    ж: 'zh',
    з: 'z',
    и: 'i',
    і: 'i',
    й: 'y',
    к: 'k',
    л: 'l',
    м: 'm',
    н: 'n',
    о: 'o',
    п: 'p',
    р: 'r',
    с: 's',
    т: 't',
    у: 'u',
    ф: 'f',
    х: 'h',
    ц: 'ts',
    ч: 'ch',
    ш: 'sh',
    щ: 'sch',
    ъ: '',
    ы: 'y',
    ь: '',
    э: 'e',
    ю: 'yu',
    я: 'ya',
    ' ': '_',
    '`': '',
    '~': '',
    '!': '',
    '@': '',
    '#': '',
    $: '',
    '%': '',
    '^': '',
    '&': '',
    '*': '',
    '(': '',
    ')': '',
    '-': '',
    '=': '',
    '+': '',
    '[': '',
    ']': '',
    '\\': '',
    '|': '',
    '/': '',
    '.': '',
    ',': '',
    '{': '',
    '}': '',
    '\'': '',
    '"': '',
    ';': '',
    ':': '',
    '?': '',
    '<': '',
    '>': '',
    '№': '',
  };

  let result = '';
  let currentSim = '';

  for (let i = 0; i < text.length; i++) {
    // Если символ найден в массиве то меняем его
    if (transl[text[i]] !== undefined) {
      if (currentSim !== transl[text[i]] || currentSim !== space) {
        result += transl[text[i]];
        currentSim = transl[text[i]];
      }
    } else {
      // Если нет, то оставляем так как есть
      result += text[i];
      currentSim = text[i];
    }
  }

  result = result.replace(/_+/g, '_');
  // special trim
  result = result.replace(/^-/, '').replace(/-$/, '');

  return result;
};

// Transform
export const transformToFloat = (value) => (isNumber(value) ? value : isEmpty(value) ? 0 : parseFloat(value.replace(/ /g, '').replace(/,/, '.')));
export const transformToInt = (value) => (isEmpty(value)
  ? 0 : isNumber(parseInt(value)) ? parseInt(value) : 0);
export const transformToBool = (value) => (isEmpty(value) ? false : !!value);
export const transformToString = (value) => (isEmpty(value) ? '' : value.toString());
export const transformSelectToInt = (value) => (isEmpty(value) ? null : parseInt(value));
export const transformToPositiveInt = (value) => ((isEmpty(value) || (isNumber(value) && parseInt(value) < 0)) ? '' : parseInt(value));

// Compare
export const compareInt = (value1, value2) => transformToInt(value1) === transformToInt(value2);

// Format

export const formatPhone = (value) => {
  const newValue = value.replace(/\(|\)|\+|-| /g, '');

  return `${newValue.substr(0, 3)}-${newValue.substr(3, 3)}-${newValue.substr(6, 4)}`;
};
export const formatDate = (value) => moment(value).format('Do MMMM dddd \\a\\t hh:mm A');
export const formatDateToStartOfDayIso = (value) => {
  const momentDate = value ? moment(value) : moment();

  return momentDate.set({
    hour: 0,
    minute: 0,
    second: 0,
    millisecond: 0,
  }).toISOString();
};
export const formatTime = (millisecond) => {
  millisecond = Math.floor(millisecond / 1000);

  const hours = Math.floor(millisecond / 3600).toString().padStart(2, '0');
  millisecond %= 3600;
  const minutes = Math.floor(millisecond / 60).toString().padStart(2, '0');
  const seconds = (millisecond % 60).toString().padStart(2, '0');

  return `${hours}:${minutes}:${seconds}`;
};

export const getYouTubeId = (link) => {
  try {
    const url = new URL(link);
    const videoId = url.searchParams.get('v');

    return videoId;
  } catch (e) {
    return false;
  }
};

export const getFileName = (fileName) => {
  const file = fileName.split('.');
  file.pop();

  return file.join('');
};

export const getFileExtension = (fileName) => {
  const file = fileName.split('.').pop();

  // if file is an URL, then we have to delete all URL params
  return file.split('?')[0].toLowerCase();
};

export const sleep = (ms = 1000) => new Promise((resolve) => setTimeout(resolve, ms));

export const deleteActiveClass = (elements, activeDayClass) => {
  const listOfElements = document.querySelectorAll(elements);

  return listOfElements.forEach((currentDay) => {
    currentDay.classList.remove(activeDayClass);
  });
};

// Converting date for calendar operations
export const convertTime = ({
  time, day = moment(), fixStartDay = false, fixEndDay = false,
}) => {
  // Create new instance of day
  const momentDay = moment(day);
  // Get real day in current locale
  const dayParts = {
    year: momentDay.get('year'),
    month: momentDay.get('month'),
    date: momentDay.get('date'),
  };
  // Parse time
  const timeParts = time.split(':');
  // Create new date object
  const newTime = moment.utc().set({
    year: dayParts.year,
    month: dayParts.month,
    date: dayParts.date,
    hour: timeParts[0],
    minute: timeParts[1],
    second: 0,
    millisecond: 0,
  });

  // Create new date object (local)
  const newTimeMomentDayLocal = moment(newTime).local();
  const newTimeLocal = moment().local().set({
    year: dayParts.year,
    month: dayParts.month,
    date: dayParts.date,
    hour: newTimeMomentDayLocal.get('hour'),
    minute: newTimeMomentDayLocal.get('minute'),
    second: 0,
    millisecond: 0,
  });

  // Needed to be fixed if start day starts from midnight
  if (fixStartDay && newTimeLocal.clone().format('HH') === '00') {
    newTimeLocal.subtract(1, 'd');
  }

  // Needed to be fixed if end day starts from midnight
  if (fixEndDay && newTimeLocal.clone().format('HH') === '00') {
    newTimeLocal.add(1, 'd');
  }

  return newTimeLocal;
};

export const getBrowserName = () => {
  let name = 'unknown';
  if (navigator.userAgent.indexOf('MSIE') !== -1) {
    name = 'msie';
  } else if (navigator.userAgent.indexOf('Firefox') !== -1) {
    name = 'firefox';
  } else if (navigator.userAgent.indexOf('Opera') !== -1) {
    name = 'opera';
  } else if (navigator.userAgent.indexOf('Chrome') !== -1) {
    name = 'chrome';
  } else if (navigator.userAgent.indexOf('Safari') !== -1) {
    name = 'safari';
  }

  return name;
};

export const checkAssessmentRestrictions = (restrictions) => {
  const { maxAssessments, usedAssessments } = restrictions;

  return maxAssessments <= usedAssessments;
};

export const corporationHasSubscription = (currentSubscription) => _get(currentSubscription, 'subscription', false);

// decimalAdjust('round', value, exp);
// decimalAdjust('floor', value, exp);
// decimalAdjust('ceil', value, exp);


export const getSubscriptionDayDiff = (subscription) => {
  let subscriptionDayDiff = false;
  if (isEmpty(subscription)) {
    return subscriptionDayDiff;
  }

  const { dateTo } = subscription;
  if (dateTo && !isEmpty(dateTo) && moment(dateTo) > moment()) {
    subscriptionDayDiff = moment(dateTo).to(moment(), true);
  }

  return subscriptionDayDiff;
};

export const stringToColor = (str) => {
  const string = md5(str);
  let hash = 0;
  for (let i = 0; i < string.length; i++) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }
  let colour = '#';
  for (let i = 0; i < 3; i++) {
    const value = (hash >> (i * 8)) & 0xFF;
    colour += (`00${value.toString(16)}`).substr(-2);
  }

  return colour;
};

export const getStartAndEndOfYear = (year = moment().format('YYYY')) => {
  const momentYear = {
    year,
    month: 0,
    date: 1,
    hour: 0,
    minute: 0,
    second: 0,
    milliseconds: 0,
  };

  return {
    start: moment().utc().set(momentYear).startOf('year')
      .toISOString(),
    end: moment().utc().set(momentYear).endOf('year')
      .toISOString(),
  };
};

export const getBase64 = (file) => {
  const reader = new FileReader();
  reader.readAsDataURL(file);

  return new Promise((resolve, reject) => {
    reader.onload = function () {
      resolve(reader.result);
    };
    reader.onerror = function (error) {
      reject(error);
    };
  });
};

export const isBase64 = (value) => _isBase64(value) || _isBase64(value, { mime: true });

export const hash = ({ salt = '', unique = false }) => md5(`${salt}${unique ? new Date().getTime() : ''}`);

export const getAllowToCreateConference = (conferenceSettings, auth) => {
  const { allowedToCreate } = conferenceSettings;

  return !isEmpty(allowedToCreate) && _indexOf(allowedToCreate, auth.companyUserId) !== -1;
};

export const urlHasProtocol = (url) => !isEmpty(url) && (url.indexOf('http:') !== -1 || url.indexOf('https:') !== -1);

export const createFileLinkAndClick = (response, fileName) => {
  const a = document.createElement('a');

  document.body.appendChild(a);
  a.href = URL.createObjectURL(response.data);
  a.download = fileName;
  a.click();

  return true;
};

export const compareDate = (startDate, endDate) => moment(startDate).isBefore(endDate);
