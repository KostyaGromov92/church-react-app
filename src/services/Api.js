/* eslint-disable max-len */
import React from 'react';
import axios from 'axios';
import { BASE_API_URL, PHOTO_API_TOKEN } from '../constants/api';
import { Redirect } from 'react-router';
import { BASE_API_URL } from '../constants/api';
import { ADMIN_ROUTES } from '../constants/routes';


export function checkStatus(responseData) {
  if (responseData.status >= 200 && responseData.status < 300) {
    return { data: responseData.data, headers: responseData.headers, status: responseData.status };
  }

  const error = new Error((responseData.data.response && responseData.data.response.message) || '');

  error.response = responseData.data;
  throw error;
}

// eslint-disable-next-line import/prefer-default-export
export function processRequest({
  additionalUrl = '',
  method = 'GET',
  data = {},
  apiMarket = 'default',
  baseUrl = BASE_API_URL,
  token = PHOTO_API_TOKEN,
}) {
  const accessToken = localStorage.getItem('auth_token');
  const url = baseUrl + additionalUrl;

  let headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Space-Id': '00000000-0000-0000-0000-000000000000',
    Accept: 'application/json',
  };

  switch (apiMarket) {
    case 'photo':
      headers = {
        Authorization: `Basic ${btoa(`${token}:`)}`,
        'Content-Type': 'text/html; charset=UTF-8',
      };
      break;

    case 'login':
      headers = { ...headers };
      break;

    default:
      headers = { ...headers, Authorization: `${accessToken}` };
  }

  return axios({
    method,
    data,
    headers,
    crossDomain: true,
    url,
  })
    .then((res) => checkStatus(res))
    // eslint-disable-next-line consistent-return
    .catch((error) => {
      if (error.response) {
        // Request made and server responded
        if (error.response.status === 401) {
          localStorage.removeItem('auth_token');
          return <Redirect to={ADMIN_ROUTES.signIn} />;
        }
      }
    });
}
